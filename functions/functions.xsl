<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="xs tg saxon"
    xmlns:fn="http://www.w3.org/2005/xpath-functions" version="2.0">

       <xsl:function name="tg:normalizeLocalPath">
        <xsl:param name="input"/>
        <xsl:variable name="step0" select="replace($input,'file:','')"/>
        <!-- innerstes replace: '\\' zu '/' -->
        <!-- zweites replace: für Windows: f:/digibib -> /f:/digibib -->
        <!-- aeusseres replace: '/' am Ende des Strings wird entfernt -->
        <xsl:variable name="step1"
            select="replace(replace(replace($step0,'\\','/'),'^(\w:/)','/$1'),'/$','')"/>
        <!-- für Windows: f:\digibib -> /F:/digibib -->        
        <xsl:variable name="step2">
            <xsl:analyze-string select="$step1" regex="^/\w:/">
                <xsl:matching-substring>
                    <xsl:value-of select="upper-case(.)"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:value-of select="."/>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:value-of select="string($step2)"/>       
    </xsl:function>

 
    <xsl:function name="tg:encode-for-uri">
        <xsl:param name="input"/>
        <!-- '/' und ':' bleiben unveraendert. Fuer andere Sonderzeichen wird die XSLT-Funktion encode-for-uri aufgerufen. -->
        <xsl:variable name="result-sequence">
            <xsl:analyze-string select="$input" regex="[/:]">
                <xsl:matching-substring>
                    <xsl:value-of select="."/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:value-of select="encode-for-uri(.)"/>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:value-of select="string($result-sequence)"/>
    </xsl:function>

   
    <xsl:function name="tg:get-output-path">
        <xsl:param name="crturi"/>
        
        <!--z.B. C:/Dokumente und Einstellungen/kab31ej/Eigene Dateien/textgrid_all/textgrid/data/Digibib-Outputs/Philosophie -->
        <xsl:param name="path-to-main-IO-folder"/>      
        
        <!-- z.B. 02-metadaten -->
        <xsl:param name="outputFolder"/>
        <!--  <xsl:message select="concat('input_subdir', $input_subdir)"></xsl:message>-->
   <!--     <xsl:message select="concat('inputdir', $path-to-main-IO-folder)"></xsl:message>
        <xsl:message select="concat('outputFolder', $outputFolder)"></xsl:message>-->
        <xsl:variable name="BaseName">
            <xsl:value-of select="tg:getBasename($crturi)"/>
        </xsl:variable>
        <xsl:value-of select="concat($path-to-main-IO-folder,'/',$outputFolder,'/',$BaseName)"/>
    </xsl:function>
    <xsl:function name="tg:getOutputPathOfSplittetAuthor">
        <xsl:param name="crturi"/>
        
        <!--z.B. C:/Dokumente und Einstellungen/kab31ej/Eigene Dateien/textgrid_all/textgrid/data/Digibib-Outputs/Philosophie -->
        <xsl:param name="path-to-main-IO-folder"/>      
        
        <!-- z.B. 02-metadaten -->
        <xsl:param name="outputFolder"/>
        <!--  <xsl:message select="concat('input_subdir', $input_subdir)"></xsl:message>-->
       <!-- <xsl:message select="concat('inputdir', $path-to-main-IO-folder)"></xsl:message>
        <xsl:message select="concat('outputFolder', $outputFolder)"></xsl:message>-->
        <xsl:variable name="basename-without-ext">
            <xsl:value-of select="tg:getBasename_noExt($crturi)"/>
        </xsl:variable>
        <xsl:value-of select="concat($path-to-main-IO-folder,'/',$outputFolder,'/',$basename-without-ext)"/>         
    </xsl:function>
    
    <xsl:function name="tg:getFormatted_tgID">
        <xsl:param name="tgID"/>
        <!-- tgIDnr muss eine Nummer sein, damit format-number() funktioniert, damit die Dateinamen gut aussehen: 000001.xml -->
        <xsl:variable name="tgIDnr"
            select="format-number(number(replace($tgID,'tg(\d+)(\..*)?','$1')),'000000')"/>
        <xsl:variable name="tgIDsuffix" select="replace($tgID,'^tg\d+','')"/>
        <xsl:value-of select="concat($tgIDnr,$tgIDsuffix)"/>
    </xsl:function>


    <!-- Extrahiert aus einer vollstaendigen Pfadangabe den Namen der Datei-->    
    <xsl:function name="tg:getBasename">
        <xsl:param name="uri"/>
        <xsl:value-of select="replace($uri,'.+/','')"/>
    </xsl:function>

<!-- Extrahiert aus einer vollstaendigen Pfadangabe den Namen der Datei ohne Endung -->
    <xsl:function name="tg:getBasename_noExt">
        <xsl:param name="uri"/>
        <xsl:value-of select="replace(replace($uri,'.+/',''),'\.[^\.]+$','')"/>
        <!--<xsl:message select="replace(replace($uri,'.+/',''),'\.[^\.]+$','')"></xsl:message>-->
    </xsl:function>

   <xsl:function name="tg:getDirname">
        <xsl:param name="uri"/>
        <xsl:value-of select="replace($uri,'/[^/]+$','')"/>
       <!--<xsl:message select="concat('getDirNama', replace($uri,'/[^/]+$',''))"></xsl:message>-->
    </xsl:function>
    <xsl:function name="tg:get-dir-uri">
        <xsl:param name="uri"/>
        <xsl:value-of select="tg:getDirname($uri)"/>
    </xsl:function>
   

    <xsl:function name="tg:zipLists2Dictionary">
        <xsl:param name="keys"/>
        <xsl:param name="values"/>
        <xsl:variable name="xml-keys">
            <xsl:call-template name="textList2xmlList">
                <xsl:with-param name="textList" select="$keys"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="xml-values">
            <xsl:call-template name="textList2xmlList">
                <xsl:with-param name="textList" select="$values"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:for-each select="$xml-keys/*">
            <xsl:variable name="crtPosition" select="position()"/>
            <xsl:copy>
                <xsl:copy-of select="@*"/>
                <xsl:attribute name="value" select="$xml-values/*[position()= $crtPosition]/@key"/>
            </xsl:copy>
        </xsl:for-each>
    </xsl:function>

    <xsl:template name="textList2xmlList">
        <xsl:param name="textList"/>
        <xsl:analyze-string select="$textList" regex=",\s*">
            <xsl:matching-substring/>
            <xsl:non-matching-substring>
                <item key="{replace(.,'^\s*(.+)\s*$','$1')}"/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>


    <xsl:function name="tg:correct-path-sep">
        <xsl:param name="path"/>
        <xsl:value-of
            select="if (matches($path,'^/\w:/')) 
            then replace(substring($path,2),'/','\\')
            else $path"/>
        <!--<xsl:choose>
            <xsl:when test="matches($path,'^/\w:/')">
            <xsl:value-of select="replace(substring($path,2),'/','\\')"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise>
            <xsl:value-of select="$path"/>
            </xsl:otherwise>
            </xsl:choose>-->
    </xsl:function>
    
    <xsl:function name="tg:resolve-path-with-dir-references">
        <!-- docu: Funktion zur Auflösung von Referenzen in Pfaden. Z.B.:
            "/F:/textgrid/projects/../../Digitale%20Bibliothek" wird zu
            "/F:/Digitale%20Bibliothek". -->
        <xsl:param name="path"/>
        <!-- Trennen des Pfads in den Bestandteilen: -->
        <xsl:variable name="no-simple-dot"
            select="replace(replace(replace($path,'/\./','/'),'^\./',''),'/\.$','')"/>
        <xsl:variable name="components">
            <xsl:analyze-string select="$no-simple-dot" regex="(/?[^/]+)">
                <xsl:matching-substring>
                    <comp>
                        <xsl:value-of select="."/>
                    </comp>
                </xsl:matching-substring>
                <xsl:non-matching-substring/>
            </xsl:analyze-string>
        </xsl:variable>
        <!-- Die Referenzen werden zusammengruppiert, die Nicht-Referenzen
            ebenso: -->
        <xsl:variable name="grouped">
            <xsl:for-each-group select="$components/*"
                group-starting-with="*[matches(.,'^/?\.\.$')][not(preceding-sibling::*[self::comp][1][matches(.,'^/?\.\.$')])] 
                | *[not(matches(.,'^/?\.\.$'))][preceding-sibling::*[self::comp][1][matches(.,'^/?\.\.$')]]">
                <xsl:choose>
                    <xsl:when test="matches(.,'^/?\.\.$')">
                        <ref n="{count(current-group())}">
                            <xsl:sequence select="current-group()"/>
                        </ref>
                    </xsl:when>
                    <xsl:otherwise>
                        <reg n="{count(current-group())}">
                            <xsl:sequence select="current-group()"/>
                        </reg>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:variable>
        <!-- Die Referenzen sowie die überflüssigen Pfadteile werden eliminiert: -->
        <xsl:variable name="resolved">
            <xsl:for-each select="$grouped/*">
                <xsl:choose>
                    <xsl:when test="self::ref"/>
                    <xsl:when test="self::reg and following-sibling::*[1][self::ref]">
                        <xsl:variable name="nr-of-kept-components"
                            select="number(@n) - number(following-sibling::*[1][self::ref]/@n)"/>
                        <xsl:sequence select="*[position() le $nr-of-kept-components]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence select="*"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <!-- Zum Schluss werden die Pfadteile konkateniert: -->
        <xsl:value-of select="string-join($resolved/*,'')"/>
    </xsl:function>
    
    <xsl:function name="tg:resolve-path-with-variable">
        <xsl:param name="path"/>
        <xsl:param name="variables"/>
        <xsl:choose>
            <!--<xsl:when test="$variables/variable[starts-with($path,concat('{$',@name,'}'))]">-->
            <xsl:when test="$variables/variable[starts-with($path,concat('{$',@name,'}'))]">
                <xsl:variable name="var"
                    select="$variables/variable[starts-with($path,concat('{$',@name,'}'))]"/>
                <xsl:value-of
                    select="concat($var/@value,
                    substring-after($path,concat('{$',$var/@name,'}')))"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$path"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="tg:resolve-path-with-variables-and-correct-path-sep">
        <xsl:param name="path"/>
        <xsl:param name="variables"/>
        <xsl:value-of select="tg:correct-path-sep(tg:resolve-path-with-variable($path,$variables))"/>
        <!--<xsl:message
            select="tg:correct-path-sep(tg:resolve-path-with-variable($path,$variables))"/>-->
    </xsl:function>





    <!-- funciton from: http://www.dpawson.co.uk/xsl/rev2/xpath2.html-->
    <xsl:function name="tg:getRelativePath" as="xs:string">
        <!-- Calculate relative path that gets from from source path to target path.
            
            Given:
            
            [1]  Target: /A/B/C
            Source: /A/B/C/X
            
            Return: "X"
            
            [2]  Target: /A/B/C
            Source: /E/F/G/X
            
            Return: "/E/F/G/X"
            
            [3]  Target: /A/B/C
            Source: /A/D/E/X
            
            Return: "../../D/E/X"
            
            [4]  Target: /A/B/C
            Source: /A/X
            
            Return: "../../X"
            
            
        -->

        <xsl:param name="source" as="xs:string"/>
        <!-- 
            Path to get relative path *from* -->
        <xsl:param name="target" as="xs:string"/>
        <!-- 
            Path to get relataive path *to* -->
        <xsl:if test="false()">
           <!-- <xsl:message> + DEBUG: local:getRelativePath(): Starting...</xsl:message>
            <xsl:message> + DEBUG: source="<xsl:value-of select="$source"/>"</xsl:message>
            <xsl:message> + DEBUG: target="<xsl:value-of select="$target"/>"</xsl:message>-->
        </xsl:if>
        <xsl:variable name="sourceTokens"
            select="tokenize((if
            (starts-with($source, '/')) then substring-after($source, '/') else
            $source), '/')"
            as="xs:string*"/>
        <xsl:variable name="targetTokens"
            select="tokenize((if
            (starts-with($target, '/')) then substring-after($target, '/') else
            $target), '/')"
            as="xs:string*"/>
        <xsl:choose>
            <xsl:when
                test="(count($sourceTokens) > 0 and count($targetTokens) >
                0) and 
                (($sourceTokens[1] != $targetTokens[1]) and
                (contains($sourceTokens[1], ':') or
                contains($targetTokens[1], ':')))">
                <!-- Must be absolute URLs with different schemes, cannot be
                    relative, return
                    target as is. -->
                <xsl:value-of select="$target"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="resultTokens"
                    select="tg:analyzePathTokens($sourceTokens, $targetTokens, ())" as="xs:string*"/>
                <xsl:variable name="result" select="string-join($resultTokens, '/')" as="xs:string"/>
                <xsl:value-of select="$result"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="tg:analyzePathTokens" as="xs:string*">
        <xsl:param name="sourceTokens" as="xs:string*"/>
        <xsl:param name="targetTokens" as="xs:string*"/>
        <xsl:param name="resultTokens" as="xs:string*"/>
        <xsl:if test="false()">
           <!-- <xsl:message> + DEBUG: local:analyzePathTokens(): Starting...</xsl:message>
            <xsl:message> + DEBUG: sourceTokens=<xsl:value-of
                    select="string-join($sourceTokens, ',')"/></xsl:message>
            <xsl:message> + DEBUG: targetTokens=<xsl:value-of
                    select="string-join($targetTokens, ',')"/></xsl:message>
            <xsl:message> + DEBUG: resultTokens=<xsl:value-of
                    select="string-join($resultTokens, ',')"/></xsl:message>-->
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count($sourceTokens) = 0">
                <!-- Append remaining target tokens (if any) to the result -->
                <xsl:sequence select="$resultTokens, $targetTokens"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- Still source tokens, so see if source[1] = target[1] -->
                <xsl:choose>
                    <!-- If they are equal, go to the next level in the paths: -->
                    <xsl:when
                        test="(count($targetTokens) > 0) and ($sourceTokens[1] =
                        $targetTokens[1])">
                        <xsl:sequence
                            select="tg:analyzePathTokens($sourceTokens[position() > 1],
                            $targetTokens[position() > 1], $resultTokens)"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- Paths must diverge at this point. Append one ".." for each
                            token
                            left in the source: -->
                        <xsl:variable name="goUps" as="xs:string*">
                            <xsl:for-each select="$sourceTokens[not(position() eq 0)]">
                                <xsl:sequence select="'..'"/>
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:sequence
                            select="string-join(($resultTokens, $goUps,
                            $targetTokens), '/')"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    
  

    <xsl:function name="tg:getLifedatesFromtext">
        <xsl:param name="date"/>
        <!--<xsl:message select="$date"></xsl:message>-->

        <xsl:variable name="dates">
            <xsl:if test="not(count($date) &gt; 1)">
                <xsl:choose>
                    <xsl:when test="matches($date/text(), '(^|^\D+)(\d{4})\D(\d{4})($|\D+$)')">
<!--                        <xsl:message select="'hier'"></xsl:message>-->
                        <tg:birthday>
                            <xsl:value-of
                                select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{4})($|\D+$)', '$2')"
                            />
                        </tg:birthday>
                        <tg:death>
                            <xsl:value-of
                                select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{4})($|\D+$)', '$3')"
                            />
                        </tg:death>
                    </xsl:when>
                    <xsl:when test="matches($date/text(), '(^|^\D+)(\d{4})\D(\d{4})\D(\d{4})($|\D+$)')">
                        <!--<xsl:message select="'hier'"></xsl:message>-->
                        <tg:birthday>
                            <xsl:value-of
                                select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{4})\D(\d{4})($|\D+$)', '$2')"
                            />
                        </tg:birthday>
                        <tg:death>
                            <xsl:value-of
                                select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{4})\D(\d{4})($|\D+$)', '$4')"
                            />
                        </tg:death>
                    </xsl:when>
                    <xsl:when test="matches($date/text(), '(^|^\D+)(\d{4})\D(\d{4})\D(\d{4})\D(\d{4})($|\D+$)')">
                        <!--<xsl:message select="'hier'"></xsl:message>-->
                        <tg:birthday>
                            <xsl:value-of
                                select="replace($date/text(), '(^|^\D+)(\d{4})\D(\d{4})\D(\d{4})\D(\d{4})($|\D+$)', '$2')"
                            />
                        </tg:birthday>
                        <tg:death>
                            <xsl:value-of
                                select="replace($date/text(), '(^|^\D+)(\d{4})\D(\d{4})\D(\d{4})\D(\d{4})($|(\D+$))', '$5')"
                            />
                        </tg:death>
                    </xsl:when>
                    <xsl:when
                        test="matches($date/text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)') and contains(normalize-space($date/text()), 'v. Chr.')">                        
                        <tg:birthday>
                            <xsl:variable name="value"
                                select="replace($date/text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)', '$2')"/>
                            <xsl:value-of select="concat('-0', $value)"/>                           
                        </tg:birthday>
                        <tg:death xlmns="tgbirthday">
                            <xsl:variable name="value_II"
                                select="replace($date/text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)', '$3')"/>
                            <xsl:value-of select="concat('-0', $value_II)"/>
                            <!--<xsl:message select="concat('-0', $value_II, 'juppie')"></xsl:message>-->
                        </tg:death>
                    </xsl:when>
                    <xsl:when test="matches($date/text(), '(^|(^\D+))(\d{3})\D(\d{3})($|\D+$)') ">
                        <tg:birthday>
                            <xsl:variable name="value"
                                select="replace($date/text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)', '$2')"/>
                            <xsl:value-of select="concat('0', $value )"/>
                        </tg:birthday>
                        <tg:death>
                            <xsl:variable name="value_II"
                                select="replace($date/text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)', '$3')"/>
                            <xsl:value-of select="concat('0', $value_II)"/>
                        </tg:death>
                    </xsl:when>
                    <xsl:when
                        test="matches($date/text(), '(^|\D+)((\d{4})\D(\d{2}))\D((\d{4})\D(\d{2}))($|\D+)')">
                        
                        <!--<xsl:message select="'jupi'"></xsl:message>-->
                        <xsl:variable name="approxbirthday"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D((\d{4})\D(\d{2})($|\D+))', '$2')"/>
                        <xsl:variable name="numbers"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D((\d{4})\D(\d{2}))($|\D+)', '$4')"/>
                        <!--<xsl:message select="$numbers"/>-->
                        <!--<xsl:message select="$numbers"></xsl:message>-->
                        <!--<xsl:variable name="firstNumber" select="replace($approxbirthday, '(^|\D+)(\d{4})\D(\d{2})$|\D+' , '$2')"/>-->
                        <!--<xsl:message select="$firstNumber"></xsl:message>-->
                        <xsl:variable name="firstPartSecondNumber"
                            select="replace($numbers, '(\d{2})(\D)(\d{2})' , '')"/>
                        <xsl:variable name="secondPartSecondNumber"
                            select="replace($numbers, '([\d]{4})(\D)([\d]{2})' , '$3')"/>
                        <xsl:variable name="approxdeath"
                            select="concat($firstPartSecondNumber, $secondPartSecondNumber)"/>
                        <tg:birthday>
                            <xsl:value-of select="$approxbirthday"/>
                        </tg:birthday>
                        <tg:death xlmns="tgbirthday">
                            <xsl:value-of select="$approxdeath"/>
                        </tg:death>
                    </xsl:when>
                    <xsl:when
                        test="matches($date/text(), '(^|\D+)(\d{4})\D((\d{4})\D(\d{2}))($|\D+)')">
                        
                        <xsl:variable name="approxbirthday"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D((\d{4})\D(\d{2}))($|\D+)', '$2')"/>
                        <xsl:variable name="numbers"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D((\d{4})\D(\d{2}))($|\D+)', '$3')"/>
                        <!--<xsl:message select="$numbers"/>-->
                        <!--<xsl:variable name="firstNumber" select="replace($approxbirthday, '(^|\D+)(\d{4})\D(\d{2})$|\D+' , '$2')"/>-->
                        <!--<xsl:message select="$firstNumber"></xsl:message>-->
                        <xsl:variable name="firstPartSecondNumber"
                            select="replace($numbers, '(\d{2})(\D)(\d{2})($|\D+)' , '')"/>
                        <xsl:variable name="secondPartSecondNumber"
                            select="replace($numbers, '([\d]{4})(\D)([\d]{2})' , '$3')"/>
                        <xsl:variable name="approxdeath"
                            select="concat($firstPartSecondNumber, $secondPartSecondNumber)"/>
                        <tg:birthday>
                            <xsl:value-of select="$approxbirthday"/>
                        </tg:birthday>
                        <tg:death>
                            <xsl:value-of select="$approxdeath"/>
                        </tg:death>
                    </xsl:when>
                    <xsl:when
                        test="matches($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D(\d{4})($|\D+)')">
                        <xsl:variable name="approxbirthday"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D(\d{4})($|\D+)', '$2')"/>
                        <!--<xsl:variable name="firstNumber" select="replace($approxbirthday, '(^|\D+)(\d{4})\D(\d{2})$|\D+' , '$2')"/>-->
<!--                        <xsl:message select="'hier'"></xsl:message>-->
                        <xsl:variable name="approxdeath"
                            select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D(\d{4})($|\D+)', '$4')"/>
                        <tg:birthday>
                            <xsl:value-of select="$approxbirthday"/>
                        </tg:birthday>
                        <tg:death>
                            <xsl:value-of
                                select="replace($date/text(), '(^|\D+)(\d{4})\D(\d{2})\D(\d{4})($|\D+)', '$4')"
                            />
                        </tg:death>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
        </xsl:variable>
        <!--<xsl:message select="$dates"/>-->
        <xsl:choose>
           <!-- <xsl:when test="contains($crturi, 'Literatur-Geibel') and (count($dates/tg:birthday) eq 1 and count($dates/tg:death) eq 1)">
               <xsl:variable name="dates"> <tg:birthday><xsl:value-of select="$dates/tg:death"/></tg:birthday>
                <tg:death><xsl:value-of select="$dates/tg:birthday"/></tg:death></xsl:variable>
                <xsl:copy-of select="$dates"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Cleland') and (count($dates/tg:birthday) eq 1 and count($dates/tg:death) eq 1)">
                <xsl:variable name="dates"> <tg:birthday><xsl:value-of select="'1709'"/></tg:birthday>
                    <tg:death><xsl:value-of select="'1789'"/></tg:death></xsl:variable>
                <xsl:copy-of select="$dates"/>
            </xsl:when>-->
            <xsl:when test="count($dates/tg:birthday) eq 1 and count($dates/tg:death) eq 1">
                <xsl:copy-of select="$dates"/>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>
