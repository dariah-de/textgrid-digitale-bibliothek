<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">

    <xsl:output indent="yes" use-character-maps="entities"/>

    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Adds the attribute werkebene.</xd:desc>
        <xd:desc>Adds @location to article-Elements and pnames.</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation.</doc:p>
        </doc:desc>
    </doc:doc>


    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <xsl:include href="subStructuralElements.xsl"/>
    <xsl:include href="exceptions.xsl"/>
    <xsl:include href="header.xsl"/>
    <xsl:include href="transitionalElements.xsl"/>
    <!-- ***********************************************************************************  -->


    <!-- ******************************** Other variabels **************************************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>Name of the author.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="author"
        select="replace(descendant::article[starts-with(@location, '/Literatur/M/')][1]/@location[1],
        '/[^/]+/[^/]+/([^/]+)(/.+)?',
        '$1'  )    
        (: $1  verweist auf die erste Klammer im regulären
        Ausdruck, also auf den dritten Pfadteil.
        /Literatur/M/Eichendorff, Joseph von/Biographie -> Eichendorff, Joseph von
        :) "/>
    <!-- ***********************************************************************************  -->


    <!-- *********************************** Entities **************************************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>Entitis must be used for some characters, which are not allowed in some
                TEI-attribute.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:character-map name="entities">
        <xsl:output-character character="&#150;" string="–"/>
        <xsl:output-character character="&#156;" string="œ"/>
        <xsl:output-character character="&#155;" string="›"/>
        <xsl:output-character character="&#139;" string="‹"/>
    </xsl:character-map>
    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xsl:template match="*" mode="#all" name="default">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="text()[normalize-space(.) eq '' and preceding-sibling::node()[1][self::text()[normalize-space(.) eq '']]]" mode="#all"></xsl:template>
    <xsl:template match="@*" mode="#all">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- ***********************************************************************************  -->

    <xsl:template match="/">

        <xsl:choose>
            <xsl:when test="contains($crt-uri,'Literatur-George' )">
                <xsl:apply-templates mode="Literatur-George"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri,'Literatur-Fontane' )">
                <xsl:apply-templates mode="Literatur-Fontane"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri,'Literatur-Keller' )">
                <xsl:apply-templates mode="Literatur-Keller"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Hebel' )">
                <xsl:apply-templates mode="Literatur-Hebel"/>
            </xsl:when>

            <xsl:when test="contains($crt-uri, 'Literatur-Knigge' )">
                <xsl:apply-templates mode="Literatur-Knigge"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Abschatz' )">
                <xsl:apply-templates mode="Literatur-Abschatz"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Ahlefeld' )">
                <xsl:apply-templates mode="Literatur-Ahlefeld"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Proelss' )">
                <xsl:apply-templates mode="Literatur-Proelss"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Abraham' )">
                <xsl:apply-templates mode="Literatur-Abraham"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Albert-Ludewig' )">
                <xsl:apply-templates mode="Albert-Ludewig"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Stahl' )">
                <xsl:apply-templates mode="Literatur-Stahl"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Luther' )">
                <xsl:apply-templates mode="Literatur-Luther"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Busch' )">
                <xsl:apply-templates mode="Literatur-Busch"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Schambach' )">
                <xsl:apply-templates mode="Literatur-Schambach"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Novalis' )">
                <xsl:apply-templates mode="Literatur-Novalis"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Tieck' )">
                <xsl:apply-templates mode="Literatur-Tieck"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Literatur-Pfitzer' )">
                <xsl:apply-templates mode="Literatur-Pfitzer"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Karl-Theodor' )">
                <xsl:apply-templates mode="Karl-Theodor"/>
            </xsl:when>

            <xsl:when test="contains($crt-uri, 'Literatur-Pauli' )">
                <xsl:apply-templates mode="Literatur-Pauli"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'May' )">
                <xsl:apply-templates mode="May"/>
            </xsl:when>
            <xsl:when test="contains($crt-uri, 'Lautensack' )">
                <xsl:apply-templates mode="Literatur-Lautensack"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="doc" mode="#all">
        <teiCorpus xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:call-template name="generate-id"/>
            <xsl:variable name="title">
                <xsl:if test="child::*[not(self::cat or self::article-metadata)][1][self::lem] ">
                    <xsl:value-of
                        select="child::*[not(self::cat or self::article-metadata)][1][self::lem]"/>
                </xsl:if>
            </xsl:variable>
            <xsl:call-template name="teiHeader">
                <xsl:with-param name="lem" tunnel="yes" select="$title"/>
            </xsl:call-template>
            <xsl:apply-templates select="node()" mode="#current"/>
        </teiCorpus>
    </xsl:template>

    <xsl:template match="articlegroup" mode="#all">
        <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:template>


    <!-- *********************************** Article to tei Corpus **********************************************************  -->
    <!-- article mit Nachfahren, die Werke sind, müssen als teiCorpus transformiert werden, egal ob sie selbst Werke sind -->
    <!-- Bei teiCorpus muss der front-Text weitergegeben werden. In allen anderen Fällen nicht. -->

    <xsl:template
        match="article[descendant::article[@werkebene eq 'yes']]"
        mode="#all">
        <xsl:param name="frontPart"/>
        <xsl:variable name="aggregateFront">
            <xsl:if test="not($frontPart eq '') and not(preceding-sibling::article)">
                <xsl:copy-of select="$frontPart"/>
            </xsl:if>
            <xsl:if test="(not(preceding-sibling::article[1] &gt;&gt; preceding-sibling::text[1] ) or
                (not(preceding-sibling::article))and preceding-sibling::text)">
                <!--<xsl:message select="text[not(preceding-sibling::article)]"/>-->
                <div type="front">
                    <xsl:apply-templates select="preceding-sibling::text"
                        mode="restructure-text-element"/>
                   <!-- <xsl:copy-of select="preceding-sibling::text"></xsl:copy-of>-->
                </div>
            </xsl:if>
        </xsl:variable>
        <teiCorpus xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="n" select="@location"/>
            <xsl:call-template name="tgID2xml"/>
            <xsl:variable name="title">
                <xsl:if test="child::*[not(self::cat)][1][self::lem] ">
                    <xsl:value-of select="child::*[not(self::cat)][1][self::lem]"/>
                </xsl:if>
            </xsl:variable>
            <xsl:call-template name="teiHeader">
                <xsl:with-param name="lem" tunnel="yes" select="$title"/>
                <xsl:with-param name="fullpath" tunnel="yes" select="@location"/>
            </xsl:call-template>
            <xsl:apply-templates select="node()[not(self::text[not(preceding-sibling::article)])]"
                mode="#current">
                <xsl:with-param name="frontPart" select="$aggregateFront"/>
            </xsl:apply-templates>
        </teiCorpus>
    </xsl:template>

    <!-- *************************** article to TEI or div *****************************************  -->
    <xsl:template
        match="article[not(descendant::article[@werkebene='yes'])]
        [@werkebene='yes'  
        or following-sibling::*[descendant-or-self::article[@werkebene='yes']]
        or preceding-sibling::*[descendant-or-self::article[@werkebene='yes']]]"
        name="article2Tei" mode="#all">
        <xsl:param name="frontPart"/>
        <xsl:variable name="aggregateFront">
            <xsl:if test="not($frontPart eq '') and not(preceding-sibling::article)">
                <xsl:copy-of select="$frontPart"/>
            </xsl:if>
          <!--  <xsl:if test="preceding-sibling::text">
                <div type="front" xmlns="http://www.tei-c.org/ns/1.0">
                    <xsl:apply-templates select="preceding-sibling::text/@*" mode="#current"/>
                    <xsl:apply-templates select="preceding-sibling::text" mode="#current"/>
                </div>
            </xsl:if>-->
        </xsl:variable>
        <TEI>
            <xsl:attribute name="n" select="@location"/>
            <xsl:call-template name="tgID2xml"/>
            <xsl:variable name="title">
                <xsl:if test="child::*[not(self::cat)][1][self::lem] ">
                    <xsl:value-of select="child::*[not(self::cat)][1][self::lem]"/>
                </xsl:if>
            </xsl:variable>
            <xsl:call-template name="teiHeader">
                <xsl:with-param name="lem" tunnel="yes" select="$title"/>
                <xsl:with-param name="fullpath" tunnel="yes" select="@location"/>
            </xsl:call-template>
            <text>
                <xsl:choose>
                  <!--  <xsl:when test="$aggregateFront ne ''and not(preceding-sibling::article)"></xsl:when>-->
                    <xsl:when test="$aggregateFront ne '' and not(preceding-sibling::article)">
                        <front>
                            <xsl:copy-of select="$aggregateFront"/>
                            <xsl:apply-templates select="preceding-sibling::text" mode="restructure-text-element"/>
                        </front>
                    </xsl:when>
                    <xsl:when test="(not(preceding-sibling::article[1] &gt;&gt; preceding-sibling::text ) or
                        not(preceding-sibling::article))and preceding-sibling::text">
                        <front>
                           <!-- <xsl:copy-of select="$aggregateFront"/>-->
                            <xsl:apply-templates select="preceding-sibling::text" mode="restructure-text-element"/>
                        </front>
                    </xsl:when>
                  <xsl:otherwise/>
                </xsl:choose>
                <body>
                    <!-- Ausnahme nur für Abschatz, entpsrechende startpage wir im startpage template entfernt -->
                    <xsl:if
                        test="contains($crturi, 'Literatur-Ahlefeld') and preceding-sibling::startpage[@nr eq '1']">
                        <pb>
                            <xsl:attribute name="n">
                                <xsl:value-of select="1"/>
                            </xsl:attribute>
                        </pb>
                    </xsl:if>
                    <div>
                        <!-- hier werden nur article-Elemente aufgerufen, eventuelle text-elemente werden im 
                        div-template als front ausgegeben-->
                        <xsl:apply-templates
                            select="article|text[not(following-sibling::article)]"
                            mode="#current"/>
                        
                    </div>
                </body>
            </text>
        </TEI>
    </xsl:template>
    <xsl:template
        match="article[@werkebene='no' and not(descendant::article[@werkebene='yes']
        or following-sibling::*[descendant-or-self::article[@werkebene='yes']]
        or preceding-sibling::*[descendant-or-self::article[@werkebene='yes']])]" mode="#all">
        <xsl:if test="(not(preceding-sibling::article[1] &gt;&gt; preceding-sibling::text[1]) or
            not(preceding-sibling::article))and preceding-sibling::text
            ">
            <div type="front">
                <xsl:apply-templates select="preceding-sibling::text" mode="restructure-text-element"/>
            </div>
        </xsl:if>
        <div>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:apply-templates select="node()[not(self::text[following-sibling::*[1][self::article]])]" mode="#current"> </xsl:apply-templates>
        </div>
    </xsl:template>

    <xsl:template match="article[count(article) eq 0 ][@tgID eq 'tg10']" mode="Literatur-Proelss">
        <xsl:call-template name="article2Tei">
            <xsl:with-param name="frontPart" select="''" tunnel="yes"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="generate-id">
        <xsl:attribute name="xml:id">
            <xsl:value-of select="concat('ne.', generate-id())"/>
        </xsl:attribute>
    </xsl:template>

</xsl:stylesheet>
