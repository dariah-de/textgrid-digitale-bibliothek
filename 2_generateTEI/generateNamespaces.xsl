<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    <xsl:output indent="yes"/>
    <!-- in pfaden .. zu viel -->
    
    <xsl:include href="../digibib_config+util_phil.xsl"/>
    
    
    <!--<Input: 09-mkfront >-->
    <!-- Output:  10-publication-->
    <doc:doc>
        <doc:desc>
            <doc:p>The name of the output folder for a single transformation step. </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:param name="output-subfolder" select="'generateNamespaces'"/>
    
    <!-- Uri of the transformed document -->
    <doc:doc>
        <doc:desc>
            <doc:p>Uri of the transformed document</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="crturi" select="document-uri(/)"/>
    
    <!-- folder, for xml-documents, which contain the metadata of one author's work. Metadata-documents are created with zeno2tei.xsl -->
    <doc:doc>
        <doc:desc>
            <doc:p>Folder, for xml-documents, which contain the metadata of one author's work.
                Metadata-documents are created with zeno2tei.xsl </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:param name="output-subfolder-metadata" select="'work-metadata-overwriteable'"/>
    
    <!-- path to metadata-folder -->
    <doc:doc>
        <doc:desc>
            <doc:p>path to metadata-folder </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="path-of-metadata-document"
        select="tg:get-output-path($crturi,$path-to-main-IO-folder,$output-subfolder-metadata)"/>
    
    <!-- path of the result document: depends on the outputsubfolder and the name of the author-xml-file.
        @var path-to main-IO-folder: defined in digibib_config+util_phil and config.xml.    
    -->
    <doc:doc>
        <doc:desc>
            <doc:p>Path of the result document: depends on the outputsubfolder and the name of the
                author-xml-file. @var path-to main-IO-folder: defined in digibib_config+util_phil
                and config.xml. </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="path-of-result-document"
        select="
        tg:get-output-path($crturi,$path-to-main-IO-folder,$output-subfolder)"/>
    
    <doc:doc>
        <doc:desc>
            <doc:p> Text-category as defined in the zeno-data</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="zeno-category"
        select="replace(substring-before($crturi,'/Text/'), '.+/','')"/>
    
    
    
    <!--<xsl:variable name="output_subdir_failures" select="'failures'"></xsl:variable>-->
    <!-- <xsl:variable name="output_failures" select="concat(
        tg:get-dir-uri(
        tg:get-output-path($crturi,$path_digibib_outputs,$input_subdir,$path_digibib_outputs,$output_subdir_failures)
        )
        ,'/',$basename_noExt)"/>
        <xsl:variable name="output_subdir_Datefailures" select="'Datefailures'"></xsl:variable>
        <xsl:variable name="output_Datefailures" select="concat(
        tg:get-dir-uri(
        tg:get-output-path($crturi,$path_digibib_outputs,$input_subdir,$path_digibib_outputs,$output_subdir_Datefailures)
        )
        ,'/',$basename_noExt)"/>
    -->
    
    <doc:doc>
        <doc:desc>
            <doc:p>Name of the author.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="author">
        <xsl:value-of select="tei:teiCorpus[1]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"
        />
    </xsl:variable>
    
    <xsl:template match="*" mode="#default" name="default">
        
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    
    <xsl:template match="@*" mode="#default">
        <xsl:copy-of select="."/>
    </xsl:template>
    <xsl:template match="/">
        
        <!--<xsl:message select="$_path_digibib_outputs"/>-->
        
        <xsl:result-document href="{$path-of-result-document}">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>