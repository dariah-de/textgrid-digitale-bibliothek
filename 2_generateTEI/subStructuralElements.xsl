<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">


    <xsl:template match="cat" mode="#all">
        <!-- <tei:milestone>
            <xsl:attribute name="unit">
                <xsl:value-of select="replace(@name, ': ', '_')"/>
            </xsl:attribute>
        </tei:milestone>-->
    </xsl:template>


    <!--  ********************** titles and heads ************************************** -->

    <xsl:template match="h1|h2|h3|h4|h5" priority="1" mode="#all">
        <tei:head>
            <xsl:attribute name="type">
                <xsl:value-of select="name(.)"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </tei:head>
    </xsl:template>

    <xsl:template match="lem" mode="#all">
        <xsl:comment>
            <xsl:value-of select="concat('lem: ', .)"/>
        </xsl:comment>
    </xsl:template>


    <!--  **************************** the text Element ************************************** -->
    <xsl:template match="text" mode="#all">
        <xsl:comment>
            <xsl:value-of select="concat('original text element was: ', @tgID)"/>
        </xsl:comment>
    <div type="text">
    
        <xsl:choose>
            <xsl:when test="not(*[name(.) eq 'h4' or name(.) eq 'h3'])">
                <div type="text">
                    <xsl:apply-templates mode="#current"/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each-group select="node()"
                    group-starting-with="*[name(.) eq 'h4' or name(.) eq 'h3'][name(.) ne name(preceding-sibling::*[1])]">
                    <xsl:variable name="crtgrp">
                        <xsl:sequence select="current-group()"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when
                            test="$crtgrp/node()[1][self::h4[matches(.,'^\[?(Fußnoten|Anmerkungen)\.?\]?$')]]">
                            <div type="footnotes">
                                <!--<xsl:copy-of select="current-group()"/>-->
                                <xsl:apply-templates  select="current-group()" mode="#current"/>
                            </div>
                        </xsl:when>
                        <xsl:when test="$crtgrp/node()[1][self::*[name(.) eq 'h4' or name(.) eq 'h3']]">
                            <div type="{name(.)}">
                                <xsl:apply-templates select="current-group()" mode="#current"/>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="current-group()" mode="#current"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each-group>
            </xsl:otherwise>
        </xsl:choose>
    </div>
    </xsl:template>


    <!--  *************************** ps and segmentations of p********************************** -->

    <xsl:template match="p" priority="1" mode="#all">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </p>
    </xsl:template>
    
    <xsl:template match="spa" mode="#all">
        <seg>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </seg>
    </xsl:template>
    
    <xsl:template match="span" mode="#all">
        <hi>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </hi>
    </xsl:template>

    <!--  *************************** breaks ********************************** -->

    <xsl:template match="br" mode="#all">
        <lb>
            <xsl:apply-templates select="@*" mode="#current"/>
        </lb>
    </xsl:template>

    <!--  ********************** Layout Elements ********************************* -->

    <xsl:template match="i" mode="#all">
        <hi>
            <xsl:attribute name="rend">
                <xsl:value-of select="'italics'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:apply-templates select="node()" mode="#current"/>
        </hi>
    </xsl:template>

    <xsl:template match="u" mode="#all">
        <hi>
            <xsl:attribute name="rend">
                <xsl:value-of select="'underline'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:apply-templates select="node()" mode="#current"/>
        </hi>       
    </xsl:template>

    <xsl:template match="b" priority="1" mode="#all">
        <hi>
            <xsl:attribute name="rend">
                <xsl:value-of select="'bold'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:apply-templates select="node()" mode="#current"/>
        </hi>
    </xsl:template>


    <!--  ******************************* pages **************************************** -->

    <xsl:template match="page|startpage" mode="#all">
        <pb>
            <xsl:attribute name="n">
                <xsl:value-of select="@nr"/>
            </xsl:attribute>
            <xsl:apply-templates select="@tgID" mode="#current"/>
        </pb>
    </xsl:template>
    
    
    <!--  ******************************* images and their text **************************************** -->

    <xsl:template match="image" mode="#all">
        <figure>
            <xsl:apply-templates select="imagetext"/>
            <graphic>
                <xsl:apply-templates select="@*" mode="#current"/>
            </graphic>
            <xsl:apply-templates select="node()[not(self::imagetext)]" mode="#current"/>
        </figure>
    </xsl:template>
    
    <!-- z.B. bei Busch -->
    <xsl:template match="imagefindtext" mode="#all">
        <ab>
            <xsl:attribute name="type">
                <xsl:value-of select="'imagetext'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </ab>
    </xsl:template>
    
    <xsl:template match="imagetext" mode="#all">
        <head>
            <xsl:apply-templates select="@*" mode="#current"/>
            <xsl:apply-templates mode="#current"/>
        </head>
    </xsl:template>



    <!--  ******************************* sub and superscripts **************************************** -->

    <xsl:template match="sub" mode="#all">
        <hi>
            <xsl:attribute name="rend">
                <xsl:value-of select="'subscript'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </hi>
    </xsl:template>
    
    <xsl:template match="sup" mode="#all">
        <hi>
            <xsl:attribute name="rend">
                <xsl:value-of select="'superscript'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </hi>
    </xsl:template>

    <!--  ******************************* Lists **************************************** -->

    <xsl:template match="ul[not(parent::ul)]" priority="1" mode="#all">
        <list>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </list>
    </xsl:template>

    <xsl:template match="ul[parent::ul]" mode="#all">
        <item>
            <xsl:apply-templates select="@*"/>
            <list>
                <xsl:apply-templates select="node()" mode="#current"/>
            </list>
        </item>
    </xsl:template>

    <xsl:template match="li" mode="#all">
        <item>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </item>
    </xsl:template>
    <xsl:template match="gallery">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <!--  ******************************** Tables ************************************** -->

    <xsl:template match="table" mode="#all">
        <table>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </table>
    </xsl:template>

    <xsl:template match="tr" mode="#all">
        <row>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </row>
    </xsl:template>

    <xsl:template match="td" mode="#all">
        <cell>
            <xsl:if test="@colspan">
                <xsl:attribute name="cols">
                    <xsl:value-of select="@colspan"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="@*[not(name() eq 'colspan')]|node()" mode="#current"/>
        </cell>
    </xsl:template>



    <!-- Enthält bei Gottsched Johann Verweis auf Fußnoten -->
    <xsl:template match="fnref" mode="#all">
        <cell>
            <xsl:if test="@colspan">
                <xsl:attribute name="cols">
                    <xsl:value-of select="@colspan"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="@*[not(name() eq 'colspan')]|node()" mode="#current"/>
        </cell>
    </xsl:template>


    <!-- ************************ Attributes ****************************************** -->
    <!-- <xsl:template name="attributes">
        <xsl:if test="@tgID">
            <xsl:attribute name="xml:id" select="@tgID"/>
        </xsl:if>
        </xsl:template>-->
    <xsl:template match="@id" mode="#all">
        <xsl:attribute name="corresp">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="@src" mode="#all">
        <xsl:attribute name="url">
            <xsl:value-of select="concat('../graphics/', .)"/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="@width" mode="#all">
        <xsl:attribute name="width">
            <xsl:value-of select="concat(., 'px')"/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="@display" mode="#all">
        <xsl:attribute name="rend">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="@werkebene" mode="#all"/>
    
    <xsl:template match="@location" name="location" mode="#all">
        <xsl:attribute name="n" select="."/>
    </xsl:template>
    
    <xsl:template name="tgID2xml">
        <xsl:attribute name="xml:id" select="@tgID"/>
    </xsl:template>
    
    <xsl:template match="@tgID" priority="2" mode="#all">
        <xsl:attribute name="xml:id" select="."/>
    </xsl:template>
    
    <xsl:template match="@class"  mode="#all">
        <xsl:attribute name="rendition" select="string-join(for $token in tokenize(., '\s+') return concat('#', $token), ' ')"/>
    </xsl:template>
    
    <xsl:template match="page/@nr|startpage/@nr" mode="#all"/>
    
    <xsl:template match="@tgprovided" mode="#all"/>
    
    <xsl:template match="ls" mode="#all">
        <xsl:value-of select="'&#x21FE;'"/>
    </xsl:template>
    
    <!--  align is attribute of cell -->
    <xsl:template match="@align | @align" priority="10" mode="#all">
        <xsl:attribute name="rend">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <!--  Attribute namespace="K" in figure in  Preolls -->
    <xsl:template match="@namespace" mode="#all"/>



    <!--  ************************************ Others ******************************* -->
    <xsl:template match="sigel" mode="#all">
        <!--<tei:p>
            <xsl:if test="@book and not(@book eq '')">   <xsl:attribute name="sameAs">
            <xsl:value-of select="'index'"/>
            </xsl:attribute>
            <xsl:attribute name="select">
            <xsl:value-of select="@book"/>
            </xsl:attribute></xsl:if>
            </tei:p>-->
    </xsl:template>
    <xsl:template match="article-metadata" mode="#all"/>
    <xsl:template match="document-metadata" mode="#all"/>
    <xsl:template match="catdiv" mode="#all">
        <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:template>

    <!--  ************************ META nur in PROELS *************************** -->
    <xsl:template match="META" mode="#all"/>
</xsl:stylesheet>
