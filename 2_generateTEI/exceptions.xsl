<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    <xsl:variable name="crt-uri" select="tokenize(document-uri(/), '/')[last()]"/>

    <!-- *******************************************  ************************************ -->
<!--    <xsl:variable name="crturi" select="document-uri(/)"/>    
-->    <xsl:template match="text[ following-sibling::article]" priority="2"/>

    <!-- *******************************************   Lists   ************************************ -->
    
    <xsl:template
        match="ul[not(parent::ul)][count(*) eq count(image|br)]" mode="Albert-Ludewig Literatur-Stahl Karoline" priority="2">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template
        match="ul[not(parent::ul)][count(*) eq count(image|br)]" mode="Albert-Ludewig" priority="2">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <!-- *******************************************   p   ************************************ -->
    
    <xsl:template match="p[h4]" mode="Literatur-Luther" priority="2">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <!-- *******************************************   Literatur-Busch   ************************************ -->
    
    <xsl:template match="b[not(parent::p)]" mode="Literatur-Busch" priority="2"/>
    <!-- ************************************************h1-h6 ******************************************* -->
    <xsl:template match="h1[@tgID eq 'tg1.2.8']" mode="Literatur-Schambach" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    
    <!-- *******************************************   Literatur-Busch   ************************************ -->
    
    <xsl:template match="h2[@tgID eq 'tg46.3.3']" mode="Literatur-Novalis" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    
    <xsl:template match="h2[@tgID eq 'tg965.3.4']" mode="Literatur-Tieck"  priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    
    <xsl:template match="h2[@tgID eq 'tg3.4.9']" mode="Literatur-Pfitzer" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="sameAs">
                <xsl:value-of select="'h3'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>    
    
    <!-- Mueller Karl Theodor -->
    <xsl:template match="h3[@tgID eq 'tg27.2.10']" mode="Karl-Theodor" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="sameAs">
                <xsl:value-of select="'h3'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    <xsl:template match="h3[text()[1] eq 'Schimpf und Ernst']" mode="Literatur-Pauli" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="sameAs">
                <xsl:value-of select="'h3'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    
    <xsl:template
        match="h3[text()[1] eq 'Karl May'][not(following-sibling::*[1][self::h2])][not(preceding-sibling::*[1][self::br[@tgID eq 'tg523.2.155']])]"
        mode="May" priority="2">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="sameAs">
                <xsl:value-of select="'h3'"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()"/>
        </p>
    </xsl:template>
    
    <xsl:template
        match="h4[normalize-space(text()[1]) eq 'Improvisationen der Liebe']" mode="Literatur-Lautensack" priority="2"/>
    <xsl:template match="article[preceding-sibling::*[1][self::h4[normalize-space(text()[1]) eq 'Improvisationen der Liebe']]]" priority="2" mode="Literatur-Lautensack">
        <div>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="preceding-sibling::*[1][self::h4[normalize-space(text()[1]) eq 'Improvisationen der Liebe']]" mode="restructure-Lautenbach"/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template
        match="h4[normalize-space(text()[1]) eq 'Improvisationen der Liebe']" mode="restructure-Lautenbach">
        <head type="added">
            <xsl:apply-templates select="@*|node()"/>
        </head>
    </xsl:template>
    
    
    <!-- ********************************* different Elements for Proells ************************************************ -->
    <!-- <xsl:template
        match="article[count(article) eq 0 ][@tgID eq 'tg10']" mode="Literatur-Proelss">
        <xsl:call-template name="article2Tei">
            <xsl:with-param name="frontPart" select="''" tunnel="yes"/>
        </xsl:call-template>
    </xsl:template>-->
    
    <xsl:template match="div"  mode="Literatur-Proelss">
        <div xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:if test="@class">
                <xsl:attribute name="rendition">
                    <xsl:value-of select="string-join(for $token in tokenize(@class, '\s+') return concat('#', $token), ' ')"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@fo.keep-together">
                <xsl:attribute name="rend">
                    <xsl:value-of select="@fo.keep-together"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates
                select="@*[not(name() eq 'class') and not(name() eq 'fo.keep-together')]|node()"/>
        </div>
    </xsl:template>
    
    
    <xsl:template mode="Literatur-Fontane" match="text()[starts-with (normalize-space(.), 'a) Eine Sibylle. (Ölbild, sehr dunkel.)')]">
        <item xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:value-of select="."/>
        </item> 
    </xsl:template>
    
    <xsl:template match ="text()[normalize-space (.) eq 'Unwandelbar Dein getreuer Isidor.']" mode="Literatur-Keller">
        <closer xmlns="http://www.tei-c.org/ns/1.0"><xsl:value-of select="."/>
        </closer>
    </xsl:template>
    
    <xsl:template match="startpage[@nr eq '1' and following-sibling::article ]"
        mode="Literatur-Abschatz Literatur-Ahlefeld"/>
    
    <xsl:template match="text()[normalize-space(.) eq '&#34;'][parent::text]" mode="Literatur-Hebel"/>
    
    <xsl:template match="text()[normalize-space(.) eq 'dabei.'][parent::text]"
        mode="Literatur-Knigge">
        <p xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:copy-of select="."/>
        </p>
    </xsl:template>
    
    
    <xsl:template match="page|startpage" mode="Literatur-Abraham" priority="1">
        <xsl:choose>
            <xsl:when
                test="matches(@nr, '^[0]$') and (preceding-sibling::article or following-sibling::article)">
                <xsl:comment>
                    <xsl:value-of select="concat('unclear pagebreak with nr:', @nr)"/>
                </xsl:comment>
            </xsl:when>
            <xsl:otherwise>
                <pb xmlns="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="n">
                        <xsl:value-of select="@nr"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="@*"/>
                </pb>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="page|startpage" mode="Literatur-Proelss" priority="1">
        <xsl:choose>
            <xsl:when
                test="matches(@nr, '^[12345]$') and (preceding-sibling::article or following-sibling::article)">
                <xsl:comment>
                    <xsl:value-of select="concat('unclear pagebreak with nr:', @nr)"/>
                </xsl:comment>
            </xsl:when>
            <xsl:otherwise>
                <pb xmlns="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="n">
                        <xsl:value-of select="@nr"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="@*"/>
                </pb>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
  
    
    
</xsl:stylesheet>
