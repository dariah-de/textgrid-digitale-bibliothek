<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" 
    exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    
    <xsl:template match="p[parent::text[following-sibling::article]][child::text()[starts-with(.,'&#8226;')]][plink]" mode="#all" priority="2"/>
    
    <xsl:template match="p[parent::text[following-sibling::article]][i]
        [preceding-sibling::*[1][self::p[child::text()[starts-with(.,'&#8226;')]][plink]]]" mode="#all" priority="2"></xsl:template>
    
     
  
    <xsl:template match="plink" mode="#all">
        <rs>          
            <xsl:attribute name="type" select="'plink'"/>                
            <xsl:attribute name="n" select="@href"/>
            <xsl:apply-templates select="@tgID"/>
            <xsl:apply-templates/>
        </rs>
    </xsl:template>
    <xsl:template match="pname"  mode="#all">
        <anchor>
            <xsl:attribute name="ana">
                <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:attribute name="type" select="'pname'"/>
            <!-- @location must be created here? -->
            <xsl:attribute name="n" select="@location"/>
            <xsl:apply-templates select="@tgID"/>
            <xsl:apply-templates/>
        </anchor>
    </xsl:template>
   

  
</xsl:stylesheet>