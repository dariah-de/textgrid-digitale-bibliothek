<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">

    <!-- ****************************** Variables ******************************************************* -->
    <!-- Introduce here the zeno-text-category -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Zeno-text-category</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="zeno-category" select="'Literatur'"/>


    <!-- ****************************** Variables containing all bookscites and plinks with metadata******************************************************* -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Extract the value of the plinks which contains the date of creation or the date of
                the first print</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="plinks">
        <plinks xmlns="">
            <xsl:for-each select="//p[child::plink][child::text()[starts-with(.,'&#8226;')]]">
                <description>
                    <!--                <xsl:copy-of select="./plink"/>-->
                    <xsl:copy-of select="."/>
                    <xsl:copy-of
                        select="./following-sibling::*[1][self::p[child::i][not(child::plink)]]"
                        copy-namespaces="no"/>
                </description>
            </xsl:for-each>
        </plinks>
    </xsl:variable>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>get all bookdescription of the catalogue file</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="books">
        <books xmlns="">
            <!-- the document-node of the catalogue file -->
            <xsl:variable name="booklist">
                <xsl:choose>
                    <xsl:when test="ends-with($crturi,'Literatur-Proelss,-Johannes.xml')">
                        <xsl:sequence select="/*/META//DEFBOOK"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence
                            select="document(replace(document-uri(/),'[^/]+$',concat($zeno-category,'-000 A.xml')))/*/META//DEFBOOK"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <!-- get the bookdescriptions of the books, to which points the identifier in the @book attribute of the sigel element -->
            <xsl:for-each select="distinct-values(//@book)">
                <xsl:sort/>
                <xsl:variable name="crtId" select="."/>
                <xsl:if test="string-length(.) gt 0">
                    <book bookid="{.}">
                        <xsl:copy-of
                            select="$booklist//DEFBOOK[BOOKNAME[matches(.,concat('^',string($crtId),'$'))]]"
                        />
                    </book>
                </xsl:if>
            </xsl:for-each>
        </books>
    </xsl:variable>
    <!-- ********************************************************************************************************************************** -->

    <!-- ********************************************************************************************************************************** -->
    <xsl:template name="teiHeader">
        <xsl:param name="lem" tunnel="yes"/>
        <!-- fullpath: Konkatenation der lems der verschachtelten article-elemente. plinks die Metadaten enthalten
        haben in zeno-Originaldaten den fullpath-Wert im @href-Attribut-->
        <xsl:param name="fullpath" tunnel="yes"/>
        <!-- get the title of the work from the lem-element -->
        <!-- get the category from the cat or catdiv element -->
        <!-- get information about date of creation and date of first print from the corresponding plink-element -->
        <xsl:variable name="article-metadata">
            <article-metadata xmlns="">
                <title>
                    <xsl:copy-of select="lem[1]/text()"/>
                </title>
                <xsl:for-each select="cat|catdiv">
                    <xsl:copy>
                        <xsl:copy-of select="@name"/>
                    </xsl:copy>
                </xsl:for-each>
                <xsl:copy-of select="$plinks//description[p[plink[@href = $fullpath]]]"/>
                <!--<xsl:copy-of select="$plinks//description[p[plink[@href=concat('/Literatur/M/',$fullpath)]]]"/>-->

            </article-metadata>
        </xsl:variable>
        <!-- get the  id of the source-book of the current work -->
        <xsl:variable name="crtbook-id">
            <xsl:choose>
                <xsl:when test="text/sigel[string-length(@book) gt 0]">
                    <xsl:value-of
                        select="text/sigel[string-length(@book) gt
                        0][1]/@book"
                    />
                </xsl:when>
                <!--  <xsl:when
                    test="preceding::sigel[string-length(@book) gt
                    0][1]/@book">
                    <xsl:value-of
                        select="preceding::sigel[string-length(@book) gt
                        0][1]/@book"
                    />
                </xsl:when>
                
                <xsl:when test="ancestor::article[text/sigel[string-length(@book) gt
                    0]]">
                    <xsl:value-of select="ancestor::article[text[1]/sigel[string-length(@book) gt
                        0]][1]/text[1]/sigel[string-length(@book) gt 0][1]/@book"/>
                </xsl:when>
                <xsl:when
                    test="not(preceding::sigel[string-length(@book) gt
                    0][1]) and $books/books[count(book) eq 1]">
                    <xsl:value-of select="$books/books/book[1]/@bookid"/>
                </xsl:when>-->
                <xsl:otherwise>
                    <xsl:value-of select="'missing bookID in edition'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!--<xsl:message select="$crtbook-id"></xsl:message>-->
        <!--<xsl:copy-of select="$books/ tg:book[1]"/>-->
        <!-- get the corresponding bookecite and bookdesc -->
        <xsl:variable name="crtbook">
            <crtbook>
                <xsl:copy-of select="$books/books/book[@bookid eq $crtbook-id]"/>
            </crtbook>
        </xsl:variable>
        <xsl:variable name="bookcite">
            <xsl:copy-of select="$crtbook//DEFBOOK/BOOKCITE"/>
        </xsl:variable>
        <xsl:variable name="bookdesc">
            <xsl:copy-of select="$crtbook//DEFBOOK/BOOKDESCR"/>
        </xsl:variable>
        <teiHeader>
            <xsl:if test="@werkebene eq 'yes'">
                <xsl:attribute name="type" select="'work'"/>
            </xsl:if>
            <fileDesc>
                <titleStmt>
                    <title>
                        <xsl:value-of select="$lem"/>
                    </title>
                    <author>
                        <xsl:value-of select="$author"/>
                    </author>
                </titleStmt>
                <publicationStmt>
                    <authority>www.textGrid.de</authority>
                    <availability>
                        <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten
                            sowie davon einzeln zugängliche Teile sind eine Abwandlung des
                            Datenbestandes von www.editura.de durch TextGrid und werden unter der
                            Lizenz Creative Commons Namensnennung 3.0 Deutschland Lizenz (by-Nennung
                            TextGrid, www.editura.de) veröffentlicht. Die Lizenz bezieht sich nicht
                            auf die der Annotation zu Grunde liegenden allgemeinfreien Texte (Siehe
                            auch Punkt 2 der Lizenzbestimmungen). </p>
                        <p>
                            <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode"
                                >Lizenzvertrag</ref>
                        </p>
                        <p>
                            <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine
                                vereinfachte Zusammenfassung des rechtsverbindlichen Lizenzvertrages
                                in allgemeinverständlicher Sprache </ref>
                        </p>
                        <p>
                            <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur
                                Lizenz und zur Digitalen Bibliothek</ref>
                        </p>
                    </availability>
                </publicationStmt>
                <xsl:if test="$article-metadata//description[p[not(plink)]]">
                    <notesStmt>
                        <xsl:apply-templates select="$article-metadata//description" mode="header"/>
                    </notesStmt>
                </xsl:if>
                <sourceDesc>
                    <xsl:choose>
                        <xsl:when test="$lem eq 'Biographie'">
                            <p>
                                <xsl:copy-of select="$lem"/>
                            </p>
                        </xsl:when>
                        <!--  <xsl:when test=" @werkebene eq 'yes' and ($bookcite//BOOKCITE or $bookdesc//BOOKDESCR or $crtbook//book[count(*) eq 0])">-->
                        <xsl:when
                            test="$bookcite//BOOKCITE or $bookdesc//BOOKDESCR or $crtbook//book[count(*) eq 0]">
                            <biblFull>
                                <!--<xsl:message select="$bookdesc"></xsl:message>-->
                                <titleStmt>
                                    <xsl:apply-templates
                                        select="$bookcite//BOOKCITE | $bookdesc//BOOKDESCR"/>
                                    <xsl:if test="$crtbook//book[count(*) eq 0][@bookid ne '']">
                                        <xsl:apply-templates
                                            select="$crtbook//book[count(*) eq 0][string-length(@bookid) gt 0]/@bookid"
                                        />
                                    </xsl:if>
                                    <author>
                                        <xsl:value-of select="$author"/>
                                    </author>
                                </titleStmt>
                                <publicationStmt>
                                    <p/>
                                </publicationStmt>
                            </biblFull>
                        </xsl:when>
                        <xsl:when test="@werkebene eq 'yes'">
                            <biblFull>
                                <titleStmt>
                                    <title/>
                                    <author>
                                        <xsl:value-of select="$author"/>
                                    </author>
                                </titleStmt>
                                <publicationStmt>
                                    <p/>
                                </publicationStmt>
                            </biblFull>
                        </xsl:when>
                        <xsl:otherwise>
                            <p/>
                        </xsl:otherwise>
                    </xsl:choose>
                </sourceDesc>
            </fileDesc>
        </teiHeader>
    </xsl:template>


    <xsl:template match="BOOKDESCR | BOOKCITE">
        <title>
            <xsl:value-of select="."/>
        </title>
    </xsl:template>
    <xsl:template match="@bookid">
        <title>
            <xsl:value-of select="."/>
        </title>
    </xsl:template>


    <!-- ***************************** mode header ******************************************* -->
    <xd:doc>
        <xd:desc>templates for: article-metadata/description/p </xd:desc>
    </xd:doc>
    <xsl:template match="description" mode="header" priority="2">
        <xsl:apply-templates select="p[not(plink)]" mode="header"/>
    </xsl:template>
    <xsl:template match="p[i]" mode="header" priority="2">
        <note>
            <xsl:value-of select="i"/>
        </note>
    </xsl:template>
    <xsl:template match="p[not(i)]" mode="header" priority="2">
        <note>
            <xsl:value-of select="p"/>
        </note>
    </xsl:template>
</xsl:stylesheet>
