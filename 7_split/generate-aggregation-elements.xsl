<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns="http://textgrid.info/namespaces/metadata/core/2010" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    <!-- aggregate TEI-Elements as aggregations (with grafics) or items (without grafics) -->
    
    
    <xd:doc>
        <xd:desc>
            <xd:p>This is only called for non-work TEI elements.</xd:p>
            <xd:p>Test if an TEI-Element contains graphics or not.</xd:p>
            <xd:p>If it contains graphics aggregate is as aggregation and generate not only the
                corresponding item but also the corresponding aggregation.</xd:p>
            <xd:p>Otherwise aggregate it as item.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="generate-aggregation-elements">
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="path"/>
        <xsl:param name="rel-path"/>
        <xsl:choose>
            <!-- Aggregate the current TEI-Element as aggregation if it contains graphics, generate the corresponding aggregation and aggregation
                meta-files -->
            <xsl:when test=".//tei:graphic">
                <ore:aggregates>
                    <xsl:attribute name="rdf:resource" select="concat($rel-path, '.aggregation')"/>
                </ore:aggregates>                
                <xsl:call-template name="generate-aggr-meta">
                    <xsl:with-param name="path" select="$path"/>
                    <xsl:with-param name="extension" select="'.aggregation.meta'"/>                    
                </xsl:call-template>
                <!-- Generate an aggregation for the current TEI-Element that aggregates the current-TEI-item and the graphic files -->
                <xsl:call-template name="generate-aggregation">
                    <xsl:with-param name="aggr">
                        <xsl:call-template name="generate-aggregation-elements-graphics">
                            <xsl:with-param name="rel-path" select="$rel-path"/>
                        </xsl:call-template>
                    </xsl:with-param>
                    <xsl:with-param name="path" select="$path"/>
                </xsl:call-template>
            </xsl:when>
            <!-- otherwise aggregate it as item  -->
            <xsl:otherwise>
                <ore:aggregates>
                    <xsl:attribute name="rdf:resource" select="concat($rel-path, '.item.xml')"/>
                </ore:aggregates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>
            <xd:p>This is only called for work TEI elements.</xd:p>
            <xd:p>Generates an edition that aggregates the corresponding item and, if there are any graphics, the graphics.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="generate-ed-aggregation-elements">
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="path"/>
        <xsl:param name="rel-path"/>
        
        <ore:aggregates rdf:resource="{concat($rel-path, '.edition')}"/>

        
        <xsl:call-template name="generate-edition-meta">
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:choose>
            <!-- Aggregate the current TEI-Element as aggregation if it contains graphics, generate the corresponding aggregation and aggregation
                meta-files -->
            <xsl:when test=".//tei:graphic">                
                <!-- Generate an aggregation for the current TEI-Element that aggregates the current-TEI-item and the graphic files -->
                <xsl:call-template name="generate-edition">
                    <xsl:with-param name="aggr">
                        <xsl:call-template name="generate-aggregation-elements-graphics">
                            <xsl:with-param name="rel-path" select="$rel-path"/>
                        </xsl:call-template>
                    </xsl:with-param>
                    <xsl:with-param name="path" select="$path"/>
                </xsl:call-template>
            </xsl:when>
            <!-- otherwise aggregate it as single item  -->
            <xsl:otherwise>
                <xsl:call-template name="generate-edition">
                    <xsl:with-param name="aggr">
                        <ore:aggregates rdf:resource="{concat($rel-path, '.edition')}"/>
                        <ore:aggregates rdf:resource="{concat($rel-path, '.item.xml')}"/>
                    </xsl:with-param>
                    <xsl:with-param name="path" select="$path"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>This template is called, if an TEI-Element is an aggregation, because it contains
                graphics.</xd:p>
            <xd:p>In that aggregation the TEI-item it self and the graphics must be
                aggregated</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="generate-aggregation-elements-graphics">
        <xsl:param name="rel-path"/>
        <!-- aggregate the current TEI-Element-item -->
        <ore:aggregates>
            <xsl:attribute name="rdf:resource" select="concat($rel-path, '.item.xml')"/>
        </ore:aggregates>
        <!-- aggregate the graphics of the current-TEI-Element -->
        <xsl:for-each-group select=".//tei:graphic" group-by="@url">
            <xsl:if test="current-grouping-key() ne ''">
                <ore:aggregates rdf:resource="{current-grouping-key()}"/>
            </xsl:if>
        </xsl:for-each-group>
    </xsl:template>
    
    
    
</xsl:stylesheet>