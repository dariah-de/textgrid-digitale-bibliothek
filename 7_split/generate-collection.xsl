<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://textgrid.info/namespaces/metadata/core/2010"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    <xsl:template name="generate-collection">
        <xsl:param name="title" tunnel="yes"/>
        <xsl:param name="path"/>  
        <xsl:result-document
            href="{concat($path, '.collection')}">
            <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:ore="http://www.openarchives.org/ore/terms/">
                <rdf:Description>
                    <xsl:apply-templates select="TEI|teiCorpus" mode="#current"/> 
                </rdf:Description>
            </rdf:RDF>
        </xsl:result-document>
    </xsl:template>
    
    
    <!-- ********** Fuer Bilder angeben: Format beim Anrufen der Templates -->
    <xsl:template name="generate-collection-meta">
        <xsl:param name="path"/>
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="title"/>
        <xsl:result-document href="{concat($path, '.collection.meta')}">
            <object>
                <generic>
                    <title><xsl:value-of select="$author"/></title>
                    <format>text/tg.collection+tg.aggregation+xml</format>
                </generic>
                <collection>
                    <tns:collector>Editura</tns:collector>
                </collection>
                <relations>
                    <rdf:RDF xmlns:tg="http://textgrid.info/relation-ns#"
                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description>
                            <xsl:attribute name="rdf:about"
                                select="concat($path,'.collection')"/>
                            <xsl:variable name="portrait" select="(//tei:TEI[descendant::tei:fileDesc/tei:titleStmt/tei:title[text() eq 'Biographie']]//tei:graphic)[1]/@url"/>
                            <xsl:if test="$portrait">                                
                                <tg:depiction rdf:resource="../graphics/{$portrait}"/>
                            </xsl:if>
                        </rdf:Description>
                    </rdf:RDF>
                </relations>
            </object>
        </xsl:result-document>
    </xsl:template>
    
    
</xsl:stylesheet>
