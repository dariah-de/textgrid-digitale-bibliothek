<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
   <xsl:template name="generate-graphic-meta">
       <xsl:param name="header" tunnel="yes"/>
       <xsl:param name="path"></xsl:param>
       <xsl:result-document href="{$path}/../{@url}.meta" indent="yes">
        <tns:object>
            <tns:generic>
                <tns:provided>
                    <tns:title>
                        <xsl:choose>
                            <xsl:when test="parent::tei:figure/tei:ab">
                                <xsl:value-of select="parent::tei:figure/tei:ab"/>
                            </xsl:when>
                            <xsl:when
                                test="parent::tei:figure/following-sibling::tei:head[@type eq 'h1'][1]">
                                <xsl:value-of
                                    select="parent::tei:figure/following-sibling::tei:head[@type eq 'h1'][1]"
                                />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="'[Kein Titel]'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tns:title>
                    <!-- <identifier/> -->
                    <tns:format>
                        <xsl:value-of select="'image/jpeg'"/>
                    </tns:format>
                </tns:provided>
            </tns:generic>
            <tns:item>
                <tns:rightsHolder>TextGrid</tns:rightsHolder>
            </tns:item>
        </tns:object>
       </xsl:result-document>
   </xsl:template>
    
    <xsl:function name="tg:generate-graphic-title">
        <xsl:param name="string"/>
        <xsl:choose>
            <xsl:when test="matches($string, '\.|,|;')">
                <xsl:value-of select="tokenize($string, '\.|,|;')[1]"/>
            </xsl:when>
            <xsl:when test="matches(normalize-space($string), '\s')">
                <xsl:value-of select="tokenize($string, '\s')[1]"/>
            </xsl:when>
            <xsl:when test="$string and string-length($string) &lt; 50">
                <xsl:value-of select="$string"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'[kein Titel]'"></xsl:value-of>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>