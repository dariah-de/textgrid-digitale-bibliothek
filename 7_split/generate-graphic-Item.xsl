<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns="http://textgrid.info/namespaces/metadata/core/2010" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
   <xsl:template name="generate-grapic-item"> 
    <xsl:result-document href="{concat($splitDir, '/', 'graphics/', @url, '.meta')}"
        indent="yes">
        <tns:object>
            <tns:generic>
                <tns:provided>
                    <tns:title>
                        <xsl:choose>
                            <xsl:when test="parent::tei:figure/tei:ab">
                                <xsl:value-of select="parent::tei:figure/tei:ab"/>
                            </xsl:when>
                            <xsl:when
                                test="parent::tei:figure/following-sibling::tei:head[@type eq 'h1'][1]">
                                <xsl:value-of
                                    select="parent::tei:figure/following-sibling::tei:head[@type eq 'h1'][1]"
                                />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="'[Kein Titel]'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tns:title>
                    <!-- <identifier/> -->
                    <tns:format>
                        <xsl:value-of select="'image/jpeg'"/>
                    </tns:format>
                </tns:provided>
            </tns:generic>
            <tns:item>
                <tns:rightsHolder>TextGrid</tns:rightsHolder>
            </tns:item>
        </tns:object>
    </xsl:result-document></xsl:template>
</xsl:stylesheet>