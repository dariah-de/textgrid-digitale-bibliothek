<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://textgrid.info/namespaces/metadata/core/2010"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    
    
    
    <xsl:template name="generate-item">
        <xsl:param name="path"/>
        <xsl:result-document href="{concat($path, '.item.xml')}">
            <xsl:copy-of select="."/>
        </xsl:result-document>
    </xsl:template>
    
    
    <!-- ********** Fuer Bilder angeben: Format beim Anrufen der Templates -->
    <xsl:template name="generate-item-meta">
        <xsl:param name="path"/>
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="extension"/>
      <!--  <xsl:message select="$header"></xsl:message>-->
        <xsl:result-document href="{concat($path, '.item.xml.meta')}">
            <object>
                <generic>
                    <xsl:call-template name="generate-generic">
                        <xsl:with-param name="format" select="'text/xml'"/>
                    </xsl:call-template>
                </generic>
                <item>
                    <rightsHolder>TextGrid</rightsHolder>
                </item>
            </object>
        </xsl:result-document>
    </xsl:template>
    
</xsl:stylesheet>
