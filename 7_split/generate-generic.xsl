<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://textgrid.info/namespaces/metadata/core/2010"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    
    
    
    <xsl:template name="generate-generic">
        <!-- add more params if necessary -->
        <xsl:param name="format"></xsl:param>
        <xsl:param name="header" tunnel="yes"/>
       
        <xsl:for-each select="$header//tei:fileDesc/tei:titleStmt/tei:title">
            <title>
                <xsl:value-of select="."/>
            </title>
        </xsl:for-each>
        <format>
            <xsl:value-of select="$format"/>
        </format>
        <xsl:for-each select="$header//tei:fileDesc/tei:notesStmt/tei:note">
            <note>
                <xsl:value-of select="."/>
            </note>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>