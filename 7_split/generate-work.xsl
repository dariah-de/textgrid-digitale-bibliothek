<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://textgrid.info/namespaces/metadata/core/2010"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    <xsl:template name="generate-work-meta">
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="path"/>
        <xsl:result-document href="{concat($path, '.work.meta')}">
            <object>
                <generic>
                    <xsl:call-template name="generate-generic">
                        <xsl:with-param name="format" select="'text/tg.work+xml'"/>
                    </xsl:call-template>
                </generic>
                <work>
                    <agent role="author">
                        <xsl:variable name="author">
                            <xsl:sequence select="$header/tei:fileDesc/titleStmt/tei:author"/>
                        </xsl:variable>
                        <xsl:if test="$author/*/@key">
                            <xsl:attribute name="id" select="$author/*/@key"/>
                        </xsl:if>
                        <xsl:value-of select="$author"/>
                    </agent>
                    <xsl:if test="$header/tei:profileDesc/tei:creation/tei:date">
                        <xsl:call-template name="dateFormat">
                            <xsl:with-param name="date"
                                select="$header/tei:profileDesc/tei:creation/tei:date"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="$header/tei:profileDesc/tei:textClass/tei:keywords/tei:term">
                        <genre>
                            <xsl:value-of select="$header/tei:profileDesc/tei:textClass/tei:keywords/tei:term"/>
                        </genre>
                    </xsl:if>
                </work>
            </object>
        </xsl:result-document>
    </xsl:template>
    
    
    <!-- ***********************work************************ -->
    
    <xsl:template match="tei:profileDesc/tei:date" mode="work">
        <xsl:call-template name="dateFormat">
            <xsl:with-param name="date" select="."/>
        </xsl:call-template>
    </xsl:template>
    <xsl:template name="generate-work">
        <xsl:param name="path"/>
        <xsl:result-document href="{concat($path, '.work')}"/>
    </xsl:template>
    
    <xsl:template name="dateFormat">
        <xsl:param name="date"/>
        <xsl:choose>
            <xsl:when test="$date/*/@when">
                <date>
                    <xsl:copy-of select="$date/@when"/>
                </date>
            </xsl:when>
            <xsl:otherwise>
                <date>
                    <xsl:copy-of select="$date/@notBefore"/>
                    <xsl:copy-of select="$date/@notAfter"/>
                </date>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>