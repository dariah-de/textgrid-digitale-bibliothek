<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns="http://textgrid.info/namespaces/metadata/core/2010" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">

    <xsl:param name="splitDir"/>

    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Adds an ID to each zeno-Element. For this purpose the elements are counted
            hierarchally.</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->

    <!-- Extracts sth like Literatur-George,-Stefan -->
    <xsl:variable name="authordir"
        select="replace(document-uri(/), '^.*/(.*)\.xml$', '$1')"/>
    

    <!-- ********************** Output, includes and imports   *********************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </xd:doc>
    <!--<xsl:param name="output-subfolder" select="'01_tgID'"/>-->

    <xsl:output indent="yes"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <xsl:include href="generate-aggregation.xsl"/>
    <xsl:include href="generate-edition.xsl"/>
    <xsl:include href="generate-work.xsl"/>
    <xsl:include href="generate-item.xsl"/>
    <xsl:include href="generate-generic.xsl"/>
    <xsl:include href="generate-aggregation-elements.xsl"/>
    <xsl:include href="generate-graphic-Item.xsl"/>
    <xsl:include href="generate-graphic-Meta.xsl"/>
    <xsl:include href="generate-collection.xsl"/>
    <!-- ***********************************************************************************  -->
    <xsl:variable name="author">
        <xsl:choose>
            <xsl:when test="contains($crturi, 'Literatur-Goethe')">
                <xsl:value-of select="'Goethe, Johann Wolfgang von'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of
                    select="/tei:teiCorpus[1]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <!--<xsl:variable name="author-dir" select="concat($author, '/')"/>-->

    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@*|comment()|processing-instruction()">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- **************************************************************************************************  -->
    <xsl:variable name="licenceText"> Der annotierte Datenbestand der Digitalen Bibliothek inklusive
        Metadaten sowie davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
        www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons Namensnennung 3.0
        Deutschland Lizenz (by-Nennung TextGrid) veröffentlicht. Die Lizenz bezieht sich nicht auf
        die der Annotation zu Grunde liegenden allgemeinfreien Texte (Siehe auch Punkt 2 der
        Lizenzbestimmungen).</xsl:variable>

<!-- Don't create the colletion for the root -->
    <xsl:template match="/tei:teiCorpus" priority="2">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="/tei:teiCorpus/tei:teiCorpus" priority="2">

        <xsl:variable name="header" select="tei:teiHeader"/>
        <xsl:variable name="path" select="tg:generate-Path($author, 'collection')"/>

        <xsl:call-template name="mapping-table"/>
        
        <!-- ************************* generate the graphic-meta files for the whole enzyklo *************************-->
        <xsl:for-each-group select=".//tei:graphic" group-by="@url">
            <!--  <xsl:variable name="title" select=".[tokenize(head, '\.|,|;')][1]"/>-->            
            <xsl:if test="current-grouping-key() ne ''">
                <xsl:call-template name="generate-graphic-meta">
                    <xsl:with-param name="path" select="$path"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each-group>
        <xsl:call-template name="generate-collection">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title">
                <tei:title>
                    <xsl:copy-of select="$author"/>
                </tei:title>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="generate-collection-meta">
            <xsl:with-param name="header" select="$header"/>
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title">
                <tei:title>
                    <xsl:copy-of select="$author"/>
                </tei:title>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="tei:teiCorpus[tei:teiHeader[@type eq 'work']]" priority="1">
        <xsl:variable name="header" select="tei:teiHeader"/>
        <xsl:variable name="path" select="tg:generate-Path($author, @xml:id)"/>
        <ore:aggregates>
            <xsl:attribute name="rdf:resource">
                <xsl:value-of select="concat(tg:generate-rel-Path($author,@xml:id), '.edition')"/>
            </xsl:attribute>
        </ore:aggregates>
        <xsl:call-template name="generate-edition-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
        <xsl:call-template name="generate-edition">
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
        <xsl:call-template name="generate-work-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
        <xsl:call-template name="generate-work">
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
    </xsl:template>


    <xsl:template match="tei:teiCorpus">
        <xsl:variable name="header" select="tei:teiHeader"/>
        <xsl:variable name="path" select="tg:generate-Path($author, @xml:id)"/>
        <xsl:variable name="title" select="$header/tei:fileDesc/tei:titleStmt/tei:title"/>
        <ore:aggregates>
            <xsl:attribute name="rdf:resource">
                <xsl:value-of select="concat(tg:generate-rel-Path($author,@xml:id), '.aggregation')"
                />
            </xsl:attribute>
        </ore:aggregates>
        <xsl:call-template name="generate-aggr-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
        <xsl:call-template name="generate-aggregation">
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
    </xsl:template>



    <xsl:template match="tei:TEI[tei:teiHeader[@type eq 'work']]" priority="1">
        <xsl:variable name="header" select="tei:teiHeader"/>
        <xsl:variable name="path" select="tg:generate-Path($author, @xml:id)"/>
        <!--<xsl:call-template name="generate-edition-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
        <xsl:call-template name="generate-edition">
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>-->
        <xsl:call-template name="generate-work-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-work">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-item-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-item">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-ed-aggregation-elements">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="rel-path" select="tg:rel-Path-of($path)"/>
        </xsl:call-template>
        <!-- <xsl:call-template name=""></xsl:call-template>-->
    </xsl:template>

    <xsl:template match="tei:TEI">
        <xsl:variable name="header" select="tei:teiHeader"/>
        <xsl:variable name="path" select="tg:generate-Path($author, @xml:id)"/>


        <xsl:call-template name="generate-item-meta">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-item">
            <xsl:with-param name="header" select="$header" tunnel="yes"/>
            <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>

        <xsl:call-template name="generate-aggregation-elements">
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="rel-path" select="tg:rel-Path-of($path)"/>
        </xsl:call-template>
        <!-- <xsl:call-template name=""></xsl:call-template>-->
    </xsl:template>


    <xsl:function name="tg:generate-Path">
        <xsl:param name="param-author"/>
        <xsl:param name="id"/>
        <xsl:value-of select="concat($authordir, '/', $id, '/', $id)"/>
    </xsl:function>
    <xsl:function name="tg:generate-rel-Path">
        <xsl:param name="param-author"/>
        <xsl:param name="id"/>
        <xsl:value-of select="concat('../', (: $param-author, '/', :) $id, '/', $id)"/>
    </xsl:function>
    <xsl:function name="tg:rel-Path-of">
        <xsl:param name="path"/>
        <xsl:value-of select="replace($path, '^[^/]*/', '../')"/>
    </xsl:function>




    <!--<xsl:template name="generate-aggregation">
        <xsl:param name="path"/>
        <xsl:result-document href="{concat($path, '.agregation')}">
            <rdf:RDF>
                <rdf:Description>
                    <xsl:apply-templates select="tei:TEI|tei:teiCorpus"/>
                </rdf:Description>
            </rdf:RDF>
        </xsl:result-document>
    </xsl:template>-->


    <!-- 
        The following functions and templates generate a mapping table that map old 
        to new filenames. Will be required to map data that has already been imported
        to new versions.
    -->

    <xsl:function name="tg:old-path">
        <xsl:param name="elem"/>
        <xsl:variable name="authordir"
            select="replace(document-uri(root($elem)), '^.*/(.*)\.xml$', '$1')"/>
        <xsl:variable name="n">
            <xsl:number select="$elem" count="tei:teiCorpus|tei:TEI" level="any" format="00001"/>
        </xsl:variable>
        <xsl:value-of select="concat($authordir, '/', $n, '/', $n)"/>
    </xsl:function>

    <xsl:function name="tg:csv-row">
        <xsl:param name="fields"/>
        <xsl:variable name="sep">","</xsl:variable>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="string-join($fields, $sep)"/>
        <xsl:text>"&#10;</xsl:text>
    </xsl:function>

    <xsl:function name="tg:mapping-row">
        <xsl:param name="old"/>
        <xsl:param name="new"/>
        <xsl:param name="extensions"/>
        <xsl:param name="title"/>
        <xsl:for-each select="tokenize($extensions, '\s+')">
            <xsl:value-of select="tg:csv-row((concat($old, '.', .), concat($new, '.', .), $title))"
            />
        </xsl:for-each>
    </xsl:function>

    <xsl:template name="mapping-table">
        <xsl:result-document href="{$authordir}/{$authordir}.map.csv" method="text" encoding="utf8">
            <xsl:text>"Old","New","Title"&#10;</xsl:text>
            <xsl:for-each select=".//(tei:teiCorpus|tei:TEI)">
                <xsl:variable name="extensions">
                    <xsl:choose>
                        <xsl:when test="self::TEI[tei:teiHeader[@type eq 'work']]">
                            <xsl:sequence select="('item.xml', 'edition', 'work')"/>
                        </xsl:when>
                        <xsl:when test="self::tei:TEI">
                            <xsl:sequence select="'item.xml'"/>
                        </xsl:when>
                        <xsl:when test="self::tei:teiCorpus[tei:teiHeader[@type eq 'work']]">
                            <xsl:sequence select="('edition', 'work')"/>
                        </xsl:when>
                        <xsl:when test="self::tei:teiCorpus">
                            <xsl:sequence select="'aggregation'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:message terminate="yes"> This should not have happened.
                                    (<xsl:value-of select="local-name(.)"/>#<xsl:value-of
                                    select="@xml:id"/>) </xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of
                    select="tg:mapping-row(
                    tg:old-path(.), 
                    tg:generate-Path($author, @xml:id), 
                    $extensions, 
                    (.//tei:title)[1])"
                />
            </xsl:for-each>
        </xsl:result-document>
    </xsl:template>


</xsl:stylesheet>
