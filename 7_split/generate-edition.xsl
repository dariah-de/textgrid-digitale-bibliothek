<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns="http://textgrid.info/namespaces/metadata/core/2010" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"    
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    
    <xsl:template name="generate-edition-meta">
        <xsl:param name="header" tunnel="yes"/>
        <xsl:param name="path"/>        
        <xsl:result-document href="{concat($path, '.edition.meta')}">
        <object>
            <generic>
                <xsl:call-template name="generate-generic">                   
                    <xsl:with-param name="format" select="'text/tg.edition+tg.aggregation+xml'"/>
                </xsl:call-template>
            </generic>
            <edition>
                <isEditionOf>
                    <xsl:value-of select="concat(tg:rel-Path-of($path), '.work')"/>
                </isEditionOf>
                <author>
                    <xsl:if test="$header//tei:fileDesc/tei:titleStmt/tei:author/@key">
                        <xsl:attribute name="id" select="@key"/>
                    </xsl:if>
                    <xsl:value-of select="$header//tei:fileDesc/tei:titleStmt/tei:author"/>
                </author>
                <source>
                    <xsl:apply-templates select="$header//tei:fileDesc/tei:sourceDesc/tei:biblFull"
                        mode="edition"/>
                </source>
                <license licenseUri="http://creativecommons.org/licenses/by/3.0/de/legalcode">
                    <xsl:value-of select="normalize-space($licenceText)"/>
                </license>
            </edition>
        </object>
        </xsl:result-document>
    </xsl:template>
    
    
    
    <xd:doc>
        <xd:desc>
            <xd:p>Generates an edition. This creates the edition file (result document) , 
                in there an entry for the item (if we are a TEI file) and entries for the 
                referenced graphics.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="generate-edition">
        <xsl:param name="path"/>
        <xsl:param name="aggr"></xsl:param>
        <xsl:result-document href="{concat($path, '.edition')}">
            <rdf:RDF>
                <rdf:Description>
                    <xsl:choose>
                        <xsl:when test="self::tei:TEI[.//tei:graphic]">
                            <xsl:copy-of select="$aggr"/>
                        </xsl:when>
                        <xsl:when test="self::tei:TEI">
                            <ore:aggregates rdf:resource="{concat(tg:rel-Path-of($path), '.item.xml')}"/>                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:comment>
                                Also, diese Edition entspricht einem <xsl:value-of select="name(.)"/> mit der ID <xsl:value-of select="@xml:id"/>.
                            </xsl:comment>                            
                            <xsl:apply-templates select="tei:TEI|tei:teiCorpus" mode="#current"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </rdf:Description>
            </rdf:RDF>
        </xsl:result-document>
    </xsl:template>
    
    <!-- ***********************edition************************ -->
    
    <xsl:template match="tei:bibFull" mode="edition">
        <bibliographicCitation>
            <author>
                <xsl:if test="tei:titleStmt/tei:author/@key">
                    <xsl:attribute name="id" select="@key"/>
                </xsl:if>
                <xsl:value-of select="tei:titleStmt/tei:author"/>
            </author>
            <xsl:for-each select="tei:titleStmt/tei:title">
                <title>
                    <xsl:value-of select="."/>
                </title>
            </xsl:for-each>
            <xsl:for-each select="tei:publicationStmt/tei:pubPlace">
                <publicationPlace>
                    <value>
                        <xsl:value-of select="."/>
                    </value>
                </publicationPlace>
            </xsl:for-each>
            <xsl:if test="tei:extent">
                <spage>
                    <xsl:value-of select="tei:extent"/>
                </spage>
            </xsl:if>
            <xsl:if test="tei:publicationStmt/tei:date">
                <xsl:call-template name="dateFormat">
                    <xsl:with-param name="date" select="tei:publicationStmt/tei:date"/>
                </xsl:call-template>
            </xsl:if>
        </bibliographicCitation>
    </xsl:template>
    
    
    <!--
    
    <xsl:template match="tei:bibFull" mode="edition">
        <bibliographicCitation>
            <author>
                <xsl:if test="tei:titleStmt/author/@key">
                    <xsl:attribute name="id" select="@key"/>
                </xsl:if>
                <xsl:value-of select="tei:titleStmt/tei:author"/>
            </author>
            <xsl:for-each select="tei:titleStmt/tei:title">
                <title>
                    <xsl:value-of select="."/>
                </title>
            </xsl:for-each>
            <xsl:for-each select="tei:publicationStmt/tei:pubPlace">
                <publicationPlace>
                    <value>
                        <xsl:value-of select="."/>
                    </value>
                </publicationPlace>
            </xsl:for-each>
            <xsl:if test="tei:extent">
                <spage>
                    <xsl:value-of select="tei:extent"/>
                </spage>
            </xsl:if>
            <xsl:if test="tei:publicationStmt/tei:date">
                <xsl:call-template name="dateFormat">
                    <xsl:with-param name="date" select="tei:publicationStmt/tei:date"/>
                </xsl:call-template>
            </xsl:if>
        </bibliographicCitation>
    </xsl:template> -->
</xsl:stylesheet>