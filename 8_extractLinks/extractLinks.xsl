<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">
    
    
    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Adds an ID to each zeno-Element. For this purpose the elements are counted
            hierarchally.</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  --> 
    
    
    <!-- ********************** Output, includes and imports   *********************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </xd:doc>
    <!--<xsl:param name="output-subfolder" select="'01_tgID'"/>-->
    
    <xsl:output indent="yes"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <!-- ***********************************************************************************  --> 
    
    
    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*" priority="0">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@*|comment()|processing-instruction()">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- **************************************************************************************************  -->
</xsl:stylesheet>
    
