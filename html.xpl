<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step xmlns:p="http://www.w3.org/ns/xproc" xmlns:l="http://xproc.org/library"
	xmlns:cx="http://xmlcalabash.com/ns/extensions" xmlns:c="http://www.w3.org/ns/xproc-step"
	version="1.0">
	<p:input port="source">
		<p:empty/>
	</p:input>
	<p:input port="parameters" kind="parameter"/>
	<p:output port="result" sequence="true"/>
	<p:option name="path" required="true"/>
	<p:option name="output" required="true"/>

	<p:import href="http://xproc.org/library/recursive-directory-list.xpl"/>
	<p:import href="http://xmlcalabash.com/extension/steps/library-1.0.xpl"/>

	<l:recursive-directory-list>
		<p:with-option name="path" select="$path"/>
	</l:recursive-directory-list>

	<p:for-each>
		<p:iteration-source select="//c:file"/>
		<p:variable name="filename" select="p:resolve-uri(/c:file/@name)"/>
		<p:variable name="output"
			select="p:resolve-uri(replace(replace($filename, $path, $output), '.item.xml', '.html'))"/>

		<p:choose>
			<p:when test="ends-with($filename, '.item.xml')">

				<cx:message log="debug">
					<p:with-option name="message"
						select="concat('Reading metadata from ', $filename)"/>
				</cx:message>

				<p:load>
					<p:with-option name="href" select="$filename"/>
				</p:load>

				<p:xslt>
					<p:input port="stylesheet">
						<p:document href="html/stylesheets/tohtml.xsl"/>
					</p:input>
				</p:xslt>

				<p:xslt>
					<p:input port="stylesheet">
						<p:document href="html/stylesheets/frame.xsl"/>
					</p:input>
				</p:xslt>

				<p:store method="xhtml" omit-xml-declaration="false" indent="true">
					<p:with-option name="href" select="$output"/>
				</p:store>
			</p:when>
			<p:otherwise>
				<p:sink/>
			</p:otherwise>
		</p:choose>
	</p:for-each>

	<p:identity>
		<p:input port="source">
			<p:empty/>
		</p:input>
	</p:identity>

</p:declare-step>
