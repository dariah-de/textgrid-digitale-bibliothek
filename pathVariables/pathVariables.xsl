<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:tgl="http://textgrid.info/namespaces/metadata/language/2010"
    xmlns:tgs="http://textgrid.info/namespaces/metadata/script/2010"
    xmlns:tgr="http://textgrid.info/namespaces/metadata/agent/2010"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:jxb="http://java.sun.com/xml/ns/jaxb" jxb:version="2.0" xmlns:saxon="http://saxon.sf.net/"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg saxon jxb tns tgl tgs tgr doc" version="2.0">
    
    <xsl:include href="../functions/functions.xsl"/>
    <xsl:variable name="crturi" select="document-uri(/)"/>    
    
   <!-- ****************** Create the output-path**************************************************** --> 
    <xd:doc>
        <xd:desc>
            <xd:p>Path to the main-output-directory.</xd:p>
            <xd:p>The output-subfolder of a single tranformation is created in this directory. </xd:p>
            <xd:p>Choose a name for the subdirectory name through param 'subdirectory' in each stylesheet.</xd:p>
        </xd:desc>
    </xd:doc>
    <!--<xsl:param name="_path_digibib_outputs"
    select="'C:/Dokumente und Einstellungen/kab31ej/Eigene Dateien/textgrid_all/textgrid/data/Digibib-Outputs/LiteraturNewWorkflow'"/>-->
  <!--  <xsl:param name="_path_digibib_outputs"
        select="'/mnt/data/digibib/target/LiteraturNewWorkflow'"/>
    
    <xd:doc>
        <xd:desc>
            <xd:p>Normalizing the output-path to the main folder.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="path-to-main-IO-folder"
        select="tg:normalizeLocalPath($_path_digibib_outputs)"/> 
     
    <xd:doc>
        <xd:desc>
            <xd:p>Uri of the transformed document</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="crturi" select="document-uri(/)"/>    

    <xd:doc>
        <xd:desc>
            <xd:p>Path of the result document.</xd:p>
            <xd:p>Concatenation of the path to the main directory, the name of the outputsubdirectory and the name of transformed xml-file.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="path-of-result-document"
        select="
        tg:get-output-path($crturi,$path-to-main-IO-folder,$output-subfolder)"/>-->

       <!-- *************************************************************************************** -->
   
   
</xsl:stylesheet>
