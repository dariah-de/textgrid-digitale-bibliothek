<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Extracts finer grained metadata from the publicationStmt and the sourceDescrition
            of the teiHeader</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </xd:doc>
 
    <xsl:output indent="yes"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <!-- <xsl:include href="publicationStmt.xsl"/>-->
    <xsl:include href="fileDesc.xsl"/>
    <xsl:include href="profileDesc.xsl"/>
    <xsl:include href="dateFunctions.xsl"/>
    <xsl:include href="exceptionalDateOfCreationFormats.xsl"/>
    <xsl:include href="editionTitleExceptions.xsl"/>
    <xsl:include href="placeOfPublication.xsl"/>
    <!-- ***********************************************************************************  -->


    <!-- ******************************** Other variabels **************************************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>Name of the author.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="author">
        <xsl:value-of select="tei:teiCorpus[1]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"
        />
    </xsl:variable>
    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*" mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@*|comment()|processing-instruction()" mode="#all">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- **************************************************************************************************  -->


    <!-- **************************  Creating the result document ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Creating a result-document</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="/">
            <xsl:apply-templates/>
    </xsl:template>
    <!-- ******************************************************************************************************  -->

    <!-- **************************  Creating the result document ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Get the string of the note-Elements and the title in sourceDesc. If there is no
                information available try to get the information from the ancestor-Elements.</doc:p>
            <doc:p>Call templates for child elements in mode 'fileDesc' with the strings as
                parameters.</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="teiHeader[@type eq 'work']">
        <xsl:variable name="n" select="parent::*/@n"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="fileDesc">
                <!-- create the structured publicationStmt with place of publication and date of publication within biblFull -->
                <!-- add pnd to author-->
                <xsl:with-param name="editionTitles" tunnel="yes">
                    <xsl:choose>
                        <xsl:when test=".//titleStmt[ancestor::biblFull]/title[text() ne '']">
                            <xsl:copy-of select=".//titleStmt[ancestor::biblFull]"/>
                        </xsl:when>
                        <!-- <sourceDesc>
                            <biblFull>
                                <titleStmt>
                                    <title>Anne-->
                        <xsl:when
                            test="ancestor::*[self::teiCorpus or self::TEI][teiHeader//biblFull/titleStmt//title[text() ne '']]">
                            <xsl:copy-of
                                select="ancestor::*[self::teiCorpus or self::TEI][teiHeader//biblFull/titleStmt[title[text() ne '']]][1]/teiHeader//biblFull/titleStmt"
                            />
                        </xsl:when>
                        <xsl:when test="tg:exceptionalEditionTitle($crturi, $n)//tg:editionTitle">
                            <titleStmt>
                                <xsl:for-each
                                    select="tg:exceptionalEditionTitle($crturi, $n)//tg:editionTitle">
                                    <title>
                                        <xsl:value-of select="."/>
                                    </title>
                                </xsl:for-each>
                            </titleStmt>
                        </xsl:when>
                        <xsl:otherwise>
                            <titleStmt>
                                <title>
                                    <xsl:value-of select="'[kein Title]'"/>
                                </title>
                            </titleStmt>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="notesStmt" tunnel="yes">
                    <xsl:choose>
                        <xsl:when test=".//notesStmt/note[text() ne '']">
                            <xsl:copy-of select=".//notesStmt"/>
                        </xsl:when>
                        <xsl:when
                            test="ancestor::*[self::teiCorpus or self::TEI][teiHeader[fileDesc/notesStmt/note[text() ne '']]]">
                            <xsl:copy-of
                                select="ancestor::*[self::teiCorpus or self::TEI][teiHeader[fileDesc/notesStmt/note[text() ne '']]][1]/teiHeader//notesStmt"
                            />
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:with-param>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>

    <xsl:variable name="pnd_manual"
        select="document('Autoren/unique-pnd-manual.xml')/a:authors//a:author[@name eq $author]/@unique-pnd"/>
    <xsl:variable name="pnd"
        select="document('Autoren/authors+pnd-matches.xml')/a:authors//a:author[@name eq $author]/@unique-pnd"/>
    <xsl:template match="author" name="author" mode="#all">
        <author>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="key">
                <xsl:choose>
                    <xsl:when test="contains($crturi, 'Literatur-Goethe')">
                        <xsl:value-of select="'pnd:118540238'"/>
                    </xsl:when>
                    <xsl:when test="$pnd ne ''">
                        <xsl:value-of select="$pnd"/>
                    </xsl:when>
                    <xsl:when test="$pnd_manual ne ''">
                        <xsl:value-of select="$pnd"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="$author"/>
        </author>
    </xsl:template>

</xsl:stylesheet>
