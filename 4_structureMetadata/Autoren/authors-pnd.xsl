<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:p="http://textgrid.info/namespaces/vocabularies/pnd"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" indent="yes"/>
        
    
    <xsl:template match="*">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
        
    <xsl:template match="a:author">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:call-template name="fetchauthor">
                <xsl:with-param name="name" select="@name"/>
            </xsl:call-template>
        </xsl:copy>
    </xsl:template>
    
    
    <xsl:template name="fetchauthor">
        <xsl:param name="name"/>
        <xsl:variable name="uri" select="concat('http://textgridlab.org/pndsearch/pndquery.xql?ln=', encode-for-uri($name))"/>
        <xsl:message>Fetching <xsl:value-of select="$uri"/> ...</xsl:message>
        <xsl:variable name="record">            
            <xsl:apply-templates mode="filter-result" select="doc($uri)/p:response/*"/>
        </xsl:variable>
        <a:pndinfo persons="{count($record/p:person)}" pnds="{count(distinct-values($record/p:person/@id))}" src="{$uri}">
            <xsl:apply-templates select="$record"/>          
        </a:pndinfo>
    </xsl:template>
    
    <xsl:template match="*" mode="filter-result">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="p:variant" mode="filter-result"/>
    
</xsl:stylesheet>