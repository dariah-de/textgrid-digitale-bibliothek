<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:p="http://textgrid.info/namespaces/vocabularies/pnd" exclude-result-prefixes="xs"
    version="2.0">


    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="*">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="a:author">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:variable name="pndinfos">
                <xsl:apply-templates/>
            </xsl:variable>
            <xsl:variable name="matching-pnds" select="$pndinfos//a:pnd[@dates-match]"/>
            <xsl:choose>
                <xsl:when test="count($matching-pnds) = 0">
                    <xsl:variable name="pnds" select="$pndinfos//a:pnd"/>
                    <xsl:choose>
                        <xsl:when test="count($pnds) = 1">
                            <xsl:attribute name="unique-pnd" select="$pnds/@id"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="found">no</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="count($matching-pnds) = 1">
                    <xsl:attribute name="unique-pnd" select="$matching-pnds/@id"/>
                    <xsl:attribute name="found">yes</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="ambiguous-matches" select="$matching-pnds/@id"/>
                    <xsl:attribute name="found">yes</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:attribute name="matches" select="count($matching-pnds)"/>
            <xsl:copy-of select="$pndinfos"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="a:pndinfo">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="p:person" group-by="@id">
                <xsl:variable name="pnd" select="replace(current-grouping-key(), 'pnd:', '')"/>
                <xsl:message>PND: <xsl:value-of select="$pnd"/></xsl:message>
                <a:pnd n="{$pnd}" id="{current-grouping-key()}">
                    <xsl:message>Fetching <xsl:value-of
                            select="concat('http://textgridlab.org/pndsearch/pndquery.xql?id=', $pnd)"
                        /> ...</xsl:message>
                    <xsl:variable name="entry"
                        select="document(concat('http://textgridlab.org/pndsearch/pndquery.xql?id=', $pnd))//p:entry"/>
                    <xsl:if
                        test="( ancestor::a:author/@born le $entry/p:dateOfBirth ) and ( ancestor-or-self::a:author/@died ge $entry/p:dateOfDeath )">
                        <xsl:attribute name="dates-match">yes</xsl:attribute>
                    </xsl:if>
                    <xsl:copy-of select="$entry"/>
                </a:pnd>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
