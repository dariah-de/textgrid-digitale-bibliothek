<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:tg="http://www.textgrid.de"    
    exclude-result-prefixes="xs a tg"
    version="2.0">
    
    <xsl:param name="dir" select="'file:///mnt/data/digibib/target/04-2-corrections+more-meta/Literatur/Text/_alles_verse-in-prosa'"/>
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:include href="../Literatur/util.xsl"/>

    <xsl:template match="/">
        <a:authors>
            <xsl:for-each select="collection($dir)">
                <xsl:choose>
                    <xsl:when test=".//document-metadata/author/@name">
                        <a:author>
                            <xsl:attribute name="name" select=".//document-metadata/author/@name"/>
                            <xsl:variable name="biografie" select=".//article[matches(@location, 'Biographie$')][1]"/>
                            <xsl:variable name="dateFromText" select="//articlegroup[@name eq 'M']/article[1]/text[1]//h4[
                                   matches(text(), '(^|\D+)(\d{4})\D(\d{4})($|\D+)') 
                                or matches(text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)')
                                or matches(text(), '(^|\D+)((\d{4})\D(\d{2}))\D((\d{4})\D(\d{2}))($|\D+)') 
                                or matches(text(),'(^|\D+)(\d{4})\D(\d{2})\D(\d{4})($|\D+)')
                                or (
                                        matches(text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)') 
                                    and contains(normalize-space(text()), 'v. Chr.')) 
                                or matches(text(), '(^|\D+)(\d{3})\D(\d{3})($|\D+)')
                                ]"/>
                            <xsl:variable name="lifedates" select="tg:getLifedatesFromtext($dateFromText)"/>
                            <xsl:if test="$lifedates">
                                <xsl:attribute name="born" select="$lifedates/tg:birthday"/>
                                <xsl:attribute name="died" select="$lifedates/tg:death"/>
                            </xsl:if>
                            <!-- 
                            <xsl:if test="$biografie">
                                <xsl:attribute name="born" select="tg:fetchAuthorBirthday($biografie)"/>
                                <xsl:attribute name="died" select="tg:fetchAuthorDeath($biografie)"/>
                            </xsl:if>
                            '-->
                        </a:author>                    
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:comment>urgs! <xsl:value-of select="document-uri(.)"/> hat keinen Autor?</xsl:comment>
                    </xsl:otherwise>
                </xsl:choose>
                
            </xsl:for-each>
        </a:authors>
    </xsl:template>
    
    
</xsl:stylesheet>