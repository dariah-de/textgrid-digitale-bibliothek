<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    exclude-result-prefixes="xs tg" xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:tgl="http://textgrid.info/namespaces/metadata/language/2010"
    xmlns:tgs="http://textgrid.info/namespaces/metadata/script/2010"
    xmlns:tgr="http://textgrid.info/namespaces/metadata/agent/2010"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:tgbirthday="http://textgrid.info/namespaces/birthday"
    xmlns:jxb="http://java.sun.com/xml/ns/jaxb" jxb:version="2.0" version="2.0">

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Returns the value of a bibliografic source for some known exceptions, which cannot
                be extracted by the heuristics.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="tg:exceptionalEditionTitle">
        <xsl:param name="crturi"/>
        <xsl:param name="n"/>
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when
                    test="contains($crturi, 'Literatur-Abschatz') and $n eq '/Literatur/M/Abschatz, Hans Aßmann von/Gedichte/Gedichte'">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Hans Aßmann von Abschatz: Poetische Übersetzungen und Gedichte. Faksimiledruck nach der Gesamt-Ausgabe von 1704, Herausgegeben von Erika Alma Metzger, 1-4, Bern: Herbert Lang, 1970'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Abraham-a-Sancta-Clara') and ($n eq '/Literatur/M/Abraham a Sancta Clara/Satirischer Traktat/Mercks Wienn')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Abraham a Sancta Clara: Mercks Wienn. Das ist des wütenden Tods ein umbständige Beschreibung [...], Wienn: Peter paul Bivian; der Löbl, 1680 [Neudruck: Tübingen: Niemeyer, 1983, [Deutsche Neudrucke: Reihe Barock; 31].
                                                   '"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Abraham a Sancta Clara: Mercks Wienn. Das ist des wütenden Tods ein umbständige Beschreibung [...], Wienn: Peter paul Bivian; der Löbl, 1680 [Neudruck: Tübingen: Niemeyer, 1983, [Deutsche Neudrucke: Reihe Barock; 31]'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Der Henker')">
                    <tg:editionTitle>
                        <xsl:value-of select="'Aktion 4 (1914), Nr. 13'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and (contains($n, 'Ausgewählte Gedichte/Ein und kein Frühlingsgedicht'))">
                    <tg:editionTitle>
                        <xsl:value-of select="'Aktion 4 (1914), Nr. 22'"/>
                    </tg:editionTitle>
                </xsl:when>
                <!--  <xsl:when test="contains($crturi , 'Ball, Hugo') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Ein und kein Frühlingsgedicht/1. [Ein Doppeldecker steigt aus jeder Flasche]'])">
                                           <tg:editionTitle>
                                               <xsl:value-of select="'Erstdruck in: Die Aktion (Berlin) 4. Jg., Nr. 22, Mai 1914 (Editiontitle of source not available)'"/>
                                           </tg:editionTitle>                                            
                                       </xsl:when>-->
                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Die Sonne')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Erstdruck in: Die Aktion (Berlin) 4. Jg., Nr. 22, Mai 1914 (Editiontitle of source not available)'"
                        />
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Ich liebte nicht')">
                    <tg:editionTitle>
                        <xsl:value-of select="'Revoluzzer 1 (1915), Nr. 12'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Einer Verdammten')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Hugo Ball: Gesammelte Gedichte. Herausgegeben von Annemarie Schütt-Hennings, Zürich, Arche 1963.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Hugo Ball: Gesammelte Gedichte. Zürich 1963'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Ball') and ($n eq '/Literatur/M/Ball, Hugo/Gedichte/Ausgewählte Gedichte/Die Ersten')">
                    <tg:editionTitle>
                        <xsl:value-of select="'Revoluzzer 2 (1916), Nr. 4/5'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Droste') and contains($n,  'Geistliches Jahr in Liedern auf alle Sonn- und Festtage')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Annette von Droste-Hülshoff: Sämtliche Werke in zwei Bänden. Nach dem Text der Originaldrucke und der Handschriften. Herausgegeben von Günther Weydt und Winfried Woesler, Band 1–2, München: Winkler, 1973.'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Ewald') and ($n eq '/Literatur/M/Ewald, Johann Joachim/Gedichte/Sinn Gedichte in zwey Büchern')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Johann Joachim Ewald: Sinngedichte. Abdruck der ersten Ausgabe von 1755, Herausgegeben von Georg Ellinger, Berlin: Verlag von Gebrüder Paetel, 1890.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Johann Joachim Ewald: Sinngedichte. Berlin 1890'"/>
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Gerstenberg') and ($n eq '/Literatur/M/Gerstenberg, Heinrich Wilhelm von/Gedichte/Tändeleyen')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Heinrich Wilhelm von Gerstenberg: Tändeleyen. Faksimiledruck nach der dritten Auflage von 1765. Mit den Lesartaen der Erstausgabe von 1759, Stuttgart: Metzlersche Verlagsbuchhandlung, 1966.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Heinrich Wilhelm von Gerstenberg: Tändeleyen. Stuttgart 1966'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Glaßbrenner, Adolf') and ($n eq '/Literatur/M/Glaßbrenner, Adolf/Gedichte/Verbotene Lieder')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Adolf Glassbrenner: Verbotene Lieder, Bern: Jenni, 1844.'"/>
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Adolf Glassbrenner: Verbotene Lieder, Bern 1844'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Götz') and ($n eq '/Literatur/M/Götz, Nicolaus/Gedichte/Versuch eines Wormsers in Gedichten')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Johann Nikolaus Götz: Gedichte. Aus den Jahren 1745–1765, Stuttgart: Göschen’sche Verlagshandlung, 1893.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Johann Nikolaus Götz: Gedichte. Stuttgart 1893'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Herder') and ($n eq '/Literatur/M/Herder, Johann Gottfried/Theoretische Schriften/Von Ähnlichkeit der mittlern englischen und deutschen Dichtkunst ...')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Sturm und Drang. Dichtungen und theoretische Texte in zwei Bänden. Ausgewählt und mit einem Nachwort versehen von Heinz Nicolai, München: Winkler, 1971'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Sturm und Drang. Band 1, München 1971'"/>
                    </tg:editionTitle>
                </xsl:when>
                <!--<xsl:when test="contains($crturi, 'Literatur-Heym' )and $n eq '/Literatur/M/Heym, Georg/Gedichte/Ausgewählte Gedichte/Du bist so dunkel, als die Nacht ...'">
                                           <blub></blub>
                                       </xsl:when>-->
                <xsl:when
                    test="contains($crturi , 'Literatur-Heym') and $n eq '/Literatur/M/Heym, Georg/Gedichte/Ausgewählte Gedichte/Du bist so dunkel, als die Nacht ...'">

                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Gesamtausgabe. Herausgegeben von Karl Ludwig Schneider, Band 1–2, Hamburg, München: Ellermann, 1960 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Band 1-2, Hamburg, München 1960 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Heym') and contains ($n,'Ausgewählte Gedichte/Stimme aus der Tiefe')">

                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Gesamtausgabe. Herausgegeben von Karl Ludwig Schneider, Band 1–2, Hamburg, München: Ellermann, 1960 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Band 1-2, Hamburg, München 1960 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Heym') and contains ($n, 'Ausgewählte Gedichte/Eifersucht')">

                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Gesamtausgabe. Herausgegeben von Karl Ludwig Schneider, Band 1–2, Hamburg, München: Ellermann, 1960 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Band 1-2, Hamburg, München 1960 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Heym') and contains ($n, 'Ausgewählte Gedichte/Abends')">

                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Gesamtausgabe. Herausgegeben von Karl Ludwig Schneider, Band 1–2, Hamburg, München: Ellermann, 1960 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Band 1-2, Hamburg, München 1960 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Heym') and contains ($n, 'Ausgewählte Gedichte/Eifersucht')">

                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Gesamtausgabe. Herausgegeben von Karl Ludwig Schneider, Band 1–2, Hamburg, München: Ellermann, 1960 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Heym: Dichtungen und Schriften. Band 1-2, Hamburg, München 1960 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Kerner') and ($n eq '/Literatur/M/Kerner, Justinus/Gedichte/Die lyrischen Gedichte')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Justinus Kerner: Werke. 6 Teile in 2 Bänden, Herausgegeben von Raimund Pissin, Band 1 u. 2, Berlin: Bong 1914. [Nachdruck: Hildesheim/ New York: Olms, 1974].'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Justinus Kerner: Werke. 6 Teile in 2 Bänden, Band 1, Berlin 1914'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , '') and ($n eq '/Literatur/M/Lichtenberg, Georg Christoph/Aufzeichnungen und Aphorismen/[Aus den »Sudelbüchern«]')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Christoph Lichtenberg: Schriften und Briefe. Herausgegeben von Wolfgang Promies, Band 1–3, München: Hanser, 1967 ff.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Georg Christoph Lichtenberg: Schriften und Briefe. Band 1, München 1967 ff.'"
                        />
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Metastasio') and matches($n, '(/Literatur/M/Metastasio, Pietro/Libretti/L).(isola disabitata)')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Haydn, Joseph: L`isola disabitata. Drama per musica, del Signor Abate Metastasio, Berlin: [o. V.], 1786.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Haydn, Joseph: L`isola disabitata. Berlin 1786'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi, 'Literatur-Morgenstern') and matches($n, '(/Literatur/M/Morgenstern, Christian/Gedichte/In Phanta).(s Schloss)')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Christian Morgenstern: Sämtliche Dichtungen. Herausgegeben von H. O. Proskauer, Abteilung 1, Basel: Zbinden Verlag, 1971–1973.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Christian Morgenstern: Sämtliche Dichtungen. Abteilung 1, Band 1, Basel 1971–1973'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi, 'Wilhelm') and ($n eq '/Literatur/M/Müller, Wilhelm/Gedichte/Gedichte aus den hinterlassenen Papieren eines reisenden Waldhornisten 1')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Wilhelm Müller: Gedichte. Vollständige kritische Ausgabe mit Einleitung und Anmerkungen besorgt von James Taft Hatfield, Berlin: B. Behr’s Verlag, 1906 [= Deutsche Literaturdenkmale, Nr. 137 = 3. Folge, Nr. 17].'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of select="'Wilhelm Müller: Gedichte. Berlin 1906'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Neukirch') and ($n eq '/Literatur/M/Neukirch, Benjamin/Gedichte/Gedichte')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Herrn von Hoffmannswaldau und anderer Deutschen auserlesener und bißher ungedruckter Gedichte erster Teil, Herausgegeben von Angelo George de Capua und Ernst Alfred Philippson, Tübingen: Niemeyer, 1961 [= Nachdruck von Benjamin Neukirchs Anthologie, Leipzig: Fritsch 1695].'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Herrn von Hoffmannswaldau und anderer Deutschen auserlesener und bißher ungedruckter Gedichte erster Teil, Tübingen 1961'"
                        />
                    </tg:editionTitle>
                </xsl:when>

                <xsl:when
                    test="contains($crturi , 'Literatur-Scheerbart') and ($n eq '/Literatur/M/Scheerbart, Paul/Romane/Liwûna und Kaidôh. Ein Seelenroman')">
                    <tg:editionTitle>
                        <xsl:value-of select="'Insel 2 (1901), Nr. 4'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Schopenhauer') and ($n eq '/Literatur/M/Schopenhauer, Adele/Roman/Anna')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Adele Schopenhauer: Anna. Ein Roman aus der nächsten Vergangenheit. Theil 1–2, Band 1, Leipzig: F. A. Brockhaus, 1845.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Adele Schopenhauer: Anna. Theil 1–2, Band 1, Leipzig 1845'"/>
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Scribe') and ($n eq '/Literatur/M/Scribe, Eugène/Libretti/La dame blanche')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Boieldieu, François-Adrien: La dame blanche, Opéra-comique en trois actes, in: Eugène Scribe: Théatre de Eugène Scribe de l´Académie Française, V,1: Opéras-comiques, Paris: Michel Lévy Frères, 1856, S. 121-181.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'Boieldieu, François-Adrien: La dame blanche, in: Eugène Scribe: Théatre de Eugène Scribe de l´Académie Française, V,1: Opéras-comiques, Paris 1856'"
                        />
                    </tg:editionTitle>
                </xsl:when>
                <xsl:when
                    test="contains($crturi , 'Literatur-Shakespeare') and ($n eq '/Literatur/M/Shakespeare, William/Komödien/Die Komödie der Irrungen')">
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'William Shakespeare: Sämtliche Werke in vier Bänden. Band 1, Herausgegeben von Anselm Schlösser. Berlin: Aufbau, 1975.'"
                        />
                    </tg:editionTitle>
                    <tg:editionTitle>
                        <xsl:value-of
                            select="'William Shakespeare: Sämtliche Werke in vier Bänden. Band 1, Berlin: Aufbau, 1975'"
                        />
                    </tg:editionTitle>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:copy-of select="$title"/>

    </xsl:function>
</xsl:stylesheet>
