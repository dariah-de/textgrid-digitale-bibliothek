<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

    <!-- **************************************** Mode 'fileDesc': Mainly for the fileDesc. Call template for profileDesc ********************************************************* -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Copy the existing elments. Add the noteStmt of the parent where required.</xd:p>
            <xd:p>Apply the templates for sourceDesc (titleStmt and publicationStmt of bibl full
                will be structered)</xd:p>
            <xd:p>Call the template for creating a profileDesc-Element</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="fileDesc" mode="fileDesc">
        <xsl:param name="notesStmt" tunnel="yes"/>
        <xsl:param name="editionTitles" tunnel="yes"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="titleStmt"/>
            <xsl:copy-of select="publicationStmt"/>
            <xsl:copy-of select="$notesStmt"/>
            <!-- Theres's no extra template four sourcDesc. Elements are copied down to titleStmt and the publicationStmt -->
            <xsl:apply-templates select="sourceDesc" mode="fileDesc"> </xsl:apply-templates>
        </xsl:copy>
        <!-- create the profileDesc with textclass and date of creation -->
        <xsl:call-template name="generateProfileDesc">
            <xsl:with-param name="editionTitles" select="$editionTitles" tunnel="yes"/>
            <xsl:with-param name="notesStmt" select="$notesStmt" tunnel="yes"/>
        </xsl:call-template>
    </xsl:template>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>For: teiHeader/sourceDesc/biblFull/titleStmt</xd:p>
            <xd:p>Called only when the titles-Elements are empty: in this case the title of an
                ancestor will be inserted</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="titleStmt[parent::biblFull][not(title[text()])]" mode="fileDesc">
        <xsl:param name="editionTitles" tunnel="yes"/>
        <xsl:copy-of select="$editionTitles"/>
    </xsl:template>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>For: teiHeader/sourceDesc/biblFull/publicationStmt</xd:p>
            <xd:p>Try to get the date of publication and the place of publication.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="publicationStmt[ancestor::biblFull]" mode="fileDesc">
        <xsl:param name="editionTitles" tunnel="yes"/>
        <xsl:param name="notesStmt" tunnel="yes"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <authority>www.textGrid.de</authority>
            <xsl:variable name="crtDates" select="tg:sortDates($editionTitles/titleStmt/title[1])"/>
            <date>
                <xsl:choose>
                    <xsl:when test="count($crtDates//tg:date) eq 1">
                        <xsl:value-of select="$crtDates//tg:date"/>
                    </xsl:when>
                    <xsl:when test="count($crtDates//tg:date) gt 1">
                        <xsl:attribute name="notBefore" select="$crtDates//tg:date[1]"/>
                        <xsl:attribute name="notAfter" select="$crtDates//tg:date[2]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'0000'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </date>
            <!-- tokenize the bibcite -->
            <xsl:variable name="tokenizeBibcite">
                <xsl:for-each select="tokenize($editionTitles/titleStmt/title[1], ' ')">
                    <tg:token>
                        <xsl:value-of select="."/>
                    </tg:token>
                 <!--   <xsl:message select="."></xsl:message>-->
                </xsl:for-each>
            </xsl:variable>
            <!-- test if the last token is a date and if he token before the last one is a word beginning with an upper letter -->
            <xsl:variable name="placeOfPubl">
                <xsl:choose>
                    <!--  and matches($tokenizeBibcite//tg:token[last()-1]/text(),'(\W|^)\p{Lu}\p{Ll}+') -->
                    <xsl:when
                        test="matches ($tokenizeBibcite//tg:token[last()]/text(), '\d{4}|\d{2,4}\D\d{2}')">
                        <xsl:value-of select="$tokenizeBibcite//tg:token[last()-1]/text()"/>
                   <!--     <xsl:message select="$tokenizeBibcite//tg:token[last()-1]/text()"></xsl:message>-->
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <!-- proove if the place of publication must be normalized -->
            <pubPlace>
                <xsl:call-template name="placeOfPublication">
                    <xsl:with-param name="poP" select="$placeOfPubl"/>
                </xsl:call-template>
            </pubPlace>
        </xsl:copy>
    </xsl:template>
    <!--  ***************************************************************************************************************** -->
</xsl:stylesheet>
