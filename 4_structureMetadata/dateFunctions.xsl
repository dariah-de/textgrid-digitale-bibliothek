<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">



    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Tokenize a string(whitespace).</xd:p>
            <xd:p>Extract the dates from different date-formats.</xd:p>
            <xd:p>Sort the extracted dates.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="tg:sortDates">
        <xsl:param name="string"/>
        <xsl:variable name="extractDates">
            <xsl:for-each select="tokenize($string, '\s+')"><!--
                <xsl:message select="."></xsl:message>-->
                <!-- 1942/43 und   1855/1856-89  -->
                <xsl:analyze-string select="$string" regex="{'\d{4}\D\d{2}\D'}">
                    <xsl:matching-substring>
                        <!-- ergibt 19 -->
                        <xsl:variable name="firstPartSecondNumber"
                            select="replace(., '\d{2}\D\d{2}\D' , '')"/>
                        <!-- 1948/49-53:  1948/49-53 ersetzen durch durch '53' -->
                        <xsl:variable name="secondPartSecondNumber"
                            select="replace(., '\d{4}\D(\d{2})\D' , '$1')"/>
                        <tg:date>
                            <xsl:value-of
                                select="concat($firstPartSecondNumber, $secondPartSecondNumber)"/>
                        </tg:date>
                    </xsl:matching-substring>
                </xsl:analyze-string>
                <!-- 1948/49-53 -->
                <xsl:analyze-string select="$string" regex="{'\d{4}\D\d{2}\D\d{2}'}">
                    <xsl:matching-substring>
                        <xsl:variable name="firstPartSecondNumber"
                            select="replace(., '\d{2}\D\d{2}\D\d{2}' , '')"/>
                        <!-- 1948/49-53:  1948/49-53 ersetzen durch durch '53' -->
                        <xsl:variable name="secondPartSecondNumber"
                            select="replace(., '\d{4}\D\d{2}\D(\d{2})' , '$1')"/>
                        <tg:date>
                            <xsl:value-of
                                select="concat($firstPartSecondNumber, $secondPartSecondNumber)"/>
                        </tg:date>
                    </xsl:matching-substring>
                </xsl:analyze-string>
                <xsl:analyze-string select="." regex="\d{{4}}">
                    <xsl:matching-substring>
                   <!--     <xsl:message select="concat(., 'analyze')"></xsl:message>-->
                        <tg:date>
                            <xsl:value-of select="."/>
                        </tg:date>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="allDatesSorted">
            <xsl:for-each select="$extractDates/tg:date">
                <xsl:sort select="."/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:copy-of select="$allDatesSorted"/>
    </xsl:function>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>tokenize a string, search for the keyword given keyword. Call function sortDates
                if the token contains the keyword.</xd:p>            
        </xd:desc>
    </xd:doc>
    <xsl:function name="tg:extractDateWithKeyword">
        <xsl:param name="string"/>
        <xsl:param name="keyword"/>
       <!-- <xsl:message select="$keyword"/>
        <xsl:message select="$string"/>-->
        <xsl:variable name="date">
            <!--  Flag ! : undocumented and untested option to use Java-Syntax for regular expressions-->
            <!-- http://www.biglist.com/lists/lists.mulberrytech.com/xsl-list/archives/201003/msg00002.html -->
            <xsl:for-each select="tokenize($string, '(?&lt;=\S{3,})\.|,', '!')">
         <!--   <xsl:for-each select="tokenize($string, '\.|,')">-->
              <!--   <xsl:message select="."/>-->
                <xsl:if test="matches(., $keyword, 'i')">
                    <xsl:copy-of select="tg:sortDates(.)"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:copy-of select="$date"/>
   <!--     <xsl:message select="$date"></xsl:message>-->
    </xsl:function>
</xsl:stylesheet>
