<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template name="publicationStmt" match="publicationStmt[ancestor::teiHeader[@type eq 'work']]">
        <publicationStmt>
            <authority>www.textGrid.de</authority>
            <availability xmlns:xi="http://www.w3.org/2001/XInclude">
                <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                    davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                    www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                    Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                    Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                    allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                <p>
                    <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                </p>
                <p>
                    <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                        Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                        allgemeinverständlicher Sprache </ref>
                </p>
                <p>
                    <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                </p>
            </availability>
        </publicationStmt>
       <xsl:if test=".[p[text() ne '']]">
           <notesStmt>
               <xsl:for-each select="p[text() ne '']">
                   <note>
                       <xsl:copy-of select="."/>
                   </note>
               </xsl:for-each>
        </notesStmt></xsl:if>
    </xsl:template>
</xsl:stylesheet>