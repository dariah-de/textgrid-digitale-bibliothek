<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    exclude-result-prefixes="xs tg" xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:tgl="http://textgrid.info/namespaces/metadata/language/2010"
    xmlns:tgs="http://textgrid.info/namespaces/metadata/script/2010"
    xmlns:tgr="http://textgrid.info/namespaces/metadata/agent/2010"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:tgbirthday="http://textgrid.info/namespaces/birthday"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:jxb="http://java.sun.com/xml/ns/jaxb" jxb:version="2.0" version="2.0">
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Returns the the date of creation for some known exceptions, which cannot be
                extracted by the heuristics.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="tg:dateOfCreationExceptions">
        <xsl:param name="publicationStmt"/>
        <xsl:param name="crturi"/>
        <xsl:choose>
            <xsl:when
                test="matches($publicationStmt,'Entstanden 1 Mai 1828','i') and contains ($crturi, 'Literatur-Schlegel')">
                <tg:date date="1828-05-01"/>
            </xsl:when>
            <xsl:when
                test="matches($publicationStmt,'Entstanden im 30-jährigen Krieg','i') and contains ($crturi, 'Literatur-Gerhardt')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1618"/>
                    <xsl:attribute name="notAfter" select="1652"/>
                </tg:date>
            </xsl:when>
            <xsl:when
                test="matches($publicationStmt,'Entstanden Mitte der 20er Jahre','i') and contains ($crturi, 'Literatur-Heine')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1924"/>
                    <xsl:attribute name="notAfter" select="1931"/>
                </tg:date>
            </xsl:when>
            <!-- Ein crtnr bei Droste-Hülshoff enthaelt zu unstrukturierte Informationen -->
            <xsl:when
                test="matches($publicationStmt,'Die ersten 25 Gedichte entstanden','i') and contains ($crturi, 'Literatur-Droste')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1820"/>
                    <xsl:attribute name="notAfter" select="1840"/>
                </tg:date>
            </xsl:when>
            <!-- <xsl:when
                                test="contains($crturi, 'Literatur-Klopstock') and parent::article[matches(@location, '(.+)(Gesang$)')]">
                                <date>
                                    <xsl:attribute name="notBefore" select="1745"/>
                                    <xsl:attribute name="notAfter" select="1773"/>
                                </date>
                            </xsl:when>
                            <xsl:when
                            test="contains ($crturi, 'Literatur-Droste') and
                            parent::article[matches(@location, '/Literatur/M/Droste-Hülshoff, Annette von/Gedichte/Geistliches Jahr in Liedern auf alle Sonn- und Festtage')]
                            [position() &lt; 25 or position() eq 25]">
                            <date>
                            <xsl:attribute name="notBefore" select="1819"/>
                            <xsl:attribute name="notAfter" select="1820"/>
                            </date>
                            </xsl:when>-->
            <xsl:when
                test="matches($publicationStmt,'Die 20 Gesänge sind in dem Zeitraum','i') and contains ($crturi, 'Literatur-Klopstock')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1745"/>
                    <xsl:attribute name="notAfter" select="1773"/>
                </tg:date>
            </xsl:when>
            <xsl:when
                test="matches($publicationStmt,'Entstanden 1 Mai 1828','i') and contains ($crturi, 'Literatur-Schlegel')">
                <date date="1828-05-01"/>
            </xsl:when>
            <xsl:when
                test="matches($publicationStmt,'Entstanden im 30-jährigen Krieg','i') and contains ($crturi, 'Literatur-Gerhardt')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1618"/>
                    <xsl:attribute name="notAfter" select="1652"/>
                </tg:date>
            </xsl:when>
            <xsl:when
                test="matches($publicationStmt,'Entstanden Mitte der 20er Jahre','i') and contains ($crturi, 'Literatur-Heine')">
                <tg:date>
                    <xsl:attribute name="notBefore" select="1924"/>
                    <xsl:attribute name="notAfter" select="1931"/>
                </tg:date>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:function>







</xsl:stylesheet>
