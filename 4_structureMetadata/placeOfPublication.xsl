<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    <xsl:template name="placeOfPublication">
        <xsl:param name="poP"/>
        <xsl:choose>
            <!-- ohne Ort: [o.O.] / o.O.u.J. -->
            <xsl:when test="matches($poP,'(\[o\.\s*O\.\s*\]|o\.O\.u\.J\.)\P{L}*(\(ca\.\s*)?$')">
                <xsl:text>[o.O.]</xsl:text>
            </xsl:when>
            <xsl:when
                test="matches($poP,'Halle a\.\s*(d\.\s*)?S(\.|\[aale\])\P{L}*?(\[o\. J.\])?$')">
                <xsl:text>Halle a.d.S.</xsl:text>
            </xsl:when>
            <!-- ohne Jahr: o.J. -->
            <xsl:when test="matches($poP,'o\.\s*J\.?\P{L}*\p{Lu}?\P{L}*$')">
                <xsl:value-of
                    select="replace($poP,
                        '^.*(\p{Lu}\p{Ll}+)\P{L}*\[?o\.\s*J\.?\]?\P{L}*\p{Lu}?\P{L}*$','$1')"
                />
            </xsl:when>
            <xsl:when test="matches($poP,'\d\.\s*Jg\.\P{L}*$')">
                <xsl:value-of
                    select="replace($poP,
                        '^.*(\p{Lu}\p{Ll}+)\W*\d\.\s*Jg\.\P{L}*$','$1')"
                />
            </xsl:when>
            <xsl:when
                test="matches($poP,
                    '(Edgar Allan Poes Werke\.|William Shakespeare:|Mr. William Shakespeares |Die englische Bühne zu Shakespeare.s Zeit)')">
                <xsl:variable name="parts">
                    <xsl:analyze-string select="$poP" regex=":">
                        <xsl:non-matching-substring>
                            <part>
                                <xsl:copy-of select="."/>
                            </part>
                        </xsl:non-matching-substring>
                    </xsl:analyze-string>
                </xsl:variable>
                <xsl:value-of
                    select="replace($parts/part[position() eq (last()-1)],
                        '^.*\W(\p{Lu}\w+)\W*$','$1')"
                />
            </xsl:when>
            <xsl:when test="matches($poP,'New Yorc?k\P{L}*$')">
                <!-- New York -->
                <xsl:value-of
                    select="replace($poP,
                        '^.*?\W((\p{Lu}\w+\s*(/|und)\s*)?New Yorc?k)\P{L}*$','$1')"
                />
            </xsl:when>
            <xsl:when test="matches($poP,'3Leipzig\P{L}*$')">
                <xsl:text>Leipzig</xsl:text>
            </xsl:when>
            <xsl:when test="matches($poP,'Mannheim und Neustadt/Hdt\.\P{L}*$')">
                <xsl:text>Mannheim und Neustadt/Hdt.</xsl:text>
            </xsl:when>
            <xsl:when test="matches($poP,'Hanau/Main\P{L}*$')">
                <xsl:text>Hanau/Main</xsl:text>
            </xsl:when>
            <xsl:when test="matches($poP,'(Frankfurt a\.\s*M\.|Frankfurth? am Ma[iy]n)\s*\P{L}*$')">
                <xsl:text>Frankfurt a.M.</xsl:text>
            </xsl:when>
            <xsl:when test="matches($poP,'Freiburg i\.\s*Br\.\P{L}*$')">
                <xsl:text>Freiburg i.Br.</xsl:text>
            </xsl:when>
            <xsl:when test="matches($poP,'Neustadt a\.d\. Aisch\P{L}*$')">
                <xsl:text>Neustadt a.d. Aisch</xsl:text>
            </xsl:when>
            <xsl:when
                test="contains($poP,'Karl Philipp Moritz: Andreas Hartkopf. Prediger Jahre, Berlin: Johann Friedrich Unger, ')">
                <xsl:text>Berlin</xsl:text>
            </xsl:when>
            <xsl:when
                test="contains($poP,'Sacher-Masoch, Leopold von: Eine Autobiographie. In: Deutsche Monatsblätter. Jahrgang 2, Heft 3, Bremen Juni ')">
                <xsl:text>Bremen</xsl:text>
            </xsl:when>
            <xsl:when
                test="contains($poP,'Jules Verne: Eine ideale Stadt. In: Thadewald, Wolfgang (Hg.): Chroniken der Science Fiction Gruppe Hannover. September ')">
                <xsl:text>Hannover</xsl:text>
            </xsl:when>
            <xsl:when test="contains($poP,'Goethes Werke. Weimarer Ausgabe,')">
                <xsl:text>Weimar</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                  <xsl:variable name="final"
                        select="replace($poP,
                        '(.*?\W+|^)((\p{Lu}\w+\s?([-/]| und )\s?)?\p{Lu}\w+)[\W\P{Lu}]*$','$2')"/>
                    <xsl:value-of
                    select="replace($final,'^Jahr$','[o.O.]')"/>
              <!--  <xsl:value-of select="'[o.O]'"/>-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
