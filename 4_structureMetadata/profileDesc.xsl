<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

    <!--  *********************************************** Variables ******************************************************** -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Contains the date of birth of the date of death</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="dateOfBirthAndDeath">
        <xsl:variable name="bothDates">
            <xsl:copy-of
                select="//TEI[ends-with(@n, '/Biographie')]/text/front//head[@type eq 'h4'][matches(text()[1],
                        '\d{3,}\D\d{2,}')]"
            />
        </xsl:variable>
        <xsl:variable name="bothDatesInOnestring"
            select="concat($bothDates/head[1], ' ', $bothDates/head[2])"/>
        <xsl:copy-of select="tg:sortDates($bothDatesInOnestring)"/>
    </xsl:variable>
    
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>zeno categories which are mapped on verse</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="verseTypesAsText">Ballade, Bildergeschichten, Epos, Epen, Fabeln, Gedicht,
        Gedichte, Lehrgedicht, Liederbücher, Liedsammlung, Lyrik, Poetische Werke, Romanze, Satiren,
        Satirische Dichtung, Sinnsprüche, Versepos, Versepen, Verserzählung, Verserzählungen,
        Versfabeln, Versroman,</xsl:variable>
    
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>zeno categories which are mapped on drama</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="dramaTypesAsText">Drama, Dramen, Dramatische Dichtung, Dramatisches Gedicht,
        Dramenfragment, Dramenfragmente, Historien, Komödie, Komödien, Kunstdrama, Libretto,
        Libretti, Lyrisches Drama, Musikdramen, Puppenspiel, Schauspiel, Tragödie,
        Tragödien,</xsl:variable>
    
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>prose</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="proseTypesAsText">Abhandlung, Anstandsbuch, Aphorismen, Aufsatz,
        Autobiographie, Autobiographischer Roman, Erzählung, Erzählungen, Essay, Fabel,
        Kompilationsliteratur, Märchen, Novelle, Prosa, Reisebeschreibung, Reiseerzählung, Roman,
        Romane, Sage, Theoretische Schrift, Volksbuch,</xsl:variable>
    <!-- ********************************************************************************* -->   
    
    
    <!-- ************************************* template for porfileDesc************************************* -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Generates the profileDesc</xd:p>
            <xd:p>The dates are extracted from the parameters EditionTitles and notesStmt. These may
                be chosen from the parent-elements. (see structure.Metadata)</xd:p>
            <xd:p>Date of creation: heuristic-base extraction.</xd:p>
            <xd:p>If theres is available a matching date from function 'extractDateWithKeyword' this
                date is chosen.</xd:p>
            <xd:p>If the string contains the keyword 'entstanden' and dates this date is
                chosen.</xd:p>
            <xd:p>Otherwhise the author´s birthday (if available) is the value of the attribute
                'notBefore'</xd:p>
            <xd:p>If the string contains the keyword 'erstdruck', and the date of the first imprint
                is less than the author´s death, this will be the value of the attribute
                'notafter'</xd:p>
            <xd:p>If the greatest date extracted from the bibliografic source is less than the
                author´s death, this will be the value of the attribute 'notafter'</xd:p>
            <xd:p>Otherwise the authors life dates are chosen for both values.</xd:p>
            <xd:p>These test are made for both note-Elements in notesStmt, if there are two.</xd:p>

        </xd:desc>
    </xd:doc>
    <xsl:template name="generateProfileDesc">
        <xsl:param name="editionTitles" tunnel="yes"/>
        <xsl:param name="notesStmt" tunnel="yes"/>
        <xsl:variable name="dateOfCreation"
            select="tg:extractDateWithKeyword($notesStmt/notesStmt/note[1], 'entstanden')"/>
        <xsl:variable name="dateOfCreation_II"
            select="tg:extractDateWithKeyword($notesStmt/notesStmt/note[2], 'entstanden')"/>
        <xsl:variable name="dateOfPublication"
            select="tg:extractDateWithKeyword($notesStmt/notesStmt/note[1], 'erstdruck')"/>
        <xsl:variable name="dateOfPublication_II"
            select="tg:extractDateWithKeyword($notesStmt/notesStmt/note[2], 'erstdruck')"/>
        <xsl:variable name="editionDates" select="tg:sortDates($editionTitles/titleStmt/title[1])"/>
        <profileDesc>
            <creation>
                <date>
                    <xsl:choose>
                        <xsl:when
                            test="tg:dateOfCreationExceptions($notesStmt/notesStmt/note[1], $crturi)//tg:date">
                            <xsl:copy-of
                                select="tg:dateOfCreationExceptions($notesStmt/notesStmt/note[1], $crturi)//tg:date"
                            />
                        </xsl:when>
                        <xsl:when test="count($dateOfCreation//tg:date) eq 1">
                            <xsl:value-of select="$dateOfCreation//tg:date"/>
                        </xsl:when>
                        <xsl:when test="count($dateOfCreation//tg:date) gt 1">
                            <xsl:attribute name="notBefore" select="$dateOfCreation//tg:date[1]"/>
                            <xsl:attribute name="notAfter" select="$dateOfCreation//tg:date[2]"/>
                        </xsl:when>
                        <xsl:when
                            test="(count($dateOfBirthAndDeath//tg:date) eq 2 and  $dateOfPublication//tg:date)
                            and ($dateOfBirthAndDeath//tg:date[last()] gt  $dateOfPublication//tg:date[last()])">
                            <xsl:attribute name="notBefore"
                                select="$dateOfBirthAndDeath//tg:date[1]"/>
                            <xsl:attribute name="notAfter"
                                select="$dateOfPublication//tg:date[last()]"/>
                        </xsl:when>
                        <xsl:when
                            test="(count($dateOfBirthAndDeath//tg:date) eq 2 and  $editionDates//tg:date)
                            and ($dateOfBirthAndDeath//tg:date[last()] gt  $editionDates//tg:date[last()])">
                            <xsl:attribute name="notBefore"
                                select="$dateOfBirthAndDeath//tg:date[1]"/>
                            <xsl:attribute name="notAfter" select="$editionDates//tg:date[last()]"/>
                        </xsl:when>
                        <xsl:when test="count($dateOfBirthAndDeath//tg:date) eq 2">
                            <xsl:attribute name="notBefore"
                                select="$dateOfBirthAndDeath//tg:date[1]"/>
                            <xsl:attribute name="notAfter"
                                select="$dateOfBirthAndDeath//tg:date[last()]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'0000'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </date>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>
                        <xsl:variable name="zeno-genre"
                            select="replace(ancestor::*[self::TEI or self::teiCorpus][1]/@n,
                            '/[^/]+/[^/]+/[^/]+/([^/]+)(/.+)?',
                            '$1')"/> 
                        <xsl:choose>
                            <xsl:when
                                test="contains($proseTypesAsText,concat($zeno-genre,','))">
                                <xsl:text>prose</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($verseTypesAsText,concat($zeno-genre,','))">
                                <xsl:text>verse</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($dramaTypesAsText,concat($zeno-genre,','))">
                                <xsl:text>drama</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>other</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </term>
                </keywords>
            </textClass>
        </profileDesc>
    </xsl:template>
    
    


    <!--  ***************************************************************************************************************** -->
</xsl:stylesheet>
