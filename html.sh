#!/bin/bash

. ./workflow.rc

# rm -rf "$odir/html"

for subdir in $odir/final/*
do
  target=$odir/html/$(basename "$subdir")
  echo "HTML transformation for $subdir ..."
  calabash -p verbose=false html.xpl path="file:$subdir" output="file:$target"
done
