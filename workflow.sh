#!/bin/bash

setup() {

    configdir="`dirname $0`"
    if [ "x$configdir" = "x" ]
    then
        configdir="."
    fi

    # Read the config file
    echo "reading config from $configdir"
        if [ -r workflow.rc ]
        then 
            . "$configdir/workflow.rc"
        else
            die "ERROR: Configuration file $configdir/workflow.rc not found."
        fi

        # Validate the configuration
        [ -d "$ssdir" ] || die "Stylesheet directory $ssdir not found or not a directory"
        [ -d "$idir" ] || die "Input directory $idir not found or not a directory"
        [ "$odir" = "" ] && die "Output directory \$odir has not been configured!"
}


# Usage: pipeline filename
#
# Runs most processing steps for one individual encyklopaedia.
pipeline() {
    path="$1"
    mode=$2
    # filename with extension
    filename="`basename \"$path\"`"
    # filename without extension
    subdir="`basename \"$path\" .xml`"
    
    if [ -r skip.txt ] && echo "$filename" | grep -q -F -f skip.txt
    then
       echo "$filename is in skip list -- skipping."
       return
    fi

    echo "Processing $filename..."
 
    echo "   1.1 Adding ids to $filename ..."
 
    saxon \
        -s:"$corpusdir/$filename" \
        -xsl:"$ssdir/1_add_data/add_tgID.xsl" \
        -o:"$odir/01_1_addID/$filename" || return
   
    echo "   1.2 Adding work information to $filename ..."
     saxon \
        -s:"$odir/01_1_addID/$filename" \
        -xsl:"$ssdir/1_add_data/add_Work_add_pname-location.xsl" \
        -o:"$odir/01_2_addWorkInfo/$filename" || return
  
    echo "   2. Creating TEI-Structure from $filename ... " 
    saxon \
        -s:"$odir/01_2_addWorkInfo/$filename" \
        -xsl:"$ssdir/2_generateTEI/generateTEI.xsl"\
        -o:"$odir/2-TEI-Structure/$filename"

    echo "   3. Linking $filename... " 
    saxon \
        -s:"$odir/2-TEI-Structure/$filename" \
        -xsl:"$ssdir/3_linking/linking.xsl" \
        -o:"$odir/3-Linking/$filename" || return

		
	echo "   4. structureMetadata ... " 
    saxon \
        -s:"$odir/3-Linking/$filename" \
        -xsl:"$ssdir/4_structureMetadata/structureMetadata.xsl" \
        -o:"$odir/4-structureMetadata/$filename" || return

    case "$filename" in
       *-000*)
          echo "--- Stopping workflow early for $filename"
          return
          ;;
    esac
  
    echo "   5. Genres ... " 
    saxon \
        -s:"$odir/4-structureMetadata/$filename" \
        -xsl:"$ssdir/5_genres/genres.xsl" \
        -o:"$odir/5-genres/$filename" || return
        
    echo "   6. Corrections ... " 
    saxon \
        -s:"$odir/5-genres/$filename" \
        -xsl:"$ssdir/6_corrections/corrections.xsl" \
        -o:"$odir/6-corrections/$filename" || return

    echo "Done with $filename."	

    echo "   7pre. Clearing split output directory"
    rm -rf -- "$odir/7-split/$subdir"
    
    echo "   7. Splitting $filename ... " 
    saxon \
        -s:"$odir/6-corrections/$filename" \
        -xsl:"$ssdir/7_split/split.xsl" splitDir="${subdir}" crtFilename="${filename}" \
        -o:"$odir/7-split/main.xml"     || return

    echo "   8. Postprocessing ..."
    if [ \! -L "$odir/final" ]
    then
       old_dir=`pwd`
       cd "$odir"
       ln -s 7-split final
       cd "$old_dir"
    fi

    
    #echo "   8. Extract targets from $filename ... " 
    #saxon \
    #    -s:"$odir/7-splitEnzyklops/$filename" \
    #    -xsl:"$ssdir/8_extractLinks/extractLinks.xsl" \
    #    -o:"$odir/8-extractLinks/$filename" || return

    #if [ "$mode" == "partial" ]
   #then
   #    echo "   (Partial application -- skipping postprocessing steps)"
   #else
   #    rewrite-links   "$subdir"
   collect-images  "$subdir"
   process-aggregations    "$subdir"
   # fi

   # echo "Done with $filename." 

}

# Prepare, i.e. clear the target directory
prepare-output-directory() {
    if [ "$1" == "" ]
    then
        outputdir="$odir"

        echo "Preparation: Removing old directories below $outputdir ..."
        [ "$outputdir" != "" -a -d "$outputdir" ] && rm -rf "$outputdir"/*

        cat > "$odir/README.html" <<EOF
        <ol>
        <li><a href='http://dev.digital-humanities.de/ci/job/digibib-enzyklops/'><img src='http://dev.digital-humanities.de/ci/buildStatus/icon?job=digibib-enzyklops'/> Transformation</a></li>
        <li><a href='http://dev.digital-humanities.de/ci/job/digibib-enzyklops-validate/'><img src='http://dev.digital-humanities.de/ci/buildStatus/icon?job=digibib-enzyklops-validate'/> XML-Validierung</a></li>
        <li><a href='http://dev.digital-humanities.de/ci/job/digibib-enzyklops-text/'><img src='http://dev.digital-humanities.de/ci/buildStatus/icon?job=digibib-enzyklops-text'/> Text-Validierung</a></li>
        <li><a href='http://dev.digital-humanities.de/ci/job/digibib-enzyklops-html/'><img src='http://dev.digital-humanities.de/ci/buildStatus/icon?job=digibib-enzyklops-html'/> HTML-Vorschau</a></li>
        </ol>
EOF
    else
        rm -rfv "$odir/splitEnzyklops/$1"
        rm -rfv "$odir/Link-rewriting/$1"
        rm -rfv "$odir/html/$1"
    fi
}


# Processes all files generated in the last (splitEnzyklops) step of the
# individual pipeline calls and fix the links within such that they contain the
# paths to the correct files. This only deals with *.item.xml files.
rewrite-links() {
    if [ "$1" == "" ]
    then
        echo "Link rewriting for all enzyclopaediae ..."
        rewritesource="$odir/7-split"
        rewritetarget="$odir/Link-rewriting"
    else
        echo "   Link rewriting for $1 ..."
        rewritesource="$odir/7-split/$1"
        rewritetarget="$odir/Link-rewriting/$1"
    fi
    # create the directory ...
    mkdir -p "$rewritetarget"
    # ... and make an absolute, canonical path for it
    rewritetarget=$(readlink -f "$rewritetarget")

    # First copy the unaffected files: 
    # Go into the split directory, 
    # find all _f_iles that are not (\!) named '*.item.xml',
    # copy them to $rewritetarget, preserving directory structure
    echo "      1. Copying unaffected files ..."
    ( cd "$rewritesource" && \
        find * -type f \! -name '*.item.xml' -print0 \
        | xargs -0 cp --parents -t "$rewritetarget" )

    n=0
    step=50
    # Now process all items:
    echo "      2. Processing potentially interlinked items ..."
    find "$rewritesource" -name '*.item.xml' | while read item
    do
        # remove the $odir/splitEnzyklops prefix to get a relative path
        relitem=${item#$rewritesource/}
        saxon \
            -s:"$item" \
            -xsl:"$ssdir/Link-rewriting/link-rewriting.xsl" \
            -o:"$rewritetarget/$relitem"
        n=$(( $n + 1 ))
        echo "     ... $n. $relitem ..."
    done

    # link the final folder
    ( cd $odir && ln -sfT Link-rewriting final )
    echo "Link rewriting done"
}

# Links the images from the source directory to the final directory.
# The stylesheets should create metadata files of the form <filename>.meta in
# the place where the images should be. This function looks for those metadata
# files, tries to find a matching image file in the source folder, and creates
# a symbolic link to it at the right place. If no image could be found for a
# specific .meta file, an ERROR message is printed.
collect-all-images() {
    echo "Collecting all images ..."
    for dir in $odir/final/*
    do
        if [ -d "$dir" ]
        then
            enz=$(basename $dir)
            collect-images "$enz"
        fi
    done
}

collect-images() {
    if [ "$1" == "" ]
    then
        collect-all-images
    else
        enz="$1"
        dir="$odir/final/$enz"

        echo "   Collecting images for $enz ..."

        find $dir -name '*.png.meta' -or -name '*.jpg.meta' | while read meta
        do
            img_name=$(basename $meta .meta)
            img_source="$idir/Literatur/Images/$img_name"
            img_source_orig="$idir/Literatur/Images original/$img_name"
            if [ -r "$img_source_orig" ]
            then
                ln -s "$img_source_orig" "${meta%.meta}"
            elif [ -r "$img_source" ]
            then
                ln -s "$img_source" "${meta%.meta}"
            else
                echo "ERROR collecting images: For $meta, neither $img_source_orig nor $img_source were found."
            fi
        done
    fi
}

# Recursively process the generated aggregations from the final folder.
# (1) Validates that all the links in the aggregation files work
# (2) Validates that the generated files are all linked somewhere, except for 
#       - .lst and .html files, which won't be imported
#       - image files, already checked by the script above
# (3) Generates a fancy HEADER.html index linking each individual file.
# Note that the HEADER file also links to html preview versions of each
# item -- these must however be created separately using the html.sh script!
process-all-aggregations() {
    echo
    echo "Validating filenames and generating indexes ..."

    for dir in $odir/final/*
    do
        if [ -d $dir ]
        then
            process-aggregations "$(basename $dir)"
        fi
    done
}

process-aggregations() {
    if [ "$1" == "" ]
    then
        process-all-aggregations
    else
        enz="$1"
        dir="$odir/final/$enz"

        echo "Looking through $enz ..."
        filelist="$dir/files.lst"
        echo '<filelist xmlns="http://www.textgrid.de/ns/filelist">' > "$filelist"
        find "$dir" -type f -and -not -name '*.lst' -and -not -name '*.html' \
            -printf '  <file modified="%TY-%Tm-%TdT%TH:%TM">%P</file>\n' >> "$filelist"
        echo '</filelist>' >> "$filelist"

        saxon -s:"$dir/collection/collection.collection" \
            -xsl:dbsplitindex.xsl \
            -o:"$dir/HEADER.html" \
            "base=$dir/HEADER.html" "filelist=$filelist" "html=$odir/html/`basename $dir`"

    fi
}

##################### Helper functions ###########################

# print an error message and exit.
die() {
    if [ $# -gt 0 ]
    then
        echo "ERROR: $@"
    else
        cat
    fi
    exit 1
}

task() {
    spec="$1"
    echo "Running subtask '$spec' ($corpusdir) ..."
    if [ -r "$spec" ]
    then
       echo "(a) file $spec"
        pipeline "$spec"
    elif [ -r "$corpusdir/$spec" ]
    then
       echo "(b) file $corpusdir/$spec"
        pipeline "$corpusdir/$spec"
    elif [ -r "$corpusdir/Literatur-${spec}.xml" ]
    then
       echo "(c) file $corpusdir/Literatur-${spec}.xml"
        pipeline "$corpusdir/Literatur-${spec}.xml"
    else
       echo "(d) ... trying to read $spec as a pattern ..."
        find "$corpusdir" -iname "*$spec*" |\
            while read f
            do
               echo "   - match: $f"
                task "$f"
            done
    fi
}

################### main function call ###########################

setup

if [ $# -gt 0 ]
then
    while [ $# -gt 0 ]
    do
        task "$1"
        shift
    done
else
    for f in "$corpusdir/"Literatur-*.xml 
    do
       case "$f" in 
          *-Nietzsche*|*-Winckelmann*)
            echo "Skipping $f ..."
            ;;
         *)
            pipeline "$f"
            ;;
      esac
    done
fi
