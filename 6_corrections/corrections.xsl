<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs tg xd a fn xd tg"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">


    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Adds an ID to each zeno-Element. For this purpose the elements are counted
            hierarchally.</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </xd:doc>
    <!--<xsl:param name="output-subfolder" select="'01_tgID'"/>-->

    <xsl:output indent="yes"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*" mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@*|comment()|processing-instruction()" mode="#all">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="contains($crturi,'Literatur-Frischlin')">
                <xsl:apply-templates mode="Literatur-Frischlin"/>
            </xsl:when>
            <xsl:when test="contains ($crturi, 'Literatur-Gutzkow,-Karl')">
                <xsl:apply-templates mode="Literatur-Gutzkow"/>
            </xsl:when>
            <xsl:when test="contains($crturi,'Literatur-Tucholsky')">
                <xsl:apply-templates mode="Literatur-Tucholsky"/>

            </xsl:when>
            <xsl:when test="contains($crturi,'Literatur-Lohenstein')">
                <xsl:apply-templates mode="Literatur-Lohenstein"/>
            </xsl:when>
            <xsl:when test="contains ($crturi, 'Literatur-Raimund')">
                <xsl:apply-templates mode="Literatur-Raimund"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Schnitzer')">
                <xsl:apply-templates mode="Literatur-Schnitzer"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Stramm')">
                <xsl:apply-templates mode="Literatur-Stramm"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Iffland')">
                <xsl:apply-templates mode="Literatur-Iffland"/>
            </xsl:when>
            <!-- ***** -->
            <xsl:when test="contains($crturi, 'Literatur-Rosner')">
                <xsl:apply-templates mode="Literatur-Rosner"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Rist')">
                <xsl:apply-templates mode="Literatur-Rist"/>
            </xsl:when>

            <!-- moliere -->
            <xsl:when
                test="contains($crturi, 'Literatur-Moli') and not(contains($crturi, 'Literatur-Molina'))">
                <xsl:apply-templates mode="Literatur-Moli"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Goethe') and contains($crturi , '002')">
                <xsl:apply-templates mode="Literatur-Goethe-002"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Metastasio')">
                <xsl:apply-templates mode="Literatur-Metastasio"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Lautensack')">
                <xsl:apply-templates mode="Literatur-Lautensack"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Weidmann')">
                <xsl:apply-templates mode="Literatur-Weidmann"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Hallmann')">
                <xsl:apply-templates mode="Literatur-Hallmann"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Stranitzky')">
                <xsl:apply-templates mode="Literatur-Stranitzky"/>
            </xsl:when>
            <xsl:when test="contains($crturi , 'Literatur-Busch')">
                <xsl:apply-templates mode="Literatur-Busch"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'nther,-Johann-Christian')">
                <!-- Günther Johann Chtristian -->
                <xsl:apply-templates mode="Johann-Christian"></xsl:apply-templates>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Schambach')">
                <xsl:apply-templates mode="Literatur-Schambach"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Pfitzer')">
                <xsl:apply-templates mode="Literatur-Pfitzer"/>
            </xsl:when>
            <xsl:when test="contains($crturi, 'Literatur-Luther')">
                <xsl:apply-templates mode="Literatur-Luther"/>
            </xsl:when>

            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="sp[count(*) eq count(milestone|pb|speaker)]" mode="#all">
        <!-- müsste restrukturiert werden: speaker enthält nur milestones, aber keine ps oder l -->
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates mode="#current"/>
            <p/>
        </xsl:copy>
    </xsl:template>
 

    <xsl:template match="speaker" mode="#all">
        <xsl:choose>
            <xsl:when
                test="(count(*) eq 1) and p and
                    not(text()[normalize-space() ne ''])">
                <speaker>
                    <xsl:apply-templates select="p/@*"/>
                    <xsl:apply-templates select="p/node()"/>
                </speaker>
            </xsl:when>
            <!--  <xsl:when test="count(*) eq count(milestone|pb)">
                <!-\- müsste restrukturiert werden: speaker enthält nur milestones, aber keine ps oder l -\->
                <speaker>
                    <xsl:apply-templates select="@*"/>
                    <xsl:apply-templates/>
                    <p/>
                </speaker>
            </xsl:when>-->
            <xsl:when test="count(*) eq 1 and stage/hi/text()[last()][normalize-space(.) = 'NB.']">
                <xsl:apply-templates/>
            </xsl:when>
            <!-- umstrukturieren: Bsp: Schnitzer -->
            <xsl:when test="count(*) eq 1 and stage">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <speaker>
                    <xsl:apply-templates select="@*"/>
                    <xsl:apply-templates/>
                </speaker>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <xsl:template mode="#all"
        match="sp/speaker[count(*) eq 1][p[count(text()) eq 1 ][matches(text(), '^\p{Lu}{2,}(\s.*)?\p{Ll}{2,}')]]
        [not (matches  (p/text(), '^GOTT'))] [not (matches  (p/text(), '^WINTER'))]"
        priority="4">
        <!-- (:Bei Frischlin spricht der Sprecher über GOTT. GOTT wird dann groß geschrieben und deshalb als eigenständiger
        sp erkannt. Siehe auch Template match =sp:) -->
        <xsl:if test="matches(normalize-space(p/text()),'^\p{Lu}{2,}(\s.*)?\p{Ll}{2,}')">
            <xsl:variable name="id" select="p/@xml:id"/>
            <xsl:analyze-string select="p/text()" regex="(^\p{{Lu}}{{2,}})(\s.*)">
                <xsl:matching-substring>
                    <speaker xmlns="http://www.tei-c.org/ns/1.0">
                        <xsl:if test="$id">
                            <xsl:attribute name="xml:id" select="concat($id, 'split')"/>
                        </xsl:if>
                        <xsl:value-of select="regex-group(1)"/>
                        <!-- <xsl:text>juhu</xsl:text> -->
                    </speaker>
                    <l xmlns="http://www.tei-c.org/ns/1.0">
                        <xsl:value-of select="regex-group(2)"/>
                    </l>
                </xsl:matching-substring>
            </xsl:analyze-string>
        </xsl:if>
    </xsl:template>
    <xsl:template
        match="sp[speaker and p[not(child::node()) or normalize-space() eq ''] and text()[matches(., '\S')] and stage]"
        mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="speaker|node()[preceding-sibling::node()[1][self::speaker]]">
                <xsl:choose>
                    <xsl:when test="self::speaker">
                        <xsl:apply-templates select="current-group()"/>
                    </xsl:when>
                    <xsl:when
                        test="self::text()[normalize-space(.) eq ''] and count(current-group()) eq 1">
                        <xsl:copy-of select="current-group()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <p>
                            <!--   <xsl:copy-of select="current-group()"></xsl:copy-of>-->
                            <xsl:apply-templates
                                select="current-group()[not(self::p[not(child::node())])]"
                                mode="stage2hi"/>
                        </p>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <!--<xsl:template match="sp/speaker[count(*) eq 1 and stage/hi/text()[last()][normalize-space(.) = 'NB.']]"
        mode="Literatur-Hallmann Literatur-Stranitzky Literatur-Iffland">
    
        <xsl:apply-templates></xsl:apply-templates>
    </xsl:template>-->

    <xsl:template
        match="sp[speaker/stage/hi[text()[1] eq 'NB']][p[starts-with(normalize-space(text()[1]), 'Bey dieser Scene muß die ganze Beleuchtung')]]"
        mode="Literatur-Weidmann" priority="1">
        <closer>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="Weidmann-sp-2-closer"/>

        </closer>
    </xsl:template>
    <xsl:template match="speaker" mode="Weidmann-sp-2-closer" priority="1">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="p|stage" mode="Weidmann-sp-2-closer" priority="1">
        <seg>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </seg>
    </xsl:template>
    <xsl:template match="sp[speaker and l and text()[matches(., '\S')] and stage]"
        mode="Literatur-Hartleben">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="speaker|node()[preceding-sibling::node()[1][self::speaker]]|l">
                <xsl:choose>
                    <xsl:when test="self::speaker">
                        <xsl:apply-templates select="current-group()"/>
                    </xsl:when>
                    <xsl:when
                        test="self::text()[normalize-space(.) eq ''] and count(current-group()) eq 1">
                        <xsl:copy-of select="current-group()"/>
                    </xsl:when>
                    <xsl:when test="l">
                        <xsl:copy-of select="current-group()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <l>
                            <!--   <xsl:copy-of select="current-group()"></xsl:copy-of>-->
                            <xsl:apply-templates
                                select="current-group()[not(self::p[not(child::node())])]"
                                mode="stage2hi"/>
                        </l>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
    <xsl:template mode="stage2hi" match="stage">
        <hi>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </hi>
    </xsl:template>

    <xsl:template match="sp[speaker[p[@xml:id eq 'tg135.2.85']]]" mode="Literatur-Gutzkow"
        priority="1">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
            <l xmlns="http://www.tei-c.org/ns/1.0"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="sp[not(head)]" mode="Literatur-Tucholsky">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="speaker" mode="Literatur-Tucholsky" priority="1">
        <l>
            <xsl:apply-templates select="@*|node()"/>
        </l>
    </xsl:template>
    <xsl:template mode="Literatur-Tucholsky" match="list[count(item) eq 1]">
        <byline>
            <xsl:apply-templates select="@*[not(name() eq 'type')]|node()" mode="#current"/>
        </byline>
    </xsl:template>
    <xsl:template mode="Literatur-Tucholsky" match="list[count(item) eq 1]/item">
        <seg>
            <xsl:apply-templates select="@*|node()"/>
        </seg>
    </xsl:template>
    <xsl:template mode="Literatur-Tucholsky" match="p[@xml:id eq 'tg1426.2.26']">
        <byline>
            <xsl:apply-templates select="@*[not(name() eq 'type')]|node()" mode="#current"/>
        </byline>
    </xsl:template>

    <xsl:template mode="Literatur-Lohenstein" match="sp[speaker[p[@xml:id eq 'tg70.2.1284']]]"
        priority="1">
        <closer>
            <xsl:copy-of select="speaker/p/@*"/>
            <xsl:value-of select="speaker"/>
        </closer>
    </xsl:template>

    <!-- ***************Raimud****************** -->
    <xsl:template match="sp[speaker[p[@xml:id eq 'tg165.2.6' or @xml:id eq 'tg157.2.6']]]"
        mode="Literatur-Raimund">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <l xmlns="http://www.tei-c.org/ns/1.0"/>
        </xsl:copy>
    </xsl:template>

    <!-- *************Schnitzer********************: Nicht uebernommen im neuen work-flow-->
 <!--   <xsl:template
        match="sp[speaker[1][count(*) eq count(stage) and matches(stage[1]/hi, 'II.')]and preceding-sibling::*[1][self::sp[not(lg | l)]] ]"
        mode="Literatur-Schnitzer">
        <!-\-  and preceding-sibling::*[1][self::sp[not(lg | l)]] -\->
        <xsl:apply-templates mode="Literatur-Schnitzer"/>
        <xsl:copy>
            <xsl:copy-of select="preceding-sibling::*[1][self::sp[not(lg | l)]]/speaker"/>
            <xsl:copy-of select="child::*[not(self::speaker[not(text())])]"/>
            
        </xsl:copy>
    </xsl:template>
    <xsl:template
        match="sp[not(lg | l) and following-sibling::*[1][self::sp/speaker[1][count(*) eq count(stage) and matches(stage[1]/hi, 'II.')]]]"
        mode="Literatur-Schnitzer" priority="1"></xsl:template>-->
    <!--<xsl:template match=""></xsl:template>-->
    <!--             
                
            <xsl:when test="contains($crturi, 'Literatur-Stramm')"/>
   -->
    <!-- ***************Schramm ****************************** -->
    <xsl:template match="sp[speaker[p[@xml:id eq 'tg97.2.36']]]" mode="Literatur-Stramm">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <l xmlns="http://www.tei-c.org/ns/1.0"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="sp[speaker[p[@xml:id eq 'tg97.2.54']]]" mode="Literatur-Stramm">
        <stage>
            <xsl:copy-of select="speaker/p/@*"/>
            <xsl:value-of select="speaker/p"/>
        </stage>
    </xsl:template>
    <!-- ****************************Moliere****************************** -->
    <xsl:template match="sp[speaker[p[matches(text(), 'MUSIK UND TANZ') and not(child::*)]]]"
        mode="Literatur-Moli">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <l/>
        </xsl:copy>
    </xsl:template>

    <!-- ********************goethe002******************* -->
    <xsl:template mode="Literatur-Goethe-002"
        match="sp[speaker[p [matches (text()[1], 'G.')][@xml:id eq 'tg15.2.635']]]|
        sp[speaker[p [matches (text()[1], 'Goethe.')][@xml:id eq 'tg5.2.148']]]">
        <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:template>


    <xsl:template match="div[p[following-sibling::head]]" mode="Literatur-Pfitzer">
    <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:for-each-group select="node()" group-starting-with="head">
            <div type ="uncertain-level">
                <xsl:copy-of select="current-group()"/>
            </div>
        </xsl:for-each-group>
    </xsl:copy>
</xsl:template>
    
    <xsl:template match="p[head]" mode="Literatur-Luther">
        <xsl:apply-templates/>
    </xsl:template>
    <!--**************************************************** Metastasio********************************************* -->

    <xsl:template match="div [@type eq 'text'] [div[@type eq 'set']][castGroup]"
        mode="Literatur-Metastasio">
        <xsl:copy>
            <xsl:attribute name="type" select="'Dramatis_Personae'"/>
            <xsl:apply-templates select="@*[not(name() eq 'type')]|node()"
                mode="Literatur-Metastasio"/>
        </xsl:copy>
    </xsl:template>
    <!-- Entfernen von leeren <div @type eq 'set'>. Wegen dem vorhergehenden Metastasio-template. Kann aber auch
    in anderen Faellen entfernt werden.-->
    <xsl:template match="div[@type eq 'set'][normalize-space (text()[1]) eq '']"
        mode="Literatur-Metastasio"/>
    <xsl:template match="castGroup[not(parent::*[castList])]" mode="Literatur-Metastasio">
        <castList xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:copy>
                <xsl:apply-templates select="@*|node()" mode="#current"/>
            </xsl:copy>
        </castList>
    </xsl:template>

    <!-- *********************************************************************************alle**************************************************** -->


    <!-- ******************************Lists****************************** -->
    <xsl:template mode="#all" match="list[figure][not(item)]">
      <!--  <milestone>
            <xsl:copy-of select="@*"/>
        </milestone >-->
        <xsl:apply-templates mode="#current"/>
    </xsl:template>


    <!-- ******************************empty p-Elements****************************** -->
    <xsl:template mode="#all" match="p[not(node())][not(ancestor::teiHeader)][not(parent::sp)]">
        <!--    <milestone>
            <xsl:copy-of select="@*"/>
        </milestone>
        <xsl:apply-templates mode="#current"/>-->
    </xsl:template>
    <xsl:template mode="#all" match="p[not(child::text()[matches(., '\S')])][child::pb]">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>


    <!-- ******************************castLists and Items****************************** -->
    <xsl:template
        match="div[@type eq 'Dramatis_Personae'][castList[(count(head) eq count(child::*))
        or (count(head) + count (castGroup) eq count (*) and count (castGroup/*) eq 0 )]]"
        mode="#all">
        <div type="corrected_div_castList" xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:copy-of select="castList/head"/>
        </div>
        <!--  <div>hier</div>-->
    </xsl:template>
    <!--  <xsl:when test="tei:castList/tei:castGroup[not(tei:castItem or tei:roleDesc or tei:castGroup) and count(*) eq 0]"/>-->
    <xsl:template
        match="div[@type eq 'Dramatis_Personae'][castList/castGroup[not(castItem or roleDesc or castGroup) and count(*) gt 0]]"
        mode="#all">
        <div type="set" xmlns="http://www.tei-c.org/ns/1.0" n="corrected_div_castList">
            <xsl:copy-of
                select="castList/castGroup[not(castItem or roleDesc or castGroup) and count(*) gt 0]/node()"
            />
        </div>
    </xsl:template>
    <xsl:template
        match="div[@type eq 'Dramatis_Personae'][castList/castGroup[count(*) eq 0 and count(text()[matches(., '\S')]) eq 0]]"
        mode="#all">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="castGroup[count(*) eq 0 and count(text()[matches(., '\S')]) eq 0]"
        mode="#all"/>

    <!-- ************************speakers************* -->

    <xsl:template
        match="sp[not(head)][following-sibling::*[1][self::sp[starts-with (speaker, 'GOTT')]]]"
        mode="Literatur-Frischlin">
        <xsl:copy>
            <xsl:copy-of select="@*|node()"/>
            <l xmlns="http://www.tei-c.org/ns/1.0">
                <xsl:value-of
                    select=" following-sibling::*[1] [self::sp[starts-with (speaker, 'GOTT')]]"/>
            </l>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="sp[not(head)][starts-with (speaker, 'GOTT')]"
        mode="Literatur-Frischlin Literatur-Schnitzer"/>
    <xsl:template match="sp[not(p or l or lg)]" mode="Literatur-Frischlin" priority="1">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:choose>
                <xsl:when test="milestone[@unit eq 'p']">
                    <xsl:apply-templates select="node()[not(milestone[@unit eq 'p'])]"/>
                    <p/>
                </xsl:when>
                <xsl:when test="milestone[@unit eq 'l']">
                    <xsl:apply-templates select="node()[not(milestone[@unit eq 'l'])]"/>
                    <l/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="node()"/>
                    <p/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>

    <!-- **************************************************************************************************  -->

    <xsl:template
        match="div[@type eq 'h4'][*[1][self::sp]/head[not(preceding-sibling::*[not(self::head)])]]"
        mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="*[1][self::sp]/head[not(preceding-sibling::*[not(self::head)])]"/>
            <!-- <xsl:apply-templates
                select="node()[self::head][not(preceding-sibling::*[not(self::head)])]"
                mode="#current"/>-->
            <xsl:apply-templates select="node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template
        match="div[@type eq 'h4']/*[1][self::sp]/head[not(preceding-sibling::*[not(self::head)])]"
        mode="#all"/>
    <xsl:template match="tg_grp" mode="#all">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template
        match="speaker[(count(*) eq 1) and p and
        not(text()[normalize-space() ne ''])]"
        mode="#all">
        <speaker>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="p/@*"/>
            <xsl:apply-templates select="p/node()" mode="#current"/>
        </speaker>
    </xsl:template>

    <!-- ******************************tables****************************** -->

    <xsl:template mode="#all" match="sp[table]|lg[table]">
        <!--  -->
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template mode="#all" match="sp[table]/speaker | lg[table]/speaker" priority="1">
        <xsl:choose>
            <xsl:when test="p">
                <xsl:apply-templates select="node()" mode="#current"/>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:apply-templates select="@*|node()" mode="#current"/>
                </p>
            </xsl:otherwise>
        </xsl:choose>



    </xsl:template>
    <xsl:template match="sp[table]/l|lg[table]/l" mode="#all">
        <p>
            <xsl:copy-of select="@*|node()"/>
        </p>
    </xsl:template>
    <!-- tables die Kinder von <sp> oder <lg> sind müssen von einem <p> umschlossen werden -->
    <xsl:template match="table[parent::*[self::sp | self::lg]]" mode="#all">
        <p>
            <xsl:copy>
                <xsl:apply-templates select="@*|node()" mode="#current"/>
            </xsl:copy>
        </p>
    </xsl:template>

    <xsl:template mode="#all" match="row[cell[following-sibling::*[1][self::note]]]">
        <!-- ## Dieses Problem erscheint bei: Literatur-Gottsched,-Johann-Christoph/Theoretische Schriften.xml -->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()" group-starting-with="cell">
                <xsl:choose>
                    <xsl:when test="current-group()[1][self::cell]">
                        <cell>
                            <xsl:copy-of select="current-group()[1]/@*|current-group()[1]/node()"/>
                            <xsl:copy-of
                                select="current-group()[position() gt 1][not(self::text()[normalize-space(.) eq ''])]"
                            />
                        </cell>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="current-group()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="div[div[following-sibling::p]]" mode="#all">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()" group-starting-with="div|p[preceding-sibling::*[1][not(self::p)]]">
                <xsl:choose>
                    <xsl:when test="self::div">
                        <xsl:apply-templates select="current-group()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when
                                test="every $x in current-group() satisfies self::p[(not(text()) or not(text()[matches(., '\S')])) and not(*)]
                                or self::text()[not(matches(., '\S'))]"> </xsl:when>
                            <xsl:otherwise>
                                <div>
                                    <xsl:apply-templates select="current-group()"/>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
    
    <!-- ********************div in lg -->
    
  <!--  <xsl:template match="lg[*[last()][self::div]][count(div) eq 1]" mode="Johann-Christian hi2head">  
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="node()[not(self::div)]"/>
        </xsl:copy>
        <xsl:copy-of select="div"/>
    </xsl:template>
    <xsl:template match="sp[ancestor::TEI//title[1][. eq '[Dasz Frauen, wenn sie gehn, ein blinder Appetit]']]" mode="Johann-Christian">
      <div>  <xsl:apply-templates mode="hi2head" select="node()[not(self::div or preceding-sibling::div)]"/></div>
        <xsl:apply-templates select="div|node()[preceding-sibling::div]"></xsl:apply-templates>
    </xsl:template>
    <xsl:template match="speaker" mode="hi2head" priority="1">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="hi[parent::speaker]" mode="hi2head">
    <head>    <xsl:apply-templates select="@*|node()"/></head>
    </xsl:template>-->
    
    <xsl:template match="div[@type eq 'text'][ancestor::TEI//title[1][. eq 'Tempel deß Friedens']]">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="p[count(text()) eq 1 and normalize-space(text()) eq 'Rektor, Sprachwissenschaftler und Volkskundler']
        [preceding-sibling::head[normalize-space(text()) eq '(1811–1879)']]
        " mode="Literatur-Schambach">
        <head>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="node()"/>
        </head>
    </xsl:template>
    
    
    <!-- ***************gallery **************++-->
<!-- gallery:  empty namespace, use the local name -->
<xsl:template match="*[local-name() eq 'gallery']" mode="Literatur-Busch">
    <xsl:apply-templates></xsl:apply-templates>
</xsl:template>
</xsl:stylesheet>
