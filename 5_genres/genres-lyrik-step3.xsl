<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    
    
    <xd:doc scope="component">
        <doc:desc>
            <xd:p>Groups startin with line breaks, h4, when the classes change from one p to another, or where the ps differ in length</xd:p>
            <xd:p>Grpoups are surrounder with tg_group. tg_group has the attribute nr, the value is the number of the p-elements in the group.
            tg_group has the attribute strlen. When the group contains a p-element which is longer than 100 characters the value is 'long'.
            Otherwise the value is 'short' or zero.</xd:p>
        </doc:desc>
    </xd:doc>
    <!-- In Frage kommen nur texte wo mindestens 1 Mal eine Zeile gefolgt von mehreren gruppierten Zeilen erscheint. -->
    <!-- ausgeschnitten: tg_grp...[preceding-sibling::tg_grp[@nr != '1']] = davor gibt es schon gruppierte Zeilen -->
    <xsl:template match="div[count(tg_grp[@nr = '1'][@strlen='short']
        [p[@tg_feat or starts-with(@tg_content,'elem')]]
        [following-sibling::*[self::tg_grp][1][@strlen = 'short'][@nr != '1']]) 
        gt 0]" mode="markHeadType">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <!-- Erkennung der Struktur vom Typ div/(head,lg) -->
            
                
               
                    <!--<xsl:message select="'markHeadType'"></xsl:message>-->
                    <xsl:variable name="headType">
                        <!-- in $headType wird ermittelt, welche Kodierung fuer die head-Zeilen verwendet wurde
                            (z.B. i als Container-Element, center o.ae. als @rendition-Angabe), indem festgestellt wird,
                            welche Kodierung die meisten von den potentiellen head-Zeilen haben. 
                            Nur diese werden dann entsprechend gekennzeichnet. -->
                        <xsl:variable name="content-types">
                            <xsl:for-each-group select="tg_grp[@nr='1']" 
                                group-by="concat(
                                p[starts-with(@tg_content,'elem') or matches(@tg_feat,'align:(center|right)')]/@tg_content,'_',
                                p[starts-with(@tg_content,'elem') or matches(@tg_feat,'align:(center|right)')]/@rendition  )">
                                <content type="{current-grouping-key()}" nr="{count(current-group())}"/>
                                <!-- z.B.: 
                                    $contentTypes = [content @type='elem-i_' @nr='3', 
                                    content @type='text_align:center;' @nr='2', 
                                    content @type='elem-span_' @nr='1']
                                    = Wie oft eine bestimmte Kodierung bei Gruppen aus jew. einer Zeile (tg_grp @nr='1') im aktuellen Text vorkommt.
                                    Das mehrfache Vorkommen deutet auf die head-Qualität der Zeile hin (konsequente Anwendung).
                                    @type kann auch leer sein, wenn: tg_grp[@nr='1']/p[not(@tg_feat)][@tg_content = 'text' or @tg_content = 'mixed'],
                                    also wenn die Zeile nichts besonderes hat, ausser dass sie alleine steht. 
                                    Solche Zeilen werden bei der Festlegung der Kodierungsart ($headType) ausgeschlossen. 
                                -->
                            </xsl:for-each-group>
                        </xsl:variable>
                       <!--<xsl:message select="$content-types"></xsl:message>-->
                        <xsl:variable name="maxOccur" select="max($content-types/content[@type != '_']/@nr)"/>
                        
                        <tg_headType occur="{$maxOccur}">
                            <xsl:attribute name="value">
                                <xsl:choose>
                                    <!-- wenn nur eine Kodierung am meisten (z.B. 5 Mal i) verwendet wurde -->
                                    <xsl:when test="count($content-types/content[@type != '_'][@nr=$maxOccur]) = 1">
                                        <xsl:value-of select="$content-types/content[@type != '_'][@nr=$maxOccur]/@type"/>
<!--                                        <xsl:message select="$content-types"></xsl:message>-->
                                    </xsl:when>
                                   
                                    <!-- wenn es mehrere Kodierungen fuer moegl. head-Zeilen gibt (z.B. 5 Mal i und 5 mal text), 
                                        wird type durch weitere Tests festgelegt: -->
                                    <xsl:otherwise>
                                        <xsl:variable name="headTypes">
                                            <xsl:sequence select="$content-types/content[@type != '_'][@nr=$maxOccur]"/>
                                        </xsl:variable>
                                        <xsl:choose>
                                            <xsl:when test="$headTypes/content[starts-with(@type,'elem-')]">
                                                <xsl:value-of select="$headTypes/content[starts-with(@type,'elem-')]/@type"/>
                                            </xsl:when>
                                            <xsl:when test="$headTypes/content[starts-with(@type,'text')]">
                                                <xsl:value-of select="$headTypes/content[starts-with(@type,'text')]/@type"/>
                                                <!--<xsl:value-of select="'text'"/>-->
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$headTypes/content[1]/@type"/>
                                            </xsl:otherwise>
                                        </xsl:choose>                                
                                        <!--<xsl:value-of select="$headTypes"/>-->
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                        </tg_headType>
                    </xsl:variable>
         
                    
                    <!--<xsl:sequence select="$headType"></xsl:sequence>-->
                    
                    <!-- Erzeugung von tg_div2 (etwas, was eine Ueberschrift und einen Rumpf hat), wo geeignet. 
                        Etwas wie hi (also kein tg_div2) ist: tg_grp[p[@tg_content = 'elem-span' or matches(@rendition,'^zenoP[CR]')]]-->
                    <!-- tg_grp[p[@tg_content = 'elem-span' or matches(@rendition,'^zenoP[CR]')]] -->
                    <xsl:for-each-group select="*" 
                        group-starting-with="tg_grp[@nr = '1'][@strlen='short']
                        [following-sibling::*[self::tg_grp][1][@strlen = 'short']]
                        [p[@tg_feat or starts-with(@tg_content,'elem')]
                        [(concat(@tg_content,'_',@rendition) = $headType/tg_headType/@value) or 
                        (concat(@tg_mixed,'_',@rendition) = $headType/tg_headType/@value) ]]
                        |tg_grp[@strlen='long']">
                        
                        <xsl:choose>
                            <xsl:when test="($headType/tg_headType/@occur != '1')
                                and  (count(current-group()[self::tg_grp]) gt 1)
                                and (current-group()[1][self::tg_grp[@nr = '1'][@strlen='short']
                                [following-sibling::*[self::tg_grp][1][@strlen = 'short']]
                                [p [(concat(@tg_content,'_',@rendition) = $headType/tg_headType/@value) or 
                                (concat(@tg_mixed,'_',@rendition) = $headType/tg_headType/@value) ] ]  ]  )">
                                <tg_div2 type="head+lg">        
                                   <!--  <xsl:copy-of select="$headType/tg_headType/@value"></xsl:copy-of>-->
                                    <xsl:copy-of select="current-group()"/>
                                </tg_div2>
                            </xsl:when>
                            <xsl:otherwise>
                              <!-- <aha> <xsl:copy-of select="$headType/tg_headType/@value"></xsl:copy-of></aha>-->
                                <xsl:copy-of select="current-group()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
