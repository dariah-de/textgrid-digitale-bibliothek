<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    
    
    <xd:doc scope="component">
        <doc:desc>
            <xd:p>Groups startin with line breaks, h4, when the classes change from one p to another, or where the ps differ in length</xd:p>
            <xd:p>Grpoups are surrounder with tg_group. tg_group has the attribute nr, the value is the number of the p-elements in the group.
                tg_group has the attribute strlen. When the group contains a p-element which is longer than 100 characters the value is 'long'.
                Otherwise the value is 'short' or zero.</xd:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="div[@type eq 'text']/div[lb or tg_new][@type eq 'h4']" mode="makeGroups">     
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="lb|tg_new[@type='h4-p' or @type='short-long' or @type='long-short' or @type='class']">
                <xsl:variable name="crtgrp">
                    <xsl:sequence select="current-group()"/>
                </xsl:variable>
               <!-- <xsl:message select="'start'">
                    <xsl:message select="$crtgrp"></xsl:message>
                </xsl:message>-->
                <xsl:choose>
                    <xsl:when test="count($crtgrp/p) gt 0"><!--
                        <xsl:message select="'makrGorpus'"></xsl:message>-->
                        <tg_grp nr="{count($crtgrp/p)}">
                            <xsl:attribute name="strlen">
                                <xsl:choose>
                                    <xsl:when test="$crtgrp/p[number(@tg_strlen) gt 100]">
                                        <xsl:value-of select="'long'"/>
                                    </xsl:when>
                                    <xsl:when test="$crtgrp/p[@tg_strlen != '0']">
                                        <xsl:value-of select="'short'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'0'"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:copy-of select="current-group()"/>
                        </tg_grp>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="current-group()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
