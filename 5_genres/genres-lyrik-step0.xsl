<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    
    
    
       
          
   <xd:doc scope="component">
       <xd:desc>
           <xd:p>@tg_strlen: the string without whitespace.</xd:p>
           <xd:p>p with text-node: marked as  @tg_content="text"</xd:p>
           <xd:p>p with one child-element and no text-node: marked as  @tg_content="elem-{name of the element}"</xd:p>
           <xd:p>p with more than one child-element and no text-node: marked as  @tg_content="elems"</xd:p>
           <xd:p>Others: 'empty'</xd:p>
           <xd:p> p with mixed content: marked as @tg_content="text" and @tg_mixed="elem-{name of the element}"</xd:p>
           <xd:p>@tg_feat</xd:p>
           <xd:p> possible values: "date;", "digits-only;", "digits-end;" (digits at the end of the string), "digits-start;",
               "date;align:center;", "digits-only;align:center;", "digits-end;align:center;" (digits at the end of the string), "digits-start;align:center;"
           </xd:p>
       </xd:desc>
   </xd:doc>
    <xsl:template match="text//p" mode="markP">
     <!--   <xsl:message select="'markPforContenttyp'"></xsl:message>-->
        <!-- Informationen zu p hinzufuegen -->
      <xsl:copy>
            <xsl:attribute name="tg_strlen" select="string-length(replace(replace(.,'\s+',' '),'^ $',''))"/>           
            
            <!-- 
                
                <xsl:variable name="hasNonEmptyTextNode">
                <xsl:choose>
                    <xsl:when test="text()[matches(.,'\S')]">
                        <xsl:value-of select="'yes'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'no'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
             <xsl:choose>
                    <xsl:when test="($hasNonEmptyTextNode = 'yes')">
                        <xsl:value-of select="'text'"/>
                    </xsl:when>
                    <xsl:when test="($hasNonEmptyTextNode = 'no') and (count(*)=1)">
                        <xsl:value-of select="concat('elem-',name(*))"/>
                    </xsl:when>
                    <xsl:when test="($hasNonEmptyTextNode = 'no') and (count(*) gt 1)">
                        <xsl:value-of select="'elems'"/>
                    </xsl:when>
                    <xsl:when test="($hasNonEmptyTextNode = 'no') and not(*)">
                        <xsl:value-of select="'empty'"/>
                    </xsl:when>
                </xsl:choose>
            
            -->
            
            <xsl:attribute name="tg_content" >
                <xsl:choose>
                    <xsl:when test="text()[matches(.,'\S')]"><!--  and not(*) -->
                        <xsl:value-of select="'text'"/>
                    </xsl:when>
                    <xsl:when test="count(*)=1">
                        <xsl:value-of select="concat('elem-',name(*))"/>
                    </xsl:when>
                    <xsl:when test="count(*) gt 1">
                        <xsl:value-of select="'elems'"/>
                    </xsl:when>
                    <xsl:when test="not(*)">
                        <xsl:value-of select="'empty'"/>
                    </xsl:when>
                </xsl:choose> 
            </xsl:attribute>
            <xsl:if test="matches(.,'\S') and (count(*) = 1)">
                <xsl:attribute name="tg_mixed" select="concat('elem-',name(*))"/>
            </xsl:if>  
            <xsl:variable name="features">
                <xsl:choose>
                    <!-- (k.343545.) 
                           <xsl:when
                        test="matches(.,'\d+') and matches(.,'(Januar|Jänner|Februar|März|April|Mai|Juni|Juli|August|September|O[ck]tober|November|De[cz]ember)')"
                        >date;</xsl:when>
                    <xsl:when test="matches(.,'^(.\.)?\s*\d+\.?\s*$')">digits-only;</xsl:when>
                    <xsl:when test="matches(.,'\d\.?\s*$')">digits-end;</xsl:when>
                    <xsl:when test="matches(.,'^\s*\d+\.?(-\d+)?[\.\)]\D+$')"
                        >digits-start;</xsl:when>
                </xsl:choose>
                <xsl:if test="starts-with(@class,'zenoPC')">align:center;</xsl:if>
                <xsl:if test="starts-with(@class,'zenoPR')">align:right;</xsl:if>
                <xsl:if test="starts-with(@class,'zenoPL')">align:left-indent;</xsl:if>
                    
                    -->
                    <xsl:when
                        test="matches(.,'\d+') and matches(.,'(Januar|Jänner|Februar|März|April|Mai|Juni|Juli|August|September|O[ck]tober|November|De[cz]ember)')"
                        >date;</xsl:when>
                    <xsl:when test="matches(.,'^(.\.)?\s*\d+\.?\s*$')">digits-only;</xsl:when>
                    <xsl:when test="matches(.,'\d\.?\s*$')">digits-end;</xsl:when>
                    <xsl:when test="matches(.,'^\s*\d+\.?(-\d+)?[\.\)]\D+$')"
                        >digits-start;</xsl:when>
                </xsl:choose>
                <xsl:if test="starts-with(@rendition,'#zenoPC')">align:center;</xsl:if>
                <xsl:if test="starts-with(@rendition,'#zenoPR')">align:right;</xsl:if>
                <xsl:if test="starts-with(@rendition,'#zenoPL')">align:left-indent;</xsl:if>
            </xsl:variable>
            <xsl:variable name="featuresAsString" select="string-join($features,'')"/>
            <xsl:if test="$featuresAsString != ''">
                <xsl:attribute name="tg_feat" select="$featuresAsString"/>
            </xsl:if>            
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
