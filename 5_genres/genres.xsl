<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Extracts finer grained metadata from the publicationStmt and the sourceDescrition
            of the teiHeader</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </xd:doc>


    <xsl:output indent="yes"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <xsl:include href="genres-drama.xsl"/>
    <xsl:include href="genres-lyrik-step0.xsl"/>
    <xsl:include href="genres-lyrik-step1.xsl"/>
    <xsl:include href="genres-lyrik-step2.xsl"/>
    <xsl:include href="genres-lyrik-step3.xsl"/>
    <xsl:include href="genres-lyrik-step4.xsl"/>
    <xsl:include href="genres-lyrik-step5.xsl"/>

    <!-- ***********************************************************************************  -->


    <!-- ******************************** Other variabels **************************************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>Name of the author.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="author">
        <xsl:value-of select="tei:teiCorpus[1]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"
        />
    </xsl:variable>
    <!-- ## Liste der Textsorten, für die das Mapping für Lyrik verwendet wird. -->
    <xsl:variable name="verseTypesAsText">Ballade, Bildergeschichten, Epos, Epen, Fabeln, Gedicht,
        Gedichte, Lehrgedicht, Liederbücher, Liedsammlung, Lyrik, Poetische Werke, Romanze, Satiren,
        Satirische Dichtung, Sinnsprüche, Versepos, Versepen, Verserzählung, Verserzählungen,
        Versfabeln, Versroman, </xsl:variable>

    <!-- ## Liste der Textsorten, für die das Mapping für Dramen verwendet wird. -->
    <xsl:variable name="dramaTypesAsText" xmlns="http://www.tei-c.org/ns/1.0">Drama, Dramen,
        Dramatische Dichtung, Dramatisches Gedicht, Dramenfragment, Dramenfragmente, Historien,
        Komödie, Komödien, Kunstdrama, Libretto, Libretti, Lyrisches Drama, Musikdramen,
        Puppenspiel, Schauspiel, Tragödie, Tragödien</xsl:variable>

    <!-- ## Liste einiger Textsorten, für die das Mapping für Prosa verwendet wird. 
        Dazu kommt alles andere, was nich als Lyrik oder Drama betrachtet wird. -->
    <xsl:variable name="proseTypesAsText">Abhandlung, Anstandsbuch, Aphorismen, Aufsatz,
        Autobiographie, Autobiographischer Roman, Erzählung, Essay, Fabel, Kompilationsliteratur,
        Märchen, Novelle, Prosa, Reisebeschreibung, Reiseerzählung, Roman, Romane, Sage,
        Theoretische Schrift, Volksbuch</xsl:variable>

    <!-- ## Diese Variable wird nicht verwendet. Sie wurde mit Hilfe der Mappings formuliert und 
        nennt bestimmte Inhalte zusammen mit dem zugehörigen Genre. -->
    <xsl:variable name="locations">
        <item key="Brant, Sebastian/Satire" genre="verse"/>
        <item key="Novalis/Fragmentensammlung/Glauben und Liebe/Blumen" genre="verse"/>
        <item key="Müller, Karl Theodor/Gedichte, Aufätze und Lieder" genre="verse"/>
        <item key="Boldt, Paul/Gedichte und Prosa/Junge Pferde! Junge Pferde!/Gedichte"
            genre="verse"/>
        <item key="George, Stefan/Gesamtausgabe der Werke" genre="verse"/>
        <item key="George, Stefan/Sonstige Werke" select="cat=Gedicht" genre="verse"/>
        <item key="Mühsam, Erich/Lyrik und Prosa/Sammlung 1898-1928/Erster Teil: Verse"
            genre="verse"/>
        <item key="Proelß, Johannes/Prosa und Lyrik/Katastrophen/Der Todesgruß auf der Taybrücke"
            genre="verse"/>
        <item
            key="Wackenroder, Wilhelm Heinrich/Schriften und Dichtungen/Phantasien über die Kunst für Freunde der Kunst/Zweiter Abschnitt/Der Traum"
            genre="verse"/>

        <item key="Claudius, Matthias/Gedichte und Prosa" genre="verse-or-prose"/>
        <item key="Busch, Wilhelm/Vermischtes" genre="verse-or-prose"/>

        <item
            key="George, Stefan/Gesamtausgabe der Werke/Tage und Taten. Aufzeichnungen und Skizzen"
            genre="prose"/>

        <item key="George, Stefan/Sonstige Werke/Phraortes [Dramatisches Fragement]" genre="drama"/>
        <item key="George, Stefan/Sonstige Werke/Graf Bothwell [Dramatisches Fragement]"
            genre="drama"/>
    </xsl:variable>

    <xsl:variable name="process-verse-in-prose-exclude">
        <item key="Goethe, Johann Wolfgang/Tagebücher" genre="prose"/>
    </xsl:variable>

    <xsl:variable name="proseTypes">
        <xsl:call-template name="textList2xmlList">
            <xsl:with-param name="textList" select="$proseTypesAsText"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="verseTypes">
        <xsl:call-template name="textList2xmlList">
            <xsl:with-param name="textList" select="$verseTypesAsText"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="dramaTypes" xmlns="http://www.tei-c.org/ns/1.0">
        <xsl:call-template name="textList2xmlList">
            <xsl:with-param name="textList" select="$dramaTypesAsText"/>
        </xsl:call-template>
    </xsl:variable>

    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*" mode="#all">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Idendity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@*|comment()|processing-instruction()" mode="#all">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- **************************************************************************************************  -->


    <!-- **************************  Creating the result document ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Creating a result-document</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="/">
        <!--  <xsl:message select="$dramaTypes/node()/@key"></xsl:message>-->
        <xsl:apply-templates/>
    </xsl:template>
    <!-- **************************************************************************************************  -->

    <!-- ************************************ Textgattungsebene *******************************************  -->
    <xsl:template match="teiCorpus[count(ancestor::teiCorpus) eq 1]/teiCorpus">
        <!--<xsl:message select="@n"/>-->
        <xsl:variable name="title" select="teiHeader/fileDesc/titleStmt/title[1]"/>
       <!-- <xsl:message select="$title"/>
        <xsl:message select="$dramaTypes/node()[@key eq $title]"/>
        <xsl:message select="$dramaTypes[item[@key eq $title]]"/>-->

        <xsl:choose>
            <!-- ## drama: -->
            <xsl:when
                test="$dramaTypes//node()[@key=$title] or $locations/node()[@genre='drama'][@key=@n]">
                <xsl:copy>
                    <xsl:message select="$title"></xsl:message>
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates select="@*|node()" mode="drama"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:copy-of select="@*"/>
                    <xsl:variable name="markPforContenttyp">
                        <xsl:apply-templates select="node()" mode="markP"/>
                    </xsl:variable>
                    <xsl:variable name="markNewClass">
                        <xsl:apply-templates select="$markPforContenttyp/*" mode="markNewClass"/>
                    </xsl:variable>
                    <xsl:variable name="makeGroups">
                        <xsl:apply-templates select="$markNewClass/*" mode="makeGroups"/>
                    </xsl:variable>
                    <xsl:variable name="markHeadType">
                        <xsl:apply-templates select="$makeGroups/*" mode="markHeadType"/>
                    </xsl:variable>
                    <xsl:variable name="linesAndSpeakers">
                        <xsl:apply-templates select="$markHeadType/*" mode="linesAndSpeakers"/>
                    </xsl:variable>
                    <xsl:variable name="closerAndHeads">
                        <xsl:apply-templates select="$linesAndSpeakers/*" mode="closerAndHeads"/>
                    </xsl:variable>
                    <!-- ## prosa und lyrik: -->
                   <!-- <xsl:copy-of select="$markHeadType"/>-->
                   <!-- <xsl:copy-of select="$linesAndSpeakers/*"/>-->
                    <xsl:copy-of select="$closerAndHeads/*"/>
                </xsl:copy>
                <!--   <xsl:apply-templates select="@*|node()" mode="verse"/>-->
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <!-- **************************************************************************************************  -->

    <!-- ************************** Priority 5 (highest priority): elements to be always copied ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <xd:p/>
        </doc:desc>
    </xd:doc>

    <xsl:template priority="5"
        match="div[not(descendant::p)][not(descendant::item)] | TEI[ends-with (@n, '/Biographie')]"
        mode="#all">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template priority="5" match="div[@type eq 'footnotes']" mode="markPforContenttyp">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- *****************************************************************************************  -->


    <xsl:template match="div[@type='footnotes']" priority="10" mode="#all">
        <!-- unabhängig von text/@tg_step werden Fußnoten nicht wie Gedichtinhalte behandelt, sondern kopiert. -->
        <xsl:copy-of select="."/>
    </xsl:template>
    

</xsl:stylesheet>
