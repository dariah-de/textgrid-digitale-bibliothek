<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    
    
    <xd:doc scope="component">
        <doc:desc>
            <xd:p>Groups startin with line breaks, h4, when the classes change from one p to another, or where the ps differ in length</xd:p>
            <xd:p>Grpoups are surrounder with tg_group. tg_group has the attribute nr, the value is the number of the p-elements in the group.
                tg_group has the attribute strlen. When the group contains a p-element which is longer than 100 characters the value is 'long'.
                Otherwise the value is 'short' or zero.</xd:p>
        </doc:desc>
    </xd:doc>
   <!-- <xsl:template match="text//tg_div2/tg_grp" mode="linesAndSpeakers p2Speaker">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>-->
    
    
    
    <xsl:template match="text//tg_new" mode="closerAndHeads p2seg" priority="2"/>
    <xsl:template match="text//@*[starts-with(name(),'tg_')]" mode="closerAndHeads"/>
  
  <!-- wurde nur fuer milestone; hi_start und hi_end verwendet -->
    <xsl:template match="text//div[descendant::lg or child::p ]//tg_grp[@TEI='hi']
        [   not(preceding-sibling::*[self::tg_div2]) 
        and not(preceding-sibling::*[self::lg])
        and not(preceding-sibling::*[self::p])   ]" mode="closerAndHeads">
        <xsl:copy>
            <xsl:apply-templates select="@*[not(name()='TEI')]|node()" mode="#current"/>
            <ah></ah>
        </xsl:copy>        
    </xsl:template>
    
    <xsl:template match="text//div[descendant::lg or child::p ]//tg_grp[@TEI='hi']
        [   not(following-sibling::*[self::sp or self::lg[@type eq 'complex']]) 
        and not(following-sibling::*[self::lg])
        and not(following-sibling::*[self::p])   ]" mode="closerAndHeads">
        <closer>
            <xsl:apply-templates select="@*|node()" mode="p2seg"/>
        </closer>        
    </xsl:template>
    <xsl:template mode="p2seg" match="p">
        <seg>
            <xsl:apply-templates select="@*|node()" mode="p2seg"/>  
        </seg>
    </xsl:template>
    
    <xsl:template match="text//tg_grp/@*[name()='nr' or name()='strlen']" mode="closerAndHeads"/>
    <xsl:template match="@tg_strlen|@tg_content|@tg_mixed|@tg_features" mode="closerAndHeads"/>
    <xsl:template match="text//tg_grp[not(@*)]" mode="closerAndHeads">
        <xsl:apply-templates mode="closerAndHeads"/>
    </xsl:template>
  
   <!-- <xsl:template match="text[@tg_step='lyrik-5']//tg_grp[@TEI='lg']/p" priority="3">
        <xsl:copy>
            <xsl:attribute name="TEI" select="'l'"/>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>-->
    
   <!-- <xsl:template match="text[@tg_step='lyrik-5']//tg_div2/@type"/>-->
    
   <!-- <xsl:template match="text[@tg_step='lyrik-5']//tg_grp[@TEI='speaker' or @TEI='div2-head']">
        <xsl:apply-templates/>
    </xsl:template>-->
    
    <!--  <xsl:template match="text[@tg_step='lyrik-5']//tg_grp[@TEI='speaker' or @TEI='div2-head']/p" priority="3">
        <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <xsl:attribute name="TEI" select="parent::*/@TEI"/>
        <xsl:apply-templates/>
        </xsl:copy>
        </xsl:template>-->
    
    <!--<xsl:template match="text[@tg_step='lyrik-5']//tg_div/p" priority="3">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="TEI" select="'p'"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>-->
    
   <!-- <xsl:template match="text[@tg_step='lyrik-5']//p" priority="0">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="TEI" select="''"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>-->
</xsl:stylesheet>
