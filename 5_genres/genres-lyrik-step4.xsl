<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">
    
    
    <xd:doc scope="component">
        <doc:desc>
            <xd:p>Groups startin with line breaks, h4, when the classes change from one p to another, or where the ps differ in length</xd:p>
            <xd:p>Grpoups are surrounder with tg_group. tg_group has the attribute nr, the value is the number of the p-elements in the group.
                tg_group has the attribute strlen. When the group contains a p-element which is longer than 100 characters the value is 'long'.
                Otherwise the value is 'short' or zero.</xd:p>
        </doc:desc>
    </xd:doc>
    <!-- wurde  fuer milestone; hi_start und hi_end verwendet, momentang nur noch fuer closer -->
    <xsl:template priority="3"
        match="text//tg_grp[not(parent::tg_div2)][p[@tg_content = 'elem-span' or matches(@class,'^zenoP[CR]')]]" mode="linesAndSpeakers">
    <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:attribute name="TEI" select="'hi'"/>
        <xsl:apply-templates/>
    </xsl:copy>
    </xsl:template>
    
    
    <xsl:template  match="text//tg_grp[@strlen='long']" mode="linesAndSpeakers">
        <!-- [not(parent::tg_div2)] -->
        <!-- aufloesen: Gruppen von langen Zeilen -->
        <xsl:apply-templates mode="delete-groups" />
    </xsl:template>
    
    <xsl:template
        match="text//tg_grp[not(parent::tg_div2)][@nr='1']
        [following-sibling::*[self::tg_grp][1][@strlen='long']
        or preceding-sibling::*[self::tg_grp][1][@strlen='long'] ]" mode="linesAndSpeakers" priority="1">
        <!-- aufloesen: eine Gruppe aus einer kurzen Zeile vor oder nach einer Gruppe von langen Zeilen  -->
        <xsl:apply-templates mode="delete-groups" />
    </xsl:template>
    
    <xsl:template match="text//tg_div2[tg_grp]" mode="linesAndSpeakers">
        <xsl:choose>
            <xsl:when test="matches(*[self::tg_grp][1]/p/@tg_feat,'(digits-only|date)')">
                <lg>
                    <xsl:attribute name="type" select="'complex'"/>
                    <xsl:apply-templates select="@*[not(name() eq 'type')]|node()" mode="linesAndSpeakers"/>
                </lg>
            </xsl:when>
            <xsl:otherwise>
                <sp> 
                    <xsl:apply-templates select="@*[not(name() eq 'type')]|node()" mode="linesAndSpeakers"/>
                </sp>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="text//tg_div2/*[self::tg_grp][1]" mode="linesAndSpeakers">
                <xsl:choose>
                    <xsl:when test="matches(p/@tg_feat,'(digits-only|date)')">
                            <xsl:apply-templates select="node()" mode="p2Line"/>
                    </xsl:when>
                    <xsl:otherwise>
                            <xsl:apply-templates select="node()" mode="p2Speaker"/>                        
                    </xsl:otherwise>
                </xsl:choose>
    </xsl:template>
    
    <!-- aus zeno2tei -->
    
    <xsl:template match="text//tg_div2/tg_grp" mode="linesAndSpeakers p2Speaker" priority="-1">
      <xsl:apply-templates mode="#current"/>
    </xsl:template>
    
    
    <xsl:template mode="p2Line" match="p">
        <l>
            <xsl:apply-templates select="@*|node()" mode="p2Speaker"/>  
        </l>
    </xsl:template>
    <xsl:template mode="p2Speaker" match="p">
        <speaker>
            <xsl:apply-templates select="@*|node()" mode="p2Speaker"/> 
        </speaker>
    </xsl:template>
    
    <xsl:template match="@tg_strlen|@tg_content|@tg_mixed|@tg_feat|@nr|@strlen" mode="p2Line p2Speaker delete-groups"/> 
    <xsl:template
        match="text//tg_grp[not(parent::tg_div2)][@strlen eq 'short']
        |text//tg_div2/*[self::tg_grp][position() gt 1][@strlen eq 'short']" mode="linesAndSpeakers">
        <!-- lg: kurze Zeilen, entweder in tg_div2 nach Position 1, oder in tg_div -->
        <lg>
            <xsl:apply-templates select="@*|node()" mode="p2Line"/>
        </lg>
    </xsl:template>
</xsl:stylesheet>
