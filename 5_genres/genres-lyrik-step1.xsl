<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

   
    <xd:doc scope="component">
        <doc:desc>
            <xd:p>Element tg_new with @type is added with different values when the preceding-sibling is a p or a head [@type eq 'h4']</xd:p>
            <xd:p></xd:p>
            <xd:p></xd:p>
            <xd:p></xd:p>
            <xd:p></xd:p>
        </doc:desc>
    </xd:doc>
    <!-- [@type eq 'h4' or @type eq 'text']: die divs die durch Dogerus Heuristiken erzeugt wurden. -->
    <xsl:template match="div[@type eq 'h4' or @type eq 'text']//p" mode="markNewClass"><!--
        <xsl:message select="'long-short'"></xsl:message>-->
        <xsl:variable name="prec" select="preceding-sibling::*[self::p or self::head[@type eq 'h4']][1]"/>
        <xsl:choose>
            <xsl:when test="$prec[self::p]">
                <xsl:choose>
                    <!-- classes of the two ps are different. p does'nt have a class but preceding has. p has class but preceding has not. -->
                    <xsl:when
                        test="(@rendition != $prec/@rendition) or (not(@rendition) and $prec/@rendition) or (@rendition and not($prec/@rendition))">
                        <tg_new type="class"/>
                    </xsl:when>
                    <!--both have the same class -->
                    <xsl:when
                        test="not(@rendition and $prec/@rendition and (@rendition = $prec/@rendition)) and (number($prec/@tg_strlen) gt 99) and (number(@tg_strlen) lt 100) ">
                        
                        <tg_new type="long-short"/>
                    </xsl:when>
                    <xsl:when
                        test="not(@rendition and $prec/@rendition and (@rendition = $prec/@rendition)) and (number($prec/@tg_strlen) lt 100) and (number(@tg_strlen) gt 99) ">
                        <tg_new type="short-long"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$prec[head[@type eq 'h4']]">
                        <tg_new type="h4-p"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
