<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:a="http://www.textgrid.info/namespace/digibib/authors"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg xd a fn xd tg" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    version="2.0">

    <xsl:param name="verse-max-length" select="100"/>
    <xsl:param name="threshold-verse-prose-nroflonglines" select="5"/>
    <xsl:variable name="_verse-max-length_" select="number($verse-max-length)"/>
    <xsl:variable name="_threshold-verse-prose-nroflonglines_"
        select="number($threshold-verse-prose-nroflonglines)"/>

    <!-- ## mode beachten! 
    Nur einige Templates verwenden mode="drama" (bei Lyrik ist es ähnlich):
    Die Templates mit mode verarbeiten einen Inhalt entweder endgültig, oder sie erzeugen unter
    anderem das Attribut @tg_step (="drama-1") und lassen die Verarbeitung in mehreren Schritten
    laufen. Änderungen, die in einem bestimmten Schritt gemacht werden, werden durch Templates (im
    default-Modus) realisiert, die @tg_step berücksichtigen.   
    Die Default-Templates (mit mode="#all") sorgen für das Kopieren und Weiterverarbeiten der Inahlte,
    wenn es keine spezielle Templates gibt. -->

    <!-- ## text: Verarbeitung in Schritten. Das Template ist benannt und damit auch für andere
        templates zugänglich, was nötig ist bei der Verarbeitung des Abschnitts mit den Personen eines
        Dramas, wenn im gleichen Abschnitt der Haupttext enthalten ist. Da für jenen Abschnitt das
        template für Personen zuständig ist, muss der Haupttext von der Personenliste getrennt
        werden und dann mit diesem Template verarbeitet werden. -->
    <xsl:template mode="drama" name="text-steps"
        match="div[@type eq 'text' or starts-with(@type, 'h')]" priority="1">
        <!--   <xsl:message select="'aha'"></xsl:message>-->
        <xsl:variable name="text-step1">
            <xsl:copy>
                <xsl:copy-of select="@*"/>
                <xsl:apply-templates mode="drama-1"/>
            </xsl:copy>
        </xsl:variable>
        <xsl:variable name="drama-2">
            <!-- dient nur noch zum Löschen von leeren hi und p Elemente, die in Schritt eins,
            durch die Aufsplittung von Sprechername und stage entstanden sind.
            -->
            <xsl:apply-templates select="$text-step1/node()" mode="drama-2"/>
        </xsl:variable>
        <xsl:variable name="drama-3">
            <!-- hi zu stage. Gruppierung von lg innerhalb eines Sprechansatzes -->
            <xsl:apply-templates select="$drama-2/node()" mode="drama-3"/>
        </xsl:variable>
        <xsl:variable name="drama-4">
            <xsl:apply-templates select="$drama-3/node()" mode="drama-4"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$drama-3/*[@type eq 'temp_maintext']">
                <xsl:apply-templates select="$drama-4/node()" mode="temp-maintext"/>
            </xsl:when>
            <xsl:otherwise>
                <div>
                    <xsl:apply-templates select="$drama-4/*/@*[not(name() eq 'tg_step')]"/>
                    <xsl:apply-templates select="$drama-4/node()"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- ## Personen sind manchmal nicht vom Haupttext getrennt.
    Hier wird zuerst Struktur erzeugt. -->
    <xsl:template priority="10" mode="drama"
        match="div[ends-with(normalize-space(@n), 'Personen')]/div[@type eq 'text']">
        <!--<xsl:message select="'aha'"></xsl:message>-->
        <!--  (:# [ p[matches(normalize-space(),'^\p{Lu}{2,}(\s+.*)?\.')] ] #:) -->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="p[matches(normalize-space(),'^\p{Lu}{2,}(\s+.*)?\.')][not( preceding-sibling::*[self::p[matches(normalize-space(),'^\p{Lu}{2,}(\s+.*)?\.')]])]">
                <xsl:choose>
                    <xsl:when
                        test="current-group()[1][self::p[matches(normalize-space(),'^\p{Lu}{2,}(\s+.*)?\.')][not( preceding-sibling::*[self::p[matches(normalize-space(),'^\p{Lu}{2,}(\s+.*)?\.')]])]]">
                        <xsl:variable name="maintext">
                            <text type="temp_maintext">
                                <xsl:copy-of select="current-group()"/>
                            </text>
                        </xsl:variable>
                        <xsl:for-each select="$maintext/*">
                            <xsl:call-template name="text-steps"/>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="castList__set">
                            <div type="castList+set">
                                <xsl:copy-of select="current-group()"/>
                            </div>
                        </xsl:variable>
                        <xsl:for-each select="$castList__set/*">
                            <!-- ## for-each ändert den Kontext. Auf diese Weise
                                ist kein Parameter nötig für das Template
                                castList__set. -->
                            <xsl:variable name="wrapped">
                                <xsl:call-template name="castList__set"/>
                            </xsl:variable>
                            <xsl:sequence select="$wrapped/*/node()"/>
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <!-- ## castList + set (benanntes Template).
        Zuerst Gruppen erstellen, die mit h4 anfangen.
        Dann in diesen Gruppen weitere Gruppen machen, um castGroup von set zu trennen.
        set fängt an mit zentriertem Text (@rendition="#zenoPC"), was zur Trennung verwendet wird.
        castGroup wird nur dann erstellt, wenn der aktuelle Kontext nicht mit zentriertem Text
        anfängt.
    -->
    <xsl:template name="castList__set">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <!--<xsl:attribute name="TEI" select="'castList'"/>-->
            <xsl:variable name="h4-castGrp-set">
                <xsl:for-each-group select="node()"
                    group-starting-with="head[@type eq 'h4'][not(preceding-sibling::*[1][self::head[@type eq 'h4']])]
                    | *[not(self::head[@type eq 'h4'])][preceding-sibling::*[1][self::head[@type eq 'h4']]]">
                    <xsl:choose>
                        <xsl:when
                            test="not(current-group()[self::*]) or current-group()[1][self::head[@type eq 'h4']]">
                            <xsl:apply-templates select="current-group()"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:variable name="crtGrp">
                                <xsl:sequence select="current-group()"/>
                            </xsl:variable>
                            <xsl:variable name="all-divs-starting-with-zenopc"
                                select="if ( $crtGrp/p[@rendition eq '#zenoPC']
                                [not(preceding-sibling::p)]
                                [following-sibling::
                                p [not(@rendition eq '#zenoPC')]
                                [following-sibling::p[@rendition eq '#zenoPC']] ] ) 
                                then true() 
                                else false()"> </xsl:variable>

                            <xsl:for-each-group select="$crtGrp/node()"
                                group-starting-with="
                                p[@rendition eq '#zenoPC']
                                [not(preceding-sibling::p[1][@rendition eq '#zenoPC'])]
                                [not(following-sibling::p[1][not(@rendition) or (@rendition ne '#zenoPC')])]">
                                <xsl:choose>
                                    <xsl:when
                                        test="not($all-divs-starting-with-zenopc) and
                                        current-group()[1]
                                        [self::p[@rendition eq '#zenoPC'][not(normalize-space() eq '')]
                                        [not(preceding-sibling::p[1][@rendition eq '#zenoPC'])]
                                        [not(following-sibling::p[1][not(@rendition) or (@rendition ne '#zenoPC')])] ]
                                        ">
                                        <div type="set" xmlns="http://www.tei-c.org/ns/1.0">
                                            <xsl:apply-templates select="current-group()"
                                                mode="#current"/>
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each-group select="current-group()"
                                            group-starting-with="p[@rendition eq '#zenoPC']
                                            [not(preceding-sibling::p[1][@rendition
                                            eq '#zenoPC'])]">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="current-group()[self::p[not(@rendition
                                                    eq '#zenoPC')] ]">
                                                  <castGroup>
                                                  <xsl:apply-templates select="current-group()"
                                                  mode="cast"/>
                                                  </castGroup>
                                                </xsl:when>
                                                <xsl:when
                                                  test="every $x in
                                                    current-group()[self::p] satisfies
                                                    $x[@rendition eq '#zenoPC']">
                                                  <div type="set">
                                                  <xsl:apply-templates select="current-group()"
                                                  mode="#current"/>
                                                  </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:choose>
                                                  <xsl:when test="current-group()[self::*]">
                                                  <div>
                                                  <xsl:apply-templates select="current-group()"
                                                  mode="#current"/>
                                                  </div>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:apply-templates select="current-group()"
                                                  mode="#current"/>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:for-each-group>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each-group>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each-group>
            </xsl:variable>
            <xsl:for-each-group select="$h4-castGrp-set/node()"
                group-starting-with="head[@type eq 'h4'][not(preceding-sibling::*[1][self::head[@type eq 'h4']])] 
                | div[@type eq 'set']">
                <xsl:choose>
                    <xsl:when test="current-group()[1][self::head[@type eq 'h4']]">
                        <div type="Dramatis_Personae">
                            <castList>
                                <xsl:copy-of select="current-group()"/>
                            </castList>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="current-group()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <!-- ## p innerhalb der Personenliste: -->
    <xsl:template match="p" mode="cast">
        <xsl:choose>
            <xsl:when test="@rendition eq '#zenoPC'"/>
            <xsl:when test="normalize-space(.) eq ''"> </xsl:when>
            <xsl:when test="not(@rendition)">
                <castItem n="free-text">
                    <xsl:apply-templates select="@* | node()"/>
                </castItem>
            </xsl:when>
            <!-- ### check -->
            <xsl:otherwise>
                <castItem>
                    <xsl:apply-templates select="@* | node()"/>
                </castItem>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



    <!-- ## Gruppierung nach Sprechern. Erkennung/Kodierung der Sprecher-Angabe (später tei:speaker). 
        Die Sprecher-Angabe ist nicht in einem eigenen Element kodiert. Sie steht am Anfang eines p, vor Punkt, und ist groß geschrieben. 
        Auch vor Punkt, aber nach dem Sprecher, können weitere Angaben (etwa Bühnenweisungen) existieren. -->
    <!-- <xsl:template
        match="div[@tg_step eq 'drama-1']//div[ancestor-or-self::div[matches(@n, 'akt', 'i')or matches(@n, 'szene', 'i') or matches(@n, 'scene', 'i')]][p[matches(normalize-space(.),'^\p{Lu}{2,}(\s+.*)?\.')]]"
        priority="10">-->
    <!-- text[@tg_step eq 'drama-1']
        /tg_div[not(@type eq 'footnotes')]" -->
    <xsl:template match="div[@type eq 'text']/div[not(@type eq 'footnotes')]" priority="10"
        mode="drama-1">
        <!--  <xsl:message select="@n"/>-->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <!--                 group-starting-with="p
                [not(matches(normalize-space(text()[1]),'^\p{Lu}\p{Ll}[^\.]*\.?'))]                
                [matches(normalize-space(.),'^\p{Lu}+([\.,\s]+(\p{Lu}+)?|$)')]" -->
            <xsl:for-each-group select="node()"
                group-starting-with="p[matches(normalize-space(.),'^\p{Lu}{2,}(\s+.*)?\.')](:großbuchstaben, mindestens 2,whitespace(mind. 1 mal) beliebiges Zeichen*)?:)">
                <!--   <xsl:message select="self::node()"/>-->
                <xsl:choose>
                    <xsl:when test="self::p[matches(normalize-space(.),'^\p{Lu}{2,}(\s+.*)?\.')]">
                        <xsl:variable name="first-in-grp" select="current-group()[1]"/>

                        <!-- ## $first-in-grp ist
                                das erste p in der Gruppe, das mit einem groß
                                geschreibenen Sprechernamen beginnt. -->
                        <!-- ## Der Sprechertext wird hier mit tg_grp @TEI="sp" kodiert: -->
                        <sp>
                            <xsl:choose>
                                <!-- ## Wenn das erste p nur den Sprechernamen
                                    enthält, dann wird einfach ein Container erzeugt:-->
                                <xsl:when test="matches($first-in-grp,'^\p{Lu}+\.\s*$')">
                                    <!--  or not(matches($first-in-grp,'\.')) -->
                                    <speaker>
                                        <xsl:copy-of select="self::*"/>
                                    </speaker>
                                </xsl:when>
                                <xsl:when test="not(matches($first-in-grp,'\.'))">
                                    <xsl:copy-of select="current-group()[1]"/>
                                </xsl:when>

                                <xsl:otherwise>
                                    <!-- ## Beispiel für diese Situation (betrifft diese otherwise-Anweisung): 
 vor:  <p tgID="tg1109.2.8">METZLER <i tgID="tg1109.2.8.1">leise zu Sievers.</i> Erzähl das noch einmal vom Berlichingen![...]</p>

nach: <tg_grp TEI="sp">
        <tg_speaker>
          <p tgID="tg1109.2.8.part1">METZLER <i tgID="tg1109.2.8.1.part1" TEI="stage">leise zu Sievers.</i></p>
        </tg_speaker>
        <tg_grp TEI="lg">
          <p tgID="tg1109.2.8.part2"> Erzähl das noch einmal vom Berlichingen![...]</p>
        </tg_grp>
      </tg_grp>
-->
                                    <xsl:variable name="first-in-grp" select="current-group()[1]"/>
                                    <xsl:variable name="first-text-with-period"
                                        select="$first-in-grp/(descendant::text()[contains(.,'.')])[1] (:erster textknoten, der einen Punkt enthält:)"/>
                                    <speaker>
                                        <!-- fuer p mit Sprechermuster(SPRECHER.) -->
                                        <xsl:call-template name="extract-range">
                                            <xsl:with-param name="before-or-after" select="'before'"
                                                tunnel="yes"/>
                                            <xsl:with-param name="context" select="$first-in-grp"/>
                                            <xsl:with-param name="target-node"
                                                select="$first-text-with-period" tunnel="yes"/>
                                            <xsl:with-param name="target-string" select="'.'"
                                                tunnel="yes"/>
                                            <xsl:with-param name="tgID_suffix" select="'.part1'"
                                                tunnel="yes"/>
                                        </xsl:call-template>
                                        <!-- <div>hier</div>-->
                                    </speaker>
                                    <xsl:text>&#xA;</xsl:text>
                                    <xsl:call-template name="extract-range">
                                        <xsl:with-param name="tgID_suffix" select="'.part2'"
                                            tunnel="yes"/>
                                        <xsl:with-param name="context" select="$first-in-grp"/>
                                        <xsl:with-param name="target-node"
                                            select="$first-text-with-period" tunnel="yes"/>
                                        <xsl:with-param name="before-or-after" select="'after'"
                                            tunnel="yes"/>
                                        <xsl:with-param name="target-string" select="'.'"
                                            tunnel="yes"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                            <!-- ## Hier die p-Elemente nach dem ersten: -->
                            <xsl:copy-of select="current-group()[position() gt 1]"/>
                        </sp>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="current-group()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <!-- ## Benanntes Template zum Extrahieren des Inhalts vor oder nach einem String in einem
        Element, egal wie tief. Die Struktur wird erhalten. -->
    <xsl:template name="extract-range">
        <xsl:param name="before-or-after" tunnel="yes"/>
        <!-- context: erstes Element der Gruppe: p[matches(normalize-space(.),'^\p{Lu}{2,}(\s+.*)?\.')] -->
        <xsl:param name="context"/>
        <!-- target-node: erster Textknoten, der einen Punkt enthaelt -->
        <xsl:param name="target-node" tunnel="yes"/>
        <!-- target:string: erster Textknoten mit Punkt -->
        <xsl:param name="target-string" tunnel="yes"/>
        <xsl:choose>

            <xsl:when test="$before-or-after eq 'before'">
                <xsl:if
                    test="($context &lt;&lt; $target-node) 
                    or (. is $target-node)">
                    <xsl:choose>
                        <xsl:when test="self::*">
                            <xsl:copy>
                                <xsl:apply-templates select="@*"/>
                                <!--      <ooho/>-->
                                <xsl:for-each select="node()">
                                    <xsl:call-template name="extract-range">
                                        <xsl:with-param name="context" select="."/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </xsl:copy>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <!-- Textknoten, die keinen -->
                                <xsl:when test="not(. is $target-node)">
                                    <xsl:copy-of select="."/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <!-- Target-node: Erste Textknoten, der einen Punkt enthaelt. Kopiert wird dieser Knoten bis einschliesslich des Punktes. -->
                                    <xsl:if test="self::text()">
                                        <xsl:value-of
                                            select="concat(substring-before(.,$target-string),$target-string)"
                                        />
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </xsl:when>
            <!-- otherwise: zur Extraktion des Textteils nach dem Punkt -->
            <xsl:otherwise>
                <xsl:if
                    test="($context &gt;&gt; $target-node) 
                    or (. is $target-node)
                    or ($target-node[ancestor::*[. is $context]])">
                    <xsl:choose>
                        <xsl:when test="self::*">
                            <xsl:copy>
                                <xsl:apply-templates select="@*"/>
                                <xsl:for-each select="node()">
                                    <xsl:call-template name="extract-range">
                                        <xsl:with-param name="context" select="."/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </xsl:copy>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="not(. is $target-node)">
                                    <xsl:copy-of select="."/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <!-- Gebe Text nach dem Punkt aus. -->
                                    <xsl:if test="self::text()">
                                        <xsl:value-of select="substring-after(.,$target-string)"/>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </xsl:otherwise>

        </xsl:choose>
    </xsl:template>


    <!--  <xsl:template
        match="div[@tg_step eq 'drama-2']//p[@rendition='#zenoPC']/hi[@rend eq 'italic']
        |div[not(ancestor::div)][@tg_step eq 'drama-2']//tg_speaker//hi[@rend eq 'italic']">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="TEI" select="'stage'"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>-->
    <xsl:template
        match="div[@type eq 'text']//p[@rendition='#zenoPC'][hi[@rend eq 'italics']] 
        |div[@type eq 'text']//speaker/p[hi[@rend eq 'italics']]"
        mode="drama-3">
        <!-- Warum ist in birch-pferiffer xml:id="tg7.2.8.2" keine stage? -->
        <stage>
            <xsl:copy>
                <xsl:copy-of select="@*"/>
                <xsl:apply-templates select="node()"/>
            </xsl:copy>
        </stage>
    </xsl:template>
    <!-- <xsl:template
        match="div[not(ancestor::div)][@tg_step eq
        'drama-3']//p[@rendition='#zenoPC'][hi[@rend eq 'italic'][@TEI eq 'stage']][count(*) eq 1][not(text()[normalize-space() ne ''])]">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="TEI" select="'stage'"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>-->

    <xsl:template
        match="div[@type eq 'text']//p[@rendition='#zenoPC']
        [parent::speaker//hi[@rend eq 'italics'] or hi[@rend eq 'italics']][count(*) eq 1][not(text()[normalize-space() ne ''])]|
        div[@type eq 'text']//speaker[p/node()[1][self::text()] or node()[1][self::text()]]/p[hi[@rend eq 'italics']]"
        mode="drama-3" priority="1">      
        <stage>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()" mode="#current"/>
        </stage>
    </xsl:template>
    <!--<!-\-  <xsl:template
        match="div[@tg_step eq 'drama-2']//p[@rendition='#zenoPC']/hi[@rend eq 'italic']
        |div[not(ancestor::div)][@tg_step eq 'drama-2']//tg_speaker//hi[@rend eq 'italic']">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="TEI" select="'stage'"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>-\->-->
   <!-- <xsl:template
        match="div[@type eq 'text']//speaker[p/node()[1][self::text()[matches(., '\S')]] or node()[1][self::text()[matches(., '\S')]]][.//hi[@rend eq 'italics']]"
        mode="drama-3"> -->
        <xsl:template
            match="div[@type eq 'text']//speaker[p/text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]]"
            mode="drama-3">     
        <!--<xsl:message select="'sososoos'"></xsl:message>
        <da></da>-->
        <!--   <xsl:copy-of select="."/>-->
            <xsl:variable name="speakername" select="p/text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <p>
               <!-- <xsl:copy-of select="p/@*"/>-->
                <xsl:copy-of select="p/text()[matches(., '\S')][1]/preceding-sibling::node()"/>
                <xsl:value-of select="replace($speakername,'\s+$','')"/>
            </p>
        </xsl:copy>
        <xsl:text> </xsl:text>
  <!--   <xsl:apply-templates select="p/node()[position() gt 1]" mode="#current"/>-->
        <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:template>
    
    <xsl:template
        match="div[@type eq 'text']//speaker[text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]
        and .//hi[@rend eq 'italics']
        ]"
        mode="drama-3">     
        <!--<xsl:message select="'sososoos'"></xsl:message>
        <da></da>-->
        <!--   <xsl:copy-of select="."/>-->
        <xsl:variable name="speakername" select="text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <p>
                <!-- <xsl:copy-of select="p/@*"/>-->
                <xsl:copy-of select="text()[matches(., '\S')][1]/preceding-sibling::node()"/>
                <xsl:value-of select="replace($speakername,'\s+$','')"/>
            </p>
        </xsl:copy>
        <xsl:text> </xsl:text>
        <!--   <xsl:apply-templates select="p/node()[position() gt 1]" mode="#current"/>-->
        <xsl:apply-templates select="node()[not(.//@xml:id[ends-with(., 'part1')])]" mode="#current"/>
    </xsl:template>
    
    <xsl:template match="speaker/p/text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]"  mode="drama-3"/>
    <xsl:template match="speaker/p[text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]]/node()
        [following-sibling::text()[matches(., '\S')][1][not(preceding-sibling::* except preceding-sibling::pb)]]"  mode="drama-3"/>
    <xsl:template match="@xml:id" priority="10">
        <xsl:param name="tgID_suffix" select="''" tunnel="yes"/>
        <xsl:attribute name="{name()}" select="concat(.,$tgID_suffix)"/>
    </xsl:template>



    <!-- <xsl:template
        match="div[not(ancestor::div)][@tg_step eq
        'drama-3']//tg_speaker[p/node()[1][self::text()]][.//hi[@rend eq 'italic']][p/i[@TEI eq 'stage']]">
        <xsl:variable name="speakername" select="p/node()[1]"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <p>
                <xsl:copy-of select="p/@*"/>
                <xsl:value-of select="replace($speakername,'\s+$','')"/>
            </p>
        </xsl:copy>
        <xsl:text> </xsl:text>
        <p TEI="stage">
            <xsl:copy-of select="p/node()[position() gt 1]"/>
        </p>
    </xsl:template>
    -->

    <!-- ## Gruppierung mit lg für Verse: -->
    <xsl:template match="div[@type eq 'text']//sp" priority="2" mode="drama-3">
        <!--  <xsl:message>söööööö</xsl:message>-->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="
                (:#1:) lb
                (:#2:) |p[@rendition eq '#zenoPC']
                (:#3:) |p[starts-with(@rendition,'#zenoPR')]
                (:#4:) |p[not(normalize-space() eq '')][not(@rendition eq '#zenoPC')][not(stage)][not(starts-with(@rendition,'#zenoPR'))] 
                (:#5:) [not(preceding-sibling::*[1][self::p[not(normalize-space() eq '')][not(@rendition eq '#zenoPC')][not(stage)][not(starts-with(@rendition,'#zenoPR'))]])] 
                
                (:# #6 und #4 sind gleich. #5 bis #7 bedeuten, dass nur das
                erste p, das die Bedingungen unter #4 erfüllt, als Beginn einer Gruppe
                verwendet wird, wenn es mehrere solche p nacheinander stehen.#:)
                ">
                <xsl:choose>
                    <xsl:when
                        test="current-group()[1][self::p[not(normalize-space() eq '')][not(@rendition eq '#zenoPC')][not(stage)][not(starts-with(@rendition,'#zenoPR'))]]  
                        (:# #9 und #4 sind gleich. Wenn das Erste in der Gruppe
                        ein p ist (wie unter #4 angegeben), dann einen
                        lg-Container erzeugen (eigtl. tg_grp), wenn in der
                        Gruppe nur kurze Zeilen sind (also Verse). #:)">
                        <xsl:choose>
                            <xsl:when
                                test="current-group()[self::p[string-length(normalize-space(.))
                                 gt $_verse-max-length_ ]] (:# lange Zeilen bedeuten normalerweise Prosa #:)">
                                <xsl:apply-templates select="current-group()" mode="no-lg__prose"/>
                            </xsl:when>
                            <!-- lg nur wenn es kurze Zeilen sind: -->
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="count(current-group()[self::p]) gt 1">
                                        <lg>
                                            <xsl:apply-templates select="current-group()" mode="lg"
                                            />
                                        </lg>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="current-group()" mode="lg"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="current-group()" mode="#current"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <!-- ## Templates zur Erzeugung des Attributs @TEI bei p:  -->
    <xsl:template match="p[not(@TEI)][not(parent::speaker)][not(lg)]" mode="lg">
        <xsl:variable name="length" select="string-length(normalize-space())"/>
        <xsl:choose>
            <xsl:when test="$length eq 0">
                <milestone unit="p"/>
            </xsl:when>
            <xsl:when test="$length gt $verse-max-length">
                <xsl:copy>
                    <xsl:copy-of select="@*|node()"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <l>
                    <xsl:copy-of select="@*|node()"/>
                </l>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template
        match="p[not(@TEI)][not(parent::speaker)][not(lg)]|l[not(parent::speaker)][not(lg)]"
        mode="no-lg__prose">
        <xsl:variable name="length" select="string-length(normalize-space())"/>
        <xsl:choose>
            <xsl:when test="string-length() eq 0">
                <milestone unit="p"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:copy-of select="@*"/>
                    <xsl:copy-of select="node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ## Zur Vereinhetlichung: Änderung aller p/@TEI innerhalb einer Texteinheit 
        zu 'p' oder 'l' (bei den p, die normalen Text enthalten, und nicht in lg stehen).
    Eine zu große Alternation von langen und kurzen Zeilen (p und l) soll es nicht geben. Zu viel
    oder relativ viel lange Zeilen deuten auf Prosa hin (dann die l zu p machen). -->
    <xsl:template match="div[@type eq 'text']" priority="2" mode="drama-4">
        <!-- [not(normalize-space() eq '')][not(@rendition eq '#zenoPC')] 
        [not(*[@TEI eq 'stage'])][not(starts-with(@rendition,'#zenoPR'))]"-->
        <xsl:copy>
            <xsl:copy-of select="@*[not(name(.) eq 'tg_step')]"/>
            <xsl:variable name="long"
                select="count(descendant::p
                [string-length(normalize-space()) gt $_verse-max-length_][not(@rendition eq '#zenoPC')][not(starts-with(@rendition,'#zenoPR'))]
                )"/>
            <xsl:variable name="short"
                select="count(descendant::p
                [string-length(normalize-space()) lt $_verse-max-length_][not(@rendition eq '#zenoPC')] 
                [not(stage)][not(starts-with(@rendition,'#zenoPR'))]
                )"/>
            <xsl:variable name="ratio-long"
                select="if (($long + $short) gt 0) 
                then ($long div ($long + $short)) 
                else 0"/>
            <!-- ## $ratio-long enthält das Verhältnis der langen Zeilen gegenüber aller Zeilen.
            Es könnte, falls sinnvoll, ein extra globaler Parameter definiert werden, womit
            $ratio-long verglichen werden kann, um festzulegen, ob ein Abschnitt als Lyrik oder als
            Prosa betrachtet werden soll. -->
            <xsl:choose>
                <xsl:when
                    test="($ratio-long gt 0) and ($long gt $threshold-verse-prose-nroflonglines)">
                    <xsl:apply-templates mode="no-lg__prose"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates mode="lg"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
        <!-- [descendant::p[not(parent::tg_grp[@TEI eq 'lg'])][not(@TEI)]] -->
    </xsl:template>
    <!--
    <xsl:template match="div[not(ancestor::div)]" mode="temp-maintext">
        <!-\- ### check -\->
        <xsl:apply-templates/>-->
    <!--</xsl:template>-->
    <xsl:template match="div[@type eq 'text']/div" mode="temp-maintext">
        <xsl:copy>
            <xsl:attribute name="type" select="'mainText'"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>


    <!-- ## Entfernen (clean up): -->
    <xsl:template
        match="div[not(ancestor::div)]//p[ends-with(@xml:id,'.part2')][count(*) eq
        1][hi][normalize-space() eq '']"
        priority="2" mode="drama-2"/>
    <xsl:template match="div[not(ancestor::div)]//hi[not(node())][not(preceding-sibling::node())]"
        priority="1" mode="drama-2"/>

    <!-- whitespace verbessern: -->
    <!--  <xsl:template
        match="text[@tg_step eq
        'drama-2']//p[ends-with(@tgID,'.part2')][not(normalize-space() eq
        '')]/node()[1][self::text()]" priority="2">
        <xsl:value-of select="replace(.,'^\s+','')"/>
    </xsl:template> -->

    <!-- ## speaker[stage] -> speaker, stage (stage aus speaker herausnehmen und danach schreiben): -->
    <!--div[not(ancestor::div)][@tg_step eq 'drama-2']//tg_speaker//hi[@rend eq 'italic'] -->

    <!-- Katrin neu-->
    <!-- <xsl:template match="@TEI" mode=""></xsl:template>-->

</xsl:stylesheet>
