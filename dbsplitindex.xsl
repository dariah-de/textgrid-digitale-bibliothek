<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:md="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:fn="http://textgrid.info/namespaces/functions/dpsplitindex"
    xmlns:f="http://www.textgrid.de/ns/filelist" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs rdf ore md f"
    version="2.0">


    <xsl:param name="base" select="static-base-uri()"/>
    <xsl:param name="html" select="resolve-uri('../html/', $base)"/>
    <xsl:param name="htmlr" select="fn:relativize($base, $html)"/>
    <xsl:param name="filelist"/>

    <xsl:template match="/">
        <xsl:variable name="title" select="fn:metadata(document-uri(/))//md:title"/>

        <html>
            <head>
                <title>
                    <xsl:value-of select="$title"/>
                </title>
            </head>
            <body>
                <style type="text/css">
                    :link {
                        text-decoration:none;
                    }
                    .format {
                        color:lightgray;
                    }
                    .edition { color: green; }
                    .aggregation { color: #088; font-style: italic; }
                    .collection { color: magenta; }
                    .xml{
                        font-weight:bold;
                    }
                    li > .error{
                        font-weight:bold;
                        background:yellow;
                    }
                    .entry:hover{
                        background:#eee;
                    }
                    a:hover{
                        border-bottom:1px solid blue;
                    }
                    .error.notfound{
                        border-bottom:1px solid red;
                    }
                    .error.extension{
                        border-bottom:1px dashed red;
                    }
                    h1 > .meta{
                        font-weight:normal;
                        color:gray;
                    }
                    .filename { color: lightgray; font-size: smaller; float: right; }
                    .entry:hover .filename { color: blue; }
                </style>
                <xsl:variable name="index">
                    <h1>
                        <a href="{fn:relativize($base, document-uri(.))}">
                            <xsl:value-of select="$title"/>
                        </a>
                        <small>
                            <a href="{fn:relativize($base, document-uri(.))}.meta" class="meta">
                                (Metadaten)</a>
                            <xsl:if test="ends-with(document-uri(.), '.edition')">
                                <xsl:call-template name="work">
                                    <xsl:with-param name="edition" select="document-uri(.)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </small>
                    </h1>
                    <xsl:apply-templates mode="body"/>
                </xsl:variable>
                <xsl:copy-of select="$index"/>

                <xsl:if test="$filelist">
                    <xsl:variable name="extraFiles">
                        <ul class="extraFiles">
                            <xsl:for-each select="document($filelist)//f:file">
                                <xsl:variable name="filename" select="text()" as="xs:anyURI"/>
                                <xsl:if test="not(
                                        $index//html:a[ends-with(@href,$filename)]
                                    or  ends-with($filename, '.work')
                                    or  ends-with($filename, '.csv'))">
                                    <li>
                                        <a href="{$filename}">
                                            <xsl:value-of select="$filename"/>
                                        </a>
                                    </li>
                                    <xsl:message>WARNING: <xsl:value-of select="$filename"/> is
                                        never referenced in an aggregation.</xsl:message>
                                </xsl:if>
                            </xsl:for-each>
                        </ul>
                    </xsl:variable>

                    <xsl:choose>
                        <xsl:when test="$extraFiles//html:li">
                            <h1>Spurious Files</h1>
                            <p>The following files exist in the filesystem, but are never
                                aggregated:</p>
                            <xsl:copy-of select="$extraFiles"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <p>All existing files except for the root have also been aggregated.</p>
                            <xsl:text disable-output-escaping="yes">&lt;!--</xsl:text>
                            <xsl:copy-of select="$extraFiles"/>
                            <xsl:text disable-output-escaping="yes">--&gt;</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="rdf:Description" mode="body">
        <ul>
            <xsl:apply-templates mode="#current"/>
        </ul>
    </xsl:template>

    <xsl:template match="ore:aggregates" mode="body" xmlns:file="java.io.File">
        <xsl:variable name="aggregate" select="resolve-uri(@rdf:resource, document-uri(/))"/>
        <!--xsl:message select="concat('aggregate=',$aggregate,', resource=', @rdf:resource, ', document-uri=',document-uri(/))"/-->
        <xsl:variable name="format" select="fn:metadata($aggregate)//md:format"/>
        <xsl:variable name="href" select="fn:relativize($base,$aggregate)"/>
        <xsl:variable name="title" select="fn:metadata($aggregate)//md:title"/>
        <li>
            <span class="entry">
                <a href="{$href}" class="{replace($format, '.*/(tg.)?([^+]*).*', '$2')}">
                    <xsl:value-of select="$title"/>
                </a>
                <span class="format"> (<xsl:value-of select="$format"/>)</span>
                <xsl:if
                    test="not(matches($aggregate, '(\.item\.xml|\.edition|\.aggregation|\.collection|\.jpg|\.png)$'))">
                    <span class="error extension"> Invalid filename: <xsl:value-of
                            select="$aggregate"/></span>
                    <xsl:message>WARNING: Invalid filename reference <xsl:value-of
                            select="$aggregate"/></xsl:message>
                </xsl:if>

                <xsl:if
                    test="matches($aggregate, '\.jpg|\.png') and not(doc-available(concat($aggregate, '.meta')))">
                    <span class="error notfound"> Aggregate not found: <xsl:value-of
                            select="$aggregate"/></span>
                    <xsl:message>ERROR: Referenced file does not exist: <xsl:value-of
                            select="$aggregate"/></xsl:message>
                </xsl:if>
                <xsl:if
                    test="matches($aggregate, '\.item\.xml|\.edition|\.aggregation|\.collection') and not(doc-available($aggregate))">
                    <span class="error notfound"> Aggregate not found: <xsl:value-of
                            select="$aggregate"/></span>
                    <xsl:message>ERROR: Referenced file does not exist: <xsl:value-of
                            select="$aggregate"/></xsl:message>
                </xsl:if>
                <xsl:if test="matches($aggregate, '\.item\.xml$')">
                    <xsl:variable name="relresource" select="substring(@rdf:resource, 4)"/>
                    <xsl:if test="substring(@rdf:resource, 1, 3) != '../'">
                        <xsl:message>ERROR: <xsl:value-of select="@rdf:resource"/> does not start
                            with '../' as expected</xsl:message>
                    </xsl:if>
                    <a class="htmllink" href="{$htmlr}/{$relresource}.html" title="{$title} (HTML)">
                        HTML </a>
                </xsl:if>
                <a class="metalink" href="{fn:relativize($base, concat($aggregate, '.meta'))}"
                    title="{$title} (Metadaten)"> Metadaten </a>
                <xsl:if test="contains($format, 'edition')">
                    <xsl:call-template name="work">
                        <xsl:with-param name="edition" select="$aggregate"/>
                    </xsl:call-template>
                </xsl:if>
                <a href="{$href}" class="filename"><xsl:value-of select="replace($href, '^.*/', '')"/></a>
            </span>
            <xsl:if test="contains($format, 'aggregation')">
                <xsl:apply-templates mode="#current" select="doc($aggregate)"/>
            </xsl:if>
        </li>
    </xsl:template>

    <xsl:template name="work">
        <xsl:param name="edition" required="yes"/>
        <xsl:variable name="work" select="fn:metadata($edition)//md:isEditionOf/text()"
            as="xs:anyURI"/>
        <xsl:text> → </xsl:text>
        <a href="{fn:relativize($base, resolve-uri($work, $edition))}.meta">Work Metadata</a>
    </xsl:template>

    <!-- fn:metadata($uri) liefert den Metadatensatz zu $uri. Triviale implementierung für die DB-Daten  -->
    <xsl:function name="fn:metadata">
        <xsl:param name="uri"/>
        <xsl:copy-of select="doc(concat($uri, '.meta'))"/>
    </xsl:function>

    <!-- von http://www.stylusstudio.com/xsllist/200803/post10180.html -->
    <xsl:function name="fn:relativize" as="xs:anyURI">
        <xsl:param name="source" as="xs:string"/>
        <xsl:param name="target" as="xs:string"/>

        <!-- Fully qualified source and target to source-uri and target-uri. -->
        <xsl:variable name="target-uri" select="resolve-uri($target)"/>
        <xsl:variable name="source-uri" select="resolve-uri($source)"/>

        <!--
            Now we collapse consecutive slashes and strip trailing filenames from
            both to compute $a (source) and $b (target).
            
            We then split $a on '/' and walk through the sequence, comparing
            each part to the corresponding component in $b, concatenating the
            results with '/'. Result $c is a string representing the complete
            set of path components shared between the beginning of $a and the
            beginning of $b.
        -->
        <xsl:variable name="a"
            select="tokenize( replace( replace($source-uri, '//+', '/'), '[^/]+$', ''), '/')"/>
        <xsl:variable name="b"
            select="tokenize( replace( replace($target-uri, '//+', '/'), '[^/]+$', ''), '/')"/>
        <xsl:variable name="c"
            select="string-join(
            for $i in 1 to count($a)
            return (if ($a[$i] = $b[$i]) then $a[$i] else ()), '/')"/>

        <xsl:choose>
            <!--
                if $c is empty, $a and $b do not share a common base, and we cannot
                return a relative path from the source to the target. In that case
                we just return the resolved target-uri.
            -->
            <xsl:when test="$c eq ''">
                <xsl:sequence select="$target-uri"/>
            </xsl:when>
            <xsl:otherwise>
                <!--
                    Given the sequence $a and the string $c, we join $a using '/' and
                    extract the substring remaining  after the prefix $c is removed.
                    We then replace all path components with '..', resulting in the
                    steps up the directory path which must be made to reach the
                    target from the source. This path is named $steps.
                -->
                <xsl:variable name="steps"
                    select="replace(replace(
                    substring-after(string-join($a, '/'), $c),
                    '^/', ''), '[^/]+', '..')"/>

                <!--
                    Resolving $steps against $source-uri gives us $common-path, the
                    fully qualified path shared between $source-uri and $target-uri.
                    
                    Stripping $common-path from $target-uri and prepending $steps will
                    leave us with $final-path, the relative path from  $source-uri to
                    $target-uri. If the result is empty, the destination is './'
                -->
                <xsl:variable name="common-path"
                    select="replace(resolve-uri($steps, $source-uri), '[^/]+$', '')"/>

                <xsl:variable name="final-path"
                    select="replace(concat($steps, substring-after($target-uri, $common-path)), '^/', '')"/>

                <xsl:sequence select="xs:anyURI(if ($final-path eq '') then './' else $final-path)"/>

            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>
