<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template name="plink-default" match="text[@tg_type='metadata']//plink">
        <ref xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </ref>
    </xsl:template>
    <xsl:template match="plink">
        <xsl:call-template name="plink-default"/>
    </xsl:template>
    <xsl:template match="plink" mode="desc">
        <xsl:call-template name="plink-default"/>
    </xsl:template>
    
    <!--docu:next-template:
        plink/@href wird zu @target oder zu @cRef.
        template is not used
    -->
    <xsl:template match="plink/@href" name="plink_href2cRef">
        <xsl:attribute name="cRef" select="."/>
     
    </xsl:template>
    <xsl:template match="plink/@href" mode="plink_href2target">
        <xsl:if test="key('all-pnames',.)">
            <xsl:attribute name="target" select="concat('#',key('all-pnames',.)[1]/@tgID)"/>
            <!-- export: problem: Bei Michael Denis und bei Schottelius: 
                key('all-pnames',.)/@tgID ergibt mehrere Ergebnisse. -->
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="pname-default" mode="#all"
        match="pname [not(ancestor::tg_note|ancestor::tei:note)][not(ancestor::tg_noteAnchor)]">
        <anchor xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </anchor>
    </xsl:template>
    <xsl:template match="pname/@location">
        <xsl:attribute name="n" select="."/>
    </xsl:template>
    <xsl:template match="pname/@name"/>
    <xsl:template match="pname/@location-from-plink"/>
    
    
    
    <xsl:template match="tg_noteAnchor">
        <ref xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:choose>
                <xsl:when test="tg_correspNote/@id">
                    <xsl:attribute name="type" select="'noteAnchor'"/>
                    <xsl:attribute name="target" select="concat('#',tg_correspNote/@id)">
                        <!-- @target verweist eigentlich auf das erste p in der
                            zugehörigen Fußnote. -->
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="type" select="'noteAnchor-undefNote'"/>
                    <xsl:attribute name="target" select="concat('#',tg_correspNote/@fallback-id)"/>
                    <xsl:attribute name="n" select="tg_correspNote/@fallback-location">
                        <!-- @cRef ist nicht erlaubt, wenn @target existiert. -->
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </ref>
    </xsl:template>
    <xsl:template match="tg_noteAnchor/plink">
        <ptr xmlns="http://www.tei-c.org/ns/1.0" cRef="{@href}"/>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tg_noteAnchor/tg_correspNote">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tg_noteAnchor//text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    <xsl:template match="tg_noteAnchor/pname">
        <xsl:call-template name="pname-default"/>
    </xsl:template>
    
    <xsl:template match="tg_note">
        <note xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="xml:id" select="concat(p[1]/@tgID,'.note')"/>
            <xsl:apply-templates select="(descendant::plink)[1]/@href" mode="plink_href2target">
                <!--: @target vom ersten plink erhalten -->
            </xsl:apply-templates>
            <!--  <div>templates1</div>-->
            <xsl:apply-templates select="@*|p"/>          
            <!--    <div>templates12</div>-->
        </note>
    </xsl:template>
    <xsl:template match="tg_note/p/pname | tg_note/p/plink ">
        <xsl:if test="self::plink">
            <xsl:call-template name="plink-default"/>
        </xsl:if>
        <xsl:if test="self::pname">
            <xsl:call-template name="pname-default"/>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>