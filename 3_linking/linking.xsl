<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:tns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:tgl="http://textgrid.info/namespaces/metadata/language/2010"
    xmlns:tgs="http://textgrid.info/namespaces/metadata/script/2010"
    xmlns:tgr="http://textgrid.info/namespaces/metadata/agent/2010"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:jxb="http://java.sun.com/xml/ns/jaxb" jxb:version="2.0" xmlns:saxon="http://saxon.sf.net/"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tg saxon jxb tns tgl tgs tgr doc" version="2.0">
    <xsl:output indent="yes"/>



    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>The linking is based on the originally pname and plink elements.</xd:p>
            <xd:p> The @href-attribute of the plink-elements originally pointed to the corresponding
                footnote through the concatenation of the xpath expression to the corresponding
                element with the value of @name attribute of the corresponding pname.</xd:p>
            <xd:p>In order to be able to use this referencing independently from the current
                position of an element, we added a knew attribute to the pname element which is
                composed of its location and the value of its @name-attribute. (So it has the same
                value as the @href of the corresponding plink)</xd:p>
            <xd:p>After transforming the zeno-data with generateTEI.xsl plinks are named rs and
                pname anchors.</xd:p>
            <xd:p>The referencing attribut of both is @n now.</xd:p>
            <xd:p>The stylesheet will transform the links in two different structures:</xd:p>
            <xd:p>References (rs and its anchor) within a text are sourounded with a ref-Element and
                the value of its target-attribute will be the value of the xml:id of the element
                addressed by the @n of the rs-Element. The rs Element will be a ptr-Element with the
                attribute @cRef (value: the @n of rs) </xd:p>
            <xd:p>Footnotes (rs and its anchor) at the end of a text or paragraph or references
                which are not in a mixed-content-enviroment, are surounded with a note-Element and
                the value of its target-attribute will be the value of the xml:id of the element
                addressed by the @n of rs. The rs Element will be a ref-Element with the attribute
                @cRef (value: the @n of rs)</xd:p>
        </xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
       <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Default-templates: Identity transformation</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="*" mode="#all" name="default-elem">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="@*" mode="#all" name="default-attr">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- *************************************************************************  -->


    <!-- **************************  Keys  ******************************************  -->
    <xsl:key name="anchor-container"
        match="*[not(ancestor::div[@type eq 'front'])]//*[rs]
        [  anchor [every $x in preceding-sibling::node() satisfies (normalize-space(string($x)) eq '') ]  ]"
        use="anchor/@n">
        <!-- ## Das ergibt ein Index der Fußnoten. Es sind Elemente, die anchor und rs enthalten und mit anchor anfangen. 
            Deswegen: every $x in preceding-sibling::node() satisfies (normalize-space(string($x)) eq '')
        (es bedeutet, dass vor anchor kein Text sein darf außer Leerzeichen.)-->
    </xsl:key>
    <xsl:key name="anchor-container-location-from-rs"
        match="*[not(ancestor::div[@type eq 'front'])]//*[rs][anchor[@n-from-rs]]"
        use="anchor/@n-from-rs">
        <!-- ## Das ist ein zweiter Index. Hier werden Containers von anchor und rs nach dem Attribut @n-from-rs indiziert.
        Es wird hier nur dann gesucht, wenn die Suche im ersten Index nicht erfolgreich war. -->
    </xsl:key>
    <xsl:key name="all-note-id-s" match="tei:byline" use="@target"/>
    <xsl:key name="all-anchors" match="anchor" use="@n"/>
    <!-- ******************************************************************************************************  -->


    <!-- ******************************************************************************************************  -->
    <xsl:template match="/">
        <xsl:variable name="add-anchor-with-rs-location">
            <!-- In diesem Schritt bekommt anchor eine zusätzliche location (@n_rs), die aus dem Wert 
                    des nachstehenden rs/@n berechnet wird. -->
            <xsl:apply-templates select="node()" mode="add-anchor-with-rs-location"/>
        </xsl:variable>
        <xsl:variable name="footnotesInMaintext">
            <xsl:apply-templates select="$add-anchor-with-rs-location/node()"
                mode="footnotesInMaintext"/>
        </xsl:variable>
        <xsl:apply-templates mode="remainingFootnotes" select="$footnotesInMaintext"/>

    </xsl:template>
    <!-- ******************************************************************************************************  -->


    <!-- ************************** Mode add-anchor-with-rs-location ******************************************  -->
    <!-- ## Template, das @n-from-rs zu anchor hinzufügt, wenn 
    der nächste rs nach anchor eine andere "location" hat als
    anchor/@n. -->
    <xsl:template mode="add-anchor-with-rs-location"
        match="* [   anchor 
        [every $x in preceding-sibling::node() satisfies (normalize-space(string($x)) eq '') ]
        [following-sibling::node()[normalize-space(.) ne ''][1][self::rs] ]  ] 
        / anchor  ">
        <xsl:variable name="rs-href-part-before-hash"
            select="replace(following-sibling::rs[1]/@n,'#[^#]*$','')"/>
        <xsl:variable name="crtlocation-part-before-hash" select="replace(@n,'#[^#]*$','')"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:if test="$rs-href-part-before-hash ne $crtlocation-part-before-hash">
                <xsl:attribute name="location-from-rs"
                    select="concat($rs-href-part-before-hash,'#',@ana)"/>
            </xsl:if>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    <!-- ******************************************************************************************************  -->

    <!-- **************************************** Mode: footnoesInMaintext and rs2ptr ****************************************  -->
    <!--  Fußnoten im Haupttext: (anchor,rs): -->
    <!-- match-Ausdruck liefert p, das rs und anchor enthält, wenn sie direkt aufeinander folgen. -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>All anchors with a following sibling rs will be surrounded with a ref-tag, when
                the preceding-sibling has text or the following sibling rs has a superscript or
                ....</xd:p>
            <xd:p>The anchor-element, the corresponding text and the rs-Element are surpunded with
                an ref-Element.</xd:p>
            <xd:p>The original rs-Element will be a ptr-Element</xd:p>
            <xd:p>The value of the target-attribute of the ref-element is the xml:id of the element
                addressed by the @n-Element of the original rs-Element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template mode="footnotesInMaintext"
        match="* 
        [not(self::tei:note)][not(ancestor-or-self::tei:anchor)] 
        [not(ancestor::tei:div[@type='footnotes'])]
        [ anchor [following-sibling::*[1][self::tei:rs]] ]">
        <!--  Vor dem Anker muss Text existieren, sonst handelt es sich um etwas anderes,
        wie etwa eine Fußnote am Ende. -->
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()"
                group-starting-with="anchor | node()[preceding-sibling::*[1][self::rs]]">
                <xsl:variable name="crtG">
                    <xsl:sequence select="current-group()"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when
                        test="current-group()[1][self::anchor]
                            [ following-sibling::node()[not(normalize-space(string(.)) eq '')][1][self::rs] ]
                            [preceding-sibling::node()[not(normalize-space(string(.)) eq '') 
                            or  following-sibling::rs[1][hi[@rend eq 'superscript']] or (matches(@ana,'^(N|F[^uU]|Fußnote[^nN])') ) ]]
                            ">
                        <ref xmlns="http://www.tei-c.org/ns/1.0">
                            <xsl:variable name="corresp"
                                select="key('anchor-container', $crtG//rs[1]/@n)"/>
                            <xsl:variable name="fallback1"
                                select="key('anchor-container-location-from-rs', $crtG//rs[1]/@n)"/>
                            <xsl:choose>
                                <xsl:when test="count($corresp) eq 1">
                                    <xsl:attribute name="type" select="'noteAnchor'"/>
                                    <xsl:attribute name="target"
                                        select="concat('#',$corresp/@xml:id)">
                                        <!-- @target verweist eigentlich auf das erste p in der
                                            zugehörigen Fußnote. -->
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:when test="count($fallback1) eq 1">
                                    <xsl:attribute name="type" select="'noteAnchor'"/>
                                    <xsl:attribute name="target"
                                        select="concat('#',$fallback1/@xml:id)"/>
                                    <!--     <xsl:attribute name="certainty" select="'high'"/>-->
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:variable name="target-part-before-hash"
                                        select="replace( $crtG//rs[1]/@n,'#[^#]*$','')"/>
                                    <xsl:variable name="fallback2"
                                        select="//*[ $crtG//rs[1]/@n eq $target-part-before-hash]"/>
                                    <xsl:if test="count($fallback2) eq 1">
                                        <xsl:attribute name="n" select="$target-part-before-hash"/>
                                        <xsl:attribute name="type" select="'noteAnchor-undefNote'"/>
                                        <xsl:attribute name="target"
                                            select="concat('#',$fallback2/@xml:id)"/>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:apply-templates select="current-group()" mode="rs2ptr"/>
                        </ref>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="current-group()" mode="#current"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>rs-ELements in the main Text are transformed to ptr.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="rs" mode="rs2ptr">
        <ptr xmlns="http://www.tei-c.org/ns/1.0" cRef="{@n}">
            <xsl:apply-templates select="@*" mode="rs2ptr"/>
        </ptr>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="rs/@n" mode="rs2ptr"/>

    <xsl:template match="rs//text()" mode="#all">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    <!-- ******************************************************************************************************  -->


    <!-- ***************************************** Mode remainingFootnotes *************************************************************  -->
    <xd:doc scope="component">
        <xd:desc>
            <xd:p>Footnotes (rs-elements) at the end of a text or a paragraph are surounded with a
                note-Element.</xd:p>
            <xd:p>References in the maintext (rs-elements), that have no text as sibling-elemente
                function as some kind of unkown reference and are also surounded with a
                note-Element.</xd:p>
            <xd:p>The value of the target-attribute of the note-Element is the xml:id of the Element
                to which the @n-Attrbiute of the original-rs-element points to.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template mode="remainingFootnotes"
        match="tei:div[@type='footnotes']
        | *[p[anchor or rs]][some $x in */@xml:id satisfies
        (exists(key('all-note-id-s',$x) ))]"
        priority="3">
        <xsl:choose>
            <xsl:when
                test="p 
                [*[1][self::anchor or self::rs] 
                [every $x in preceding-sibling::node() satisfies
                normalize-space($x) eq ''] ]">
                <xsl:copy>
                    <xsl:copy-of select="@*"/>
                    <xsl:for-each-group select="node()"
                        group-starting-with="p 
                        [not(anchor[starts-with(@ana,'N')])] [not(descendant::rs[1][sup or ls])]
                        [*[1][self::anchor or self::rs] 
                        [every $x in preceding-sibling::node() satisfies
                        normalize-space($x) eq ''] ]">
                        <xsl:variable name="crtGr" select="current-group()"/>
                        <xsl:choose>
                            <xsl:when
                                test="current-group()[1]
                                [not(anchor[starts-with(@ana,'N')])] [not(descendant::rs[1][hi[@rend eq 'superscript'] or ls])]
                                [*[1][self::anchor or self::rs] 
                                [every $x in preceding-sibling::node() satisfies
                                normalize-space($x) eq ''] ]">
                                <!-- wir zu note, das href des ersten rs zu target -->
                                <note xmlns="http://www.tei-c.org/ns/1.0">
                                    <!-- ueberpruefen, ob hier die richtigen Attibutwerte rauskommen -->
                                    <xsl:attribute name="xml:id"
                                        select="concat($crtGr[self::p][1]/@xml:id,'.note')"/>
                                    <xsl:apply-templates select="$crtGr//descendant::rs[1]/@n"
                                        mode="cRef2Target"/>
                                    <xsl:apply-templates select="$crtGr" mode="remainingFootnotes"/>
                                </note>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="current-group()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each-group>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="component">
        <xd:desc>
            <xd:p>The rs-Elements functioning as footnotes at the end of a text or an paragraph will
                be a ref-Element. </xd:p>
            <xd:p>The rs-Elements in the maintext, that have no text as sibling-elemente, function
                as some kind of unkown reference and will be a ref-Element. </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="rs[not(ancestor::tei:div[@type eq 'front'])]" mode="remainingFootnotes">
        <ref xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="cRef" select="@n"/>
            <xsl:apply-templates select="@*" mode="remainingFootnotes"/>
            <xsl:apply-templates/>
        </ref>
    </xsl:template>

    <xsl:template match="@n[parent::rs]" mode="remainingFootnotes"/>

    <xsl:template match="@n" mode="cRef2Target">
        <xsl:if test="key('all-anchors',.)">
            <xsl:attribute name="target" select="concat('#',key('all-anchors',.)[1]/@xml:id)"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="anchor/@ana" mode="remainingFootnotes"/>

    <xsl:template match="anchor/@location-from-rs" mode="remainingFootnotes"/>
    <!-- ******************************************************************************************************  -->

</xsl:stylesheet>
