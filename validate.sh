#!/bin/bash

setup() {

    configdir="`dirname $0`"
    if [ "x$configdir" = "x" ]
    then
        configdir="."
    fi

    # Read the config file
    echo "reading config from $configdir"
        if [ -r workflow.rc ]
        then 
            . "$configdir/workflow.rc"
        else
            die "ERROR: Configuration file $configdir/workflow.rc not found."
        fi

}

validate_tei() {
    echo "Validating files in $tei_validate_dir with $tei_schema ..."
    find "$tei_validate_dir" -name '*.xml' -print0 | \
                xargs -0 xmllint --schema "$tei_schema" --noout --nowarning
}

setup
validate_tei
