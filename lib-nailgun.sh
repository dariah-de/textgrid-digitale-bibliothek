NAILGUN_JAR=${NAILGUN_JAR:-/usr/share/java/nailgun.jar}
NAILGUN_CLIENT=${NAILGUN_CLIENT:-ng-nailgun}
SAXON_JAR=${SAXON_JAR:-/usr/share/saxon/saxon9he.jar}

start_nailgun() {
    classpath="$1"
    jvm_args="${2-}"
    port="${3:-2113}"

    echo "classpath=$classpath; jvm_args=$jvm_args; port=$port"

    echo java -cp "$NAILGUN_JAR:$classpath:$CLASSPATH" $jvm_args com.martiansoftware.nailgun.NGServer "$port" &
    java -cp "$NAILGUN_JAR:$classpath:$CLASSPATH" $jvm_args com.martiansoftware.nailgun.NGServer "$port" &
    sleep 1
    export NAILGUN_PORT="$port"
}


start_saxon_server() {
    export SAXON_PORT="${2:-2113}"
    start_nailgun "$SAXON_JAR" "$1" "$SAXON_PORT"

    saxon() {
        ${NAILGUN_CLIENT} --nailgun-port $SAXON_PORT net.sf.saxon.Transform ${1+"$@"}
    }
}


stop_nailgun() {
    ${NAILGUN_CLIENT} ng-stop
}
