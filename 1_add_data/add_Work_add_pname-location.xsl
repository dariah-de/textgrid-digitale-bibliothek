<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tg="http://www.textgrid.de"
    xmlns:doc="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs tg" version="2.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <!-- ************************* Rough Description  **********************************************  -->
    <xd:doc scope="stylesheet">
        <xd:desc>Adds the attribute werkebene.</xd:desc>
        <xd:desc>Adds @location to article-Elements and pnames.</xd:desc>
    </xd:doc>
    <!-- ***********************************************************************************  -->


    <!-- ********************** Output, includes and imports   *********************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p>The name of the output subdirectory of the transformation. </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:param name="output-subfolder" select="'02_work_pname-location'"/>
    <xsl:include href="../pathVariables/pathVariables.xsl"/>
    <!-- ***********************************************************************************  -->


    <!-- ******************************** Other variabels **************************************************  -->
    <doc:doc>
        <doc:desc>
            <doc:p> Text-category as defined in the zeno-data</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="zeno-category"
        select="replace(substring-before($crturi,'/Text/'), '.+/','')"/>
    <!-- ***********************************************************************************  -->


    <!-- ***********************  Keys  ************************************ -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>The key contains</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:key name="all-pnames" match="pname" use="@location"/>

    <doc:doc scope="component">
        <doc:desc>
            <doc:p>It seems that the uris of the zeno-data has been normalized somewhere, for that
                reason the paths to the articleList must be normalized too.</doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:variable name="basename-reg">
        <xsl:variable name="uri" select="replace(document-uri(/),'.+/','')"/>
        <xsl:analyze-string select="$uri" regex="%[A-F0-9][A-F0-9](%[A-F0-9][A-F0-9])?">
            <xsl:matching-substring>
                <xsl:value-of select="'_'"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:value-of select="replace(.,'[^A-Za-z0-9,\.\-()]','_')"/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:variable>
    <!-- ***********************************************************************************  -->


    <!-- **************************  Idendity Transformation  ******************************************  -->
    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Identity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Identity Transformation</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="@* | comment() | processing-instruction()">
        <xsl:copy-of select="."/>
    </xsl:template>
    <!-- ***********************************************************************************  -->


    <!-- Arrrrrrrrrrrrrrrrrrrrrrgh -->
    <xsl:variable name="articlelists-path" select="concat('../articleLists/', $basename-reg)"/>
    <xsl:variable name="doc-werkebene" select="document($articlelists-path)"/>
    <xsl:variable name="doc-werkebene-correctedIds">
        <xsl:apply-templates select="$doc-werkebene" mode="doc-werkebene"/>
    </xsl:variable>
    <!-- ***********************************************************************************  -->


    <!-- ***********************  Mode doc-werkebene ******************************************** -->
    <doc:doc scope="component">
        <doc:desc>
            <doc:p>Templates with mode="doc-werkebene" are used to correct the format of the tgId in
                the articleslists documents </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:template mode="doc-werkebene" match="*">
        <xsl:copy>
            <xsl:apply-templates mode="doc-werkebene" select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <doc:doc scope="component">
        <doc:desc>
            <doc:p>The tgId in the articlelists documents have an '_' but not a '.'. This is
                corrected here </doc:p>
        </doc:desc>
    </doc:doc>
    <xsl:template mode="doc-werkebene" match="@tgID">
        <xsl:attribute name="tgID" select="replace(.,'_','.')"/>
    </xsl:template>
    <!-- ********************************************************************************* -->


    <xd:doc scope="component">
        <doc:desc>
            <doc:p>Creating a result-document</doc:p>
        </doc:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:variable name="location-prefix"
        select="concat('/','Literatur','/',/*/descendant::articlegroup[1]/@name,'/')">
        <!-- @location wird bei jedem article erzeugt, denn das wird implizit als Verweisziel verwendet
            (bei Fussnoten oder bei zentral angelegten Metadaten, deren Zugehoerigkeit zu ihren Inhalten 
            durch Verweisung ausgedrueckt wird). -->
    </xsl:variable>

    <xsl:template match="article">
        <xsl:variable name="crtTgID" select="@tgID"/>
        <xsl:variable name="path">
            <xsl:for-each select="ancestor::article">
                <xsl:value-of select="concat(./lem[1],'/')"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="fullpath">
            <xsl:value-of select="concat($location-prefix,$path,lem[1]/text())"/>
        </xsl:variable>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="location" select="$fullpath"/>
            <xsl:attribute name="werkebene">
                <xsl:choose>
                    <!-- test="$doc-werkebene//article[@tgIDmeta = 'tg1'][contains(./text(),'OK')]" -->
                    <xsl:when test="$doc-werkebene-correctedIds//article[@tgID]">
                        <xsl:choose>
                            <xsl:when
                                test="$doc-werkebene-correctedIds//article[replace(@tgID,'_','.') = $crtTgID]">
                                <xsl:value-of select="'yes'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'no'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="@werkebene">
                                <xsl:value-of select="@werkebene"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'unset'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates>
                <xsl:with-param name="article-location" select="$fullpath" tunnel="yes"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="pname">
        <xsl:param name="article-location" tunnel="yes"/>
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="location" select="concat($article-location,'#',@name)"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>


</xsl:stylesheet>
